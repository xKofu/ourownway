using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaInicioHUD_controller : MonoBehaviour
{
    [Header("EVENTOS")]
    [SerializeField] private GameEvent nuevaPartida;
    [SerializeField] private GameEvent cargarPartida;
    [SerializeField] private GameEvent salir;

    
    
    private Vector3[] posiciones = new Vector3[3];
    
    [Header("Selector")]
    [SerializeField] private GameObject selector;
    [SerializeField] private TMPro.TextMeshProUGUI texto;
    private int id;
    void Start()
    {
        posiciones[0] = new Vector3(960, 540+20, 0);
        posiciones[1] = new Vector3(960, 540-210, 0);
        posiciones[2] = new Vector3(960, 540-440, 0);
        id = 0;
        selector.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Moverse();
    }



    private void Moverse()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A))
            SubirSelector();
        else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
            BajarSelector();
        else if (Input.GetKeyDown(KeyCode.Return))
            SeleccionarOpcion();
    }


    private void ComprobarTexto()
    {
        switch (id)
        {
            case 0:
                texto.text = "NUEVA PARTIDA";
                break;
            case 1:
                texto.text = "CARGAR PARTIDA";
                break;
            case 2:
                texto.text = "SALIR";
                break;
        }
    }


    private void SubirSelector()
    {
        id--;
        if (id <= 0)
            id = 0;
        ComprobarTexto();
        selector.SetActive(true);
        selector.transform.position = posiciones[id];

    }
    private void BajarSelector()
    {
        id++;
        if (id >= 2)
            id = 2;
        ComprobarTexto();
        selector.SetActive(true);
        selector.transform.position = posiciones[id];
    }

    private void SeleccionarOpcion()
    {
        switch (id)
        {
            case 0:
                nuevaPartida.Raise();
                break;
            case 1:
                cargarPartida.Raise();
                break;
            case 2:
                salir.Raise();
                break;
        }
    }

    public void EntraRatonBotonNuevaPartida()
    {
        id = 0;
        ComprobarTexto();
        selector.SetActive(true);
        selector.transform.position = posiciones[id];
    }

    public void EntraRatonBotonCargarPartida()
    {
        id = 1;
        ComprobarTexto();
        selector.SetActive(true);
        selector.transform.position = posiciones[id];
    }

    public void EntraRatonBotonSalir()
    {
        id = 2;
        ComprobarTexto();
        selector.SetActive(true);
        selector.transform.position = posiciones[id];
    }


}
