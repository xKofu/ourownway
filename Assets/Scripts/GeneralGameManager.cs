using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralGameManager : MonoBehaviour
{

    [Header("EVENOTS")]
    [SerializeField] private GameEvent eventoLlamarAQueSePosicioneElPlayer;


    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }



    public void Continuar()
    {
        // cambiar al paki, el se ocupa de ir al sitio que toca
        SceneManager.LoadScene("Paki");
    }

    public void Salir()
    {
        // cambiar escena a la principal
        SceneManager.LoadScene("PantallaInicio");
    }


    public void PosicionarPlayer()
    {
        StartCoroutine(EsperarsePosicionarPlayer());
    }

    private IEnumerator EsperarsePosicionarPlayer()
    {
        yield return new WaitForSeconds(0.05f);
        eventoLlamarAQueSePosicioneElPlayer.Raise();
    }
}
