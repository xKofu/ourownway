using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class General_manager : MonoBehaviour
{

    [SerializeField]
    private ScriptableInfoTP info;


    private void Start()
    {
        info.EscenaOrigen = null;
        info.numTP = 0;
        info.posSpawn = new Vector3(0, 0, 0);
    }
}
