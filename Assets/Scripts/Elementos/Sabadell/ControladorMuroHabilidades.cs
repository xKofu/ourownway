using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorMuroHabilidades : MonoBehaviour
{
    [SerializeField] private GameObject Muro;
    [SerializeField] private bool Check1;
    [SerializeField] private bool Check2;
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameEvent PanelAviso;
    private void Update()
    {
        if (Check1 && Check2)
        {
            Muro.SetActive(false);
            itp.Obstaculo1Sab = true;
            PanelAviso.Raise();
        }
                    
    }

    public void MarkCheck(int a)
    {
        if (a == 2)
            Check1 = true;

        if (a == 5)
            Check2 = true;
    }
}
