using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCaidas : MonoBehaviour
{
    [SerializeField] private Transform PosTp;
    [SerializeField] private GameObject Cortina;
    [SerializeField] private GameEventInteger gei_dmg;

    IEnumerator TpPlayer(GameObject P)
    {
        
        Cortina.SetActive(true);
        Cortina.GetComponent<Animator>().Play("CortinaIN");
        yield return new WaitForSeconds(1);
        P.transform.position = PosTp.position;
        gei_dmg.Raise(10);
        Cortina.GetComponent<Animator>().Play("CortinaOUT");
        yield return new WaitForSeconds(1);
        Cortina.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            StartCoroutine(TpPlayer(collision.gameObject));
            
    }

}
