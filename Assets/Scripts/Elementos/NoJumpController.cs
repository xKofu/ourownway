using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoJumpController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<CharacterController>().JumpCanceled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<CharacterController>().JumpCanceled = false;
        }
    }

}
