using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlObstaculos : MonoBehaviour
{

    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private string ObstaculoAQuitar;
    [SerializeField] private GameObject Obstaculo;
    [SerializeField] private bool ActivarDesactivar;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            ObstaculoFunc();        
    }

    private void ObstaculoFunc()
    {
        Obstaculo.SetActive(ActivarDesactivar);
        switch(ObstaculoAQuitar)
        {
            case "Obstaculo1CB":
                itp.Obstaculo1CB = true;
                break;
        }
        
    }
}
