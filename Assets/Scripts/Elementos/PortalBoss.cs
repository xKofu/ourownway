using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalBoss : MonoBehaviour
{
    [SerializeField] private bool TouchingPlayer;
    [SerializeField] private GameObject Key;
    [SerializeField] private GameEvent IrBoss;

    void Update()
    {
        if (TouchingPlayer)
            if (Input.GetKeyDown(KeyCode.E))
            {
                IrBoss.Raise();
            }
    }
        private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = true;
            Key.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = false;
            Key.SetActive(false);
        }
    }
}
