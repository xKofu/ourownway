using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TpController : MonoBehaviour
{

    [SerializeField] private int m_numTP;
    [SerializeField] private Transform refereciaPos;
    [SerializeField] private Vector3 puntoSpawn;
    [SerializeField] private ScriptableInfoTP infoTp;
    [SerializeField] private bool TouchingPlayer;
    [SerializeField] private GameObject Key;
    [SerializeField] private GameEvent IrPaki;

    private void Update()
    {
        if (TouchingPlayer)
            if (Input.GetKeyDown(KeyCode.E))
            {
                infoTp.numTP = m_numTP;
                infoTp.EscenaOrigen = SceneManager.GetActiveScene().name;
                infoTp.posSpawn = new Vector3(refereciaPos.position.x, refereciaPos.position.y, 0);

                IrPaki.Raise();
            }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = true;
            Key.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = false;
            Key.SetActive(false);
        }
    }

    public void SoyGerard()
    {
        infoTp.quienEntra = true;
    }

    public void SoyPau()
    {
        infoTp.quienEntra = false;
    }

}
