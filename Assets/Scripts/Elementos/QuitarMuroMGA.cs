using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitarMuroMGA : MonoBehaviour
{
    [SerializeField] private GameObject Muro;
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameEvent PanelAviso;
    public void QuitarObstaculo(int a)
    {
        if (a == 3)
        {
            itp.Obstaculo1MGA = true;
            Muro.SetActive(false);
            PanelAviso.Raise();
        }
    }

}
