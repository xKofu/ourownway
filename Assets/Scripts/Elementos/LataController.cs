using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LataController : MonoBehaviour
{
    private SpriteRenderer sp;
    private Color ConAlpha;
    private Color SinAlpha;

    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
        ConAlpha = sp.color;
        SinAlpha = sp.color;
        SinAlpha.a = 0;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(10);
        sp.color = SinAlpha;
        yield return new WaitForSeconds(1);
        sp.color = ConAlpha;
        yield return new WaitForSeconds(1);
        sp.color = SinAlpha;
        yield return new WaitForSeconds(1);
        sp.color = ConAlpha;
        yield return new WaitForSeconds(1);
        sp.color = SinAlpha;
        yield return new WaitForSeconds(0.5f);
        sp.color = ConAlpha;
        yield return new WaitForSeconds(0.5f);
        sp.color = SinAlpha;
        yield return new WaitForSeconds(0.5f);
        sp.color = ConAlpha;
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
}
