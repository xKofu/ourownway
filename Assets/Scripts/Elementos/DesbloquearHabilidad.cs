using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesbloquearHabilidad : MonoBehaviour
{
    [SerializeField] private int AbilityIndex;
    [SerializeField] private bool TouchingPlayer;
    [SerializeField] private GameObject Key;
    [SerializeField] private GameEventInteger UnlockEvent;
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private AudioSource SoundPlayer;
    [SerializeField] private AudioClip Effect;

    private void Update()
    {
        if (TouchingPlayer)
            if (Input.GetKeyDown(KeyCode.E))
            {
                SoundPlayer.PlayOneShot(Effect);
                UnlockEvent.Raise(AbilityIndex);
                switch(AbilityIndex)
                {
                    case 0:
                        itp.habilidad1Pau = true;
                        itp.PickedAb1P = true;
                        break;
                    case 1:
                        itp.habilidad2Pau = true;
                        itp.PickedAb2P = true;
                        break;
                    case 2:
                        itp.habilidad3Pau = true;
                        itp.PickedAb3P = true;
                        break;
                    case 3:
                        itp.habilidad4Pau = true;
                        itp.PickedAb4P = true;
                        break;
                    case 4:
                        itp.habilidad1Gerard = true;
                        itp.PickedAb1G = true;
                        break;
                    case 5:
                        itp.habilidad2Gerard = true;
                        itp.PickedAb2G = true;
                        break;
                    case 6:
                        itp.habilidad3Gerard = true;
                        itp.PickedAb3G = true;
                        break;
                    case 7:
                        itp.habilidad4Gerard = true;
                        itp.PickedAb4G = true;
                        break;
                }
                gameObject.SetActive(false);
            }
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = true;
            Key.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = false;
            Key.SetActive(false);
        }
    }

}
