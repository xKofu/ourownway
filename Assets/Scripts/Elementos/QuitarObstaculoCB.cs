using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitarObstaculoCB : MonoBehaviour
{
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameEvent PanelAviso;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            QuitarMuro();
    }

    private void QuitarMuro()
    {
        itp.Obstaculo3CB = true;
        PanelAviso.Raise();
    }

}
