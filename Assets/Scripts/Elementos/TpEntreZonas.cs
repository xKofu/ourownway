using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TpEntreZonas : MonoBehaviour
{
    [SerializeField] private bool TouchingPlayer;
    [SerializeField] private int IndexDestino;
    [SerializeField] private GameObject Key;
    [SerializeField] private GameEventInteger CambiaZonas;

    void Update()
    {
        if (TouchingPlayer)
            if (Input.GetKeyDown(KeyCode.E))
            {
                CambiaZonas.Raise(IndexDestino);
            }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = true;
            Key.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = false;
            Key.SetActive(false);
        }
    }
}
