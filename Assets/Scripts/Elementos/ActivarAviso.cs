using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarAviso : MonoBehaviour
{
    [SerializeField] private GameObject Panel;
  
    public void Activar()
    {
        StartCoroutine(Aviso());
    }

    IEnumerator Aviso()
    {
        Panel.SetActive(true);
        GetComponent<Animator>().Play("EntradaPanel");
        yield return new WaitForSeconds(8);
        GetComponent<Animator>().Play("SalidaPanel");
        yield return new WaitForSeconds(1);
        Panel.SetActive(false);
    }

    public void Final()
    {
        GetComponent<Animator>().Play("Final");
    }

}
