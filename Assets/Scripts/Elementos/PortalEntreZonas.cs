using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEntreZonas : MonoBehaviour
{
    [SerializeField] private bool TouchingPlayer;
    [SerializeField] private GameObject Key;
    [SerializeField] private int numPortal;
    [SerializeField] private GameEventInteger Destino;
    [SerializeField] private GameEvent HeCambiado;

    void Update()
    {
        if (TouchingPlayer)
            if (Input.GetKeyDown(KeyCode.E))
            {
                Destino.Raise(numPortal);
                HeCambiado.Raise();
            }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = true;
            Key.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            TouchingPlayer = false;
            Key.SetActive(false);
        }
    }
}
