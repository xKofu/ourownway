using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Paki_controller : MonoBehaviour
{
    [Header("Escriptables de las estadisticas/datos del jugador")]
    [SerializeField] private ScriptableInventario inv;
    [SerializeField] private ScriptableAtaqueDistancia disparos;
    [SerializeField] private ScriptableVida vidaGerard;
    [SerializeField] private ScriptableVida vidaPau;




    [Header("Escriptable para saber quien entra al paki")]
    [SerializeField] private ScriptableInfoTP siTP;

    [Header("Fotos Player's")]
    [SerializeField] GameObject foto;
    [SerializeField] private Sprite gerard;
    [SerializeField] private Sprite pau;

    [Header("Conjunto Textos Player, paki")]
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject paki;

    [Header("Textos Player, paki")]
    [SerializeField] private TMPro.TextMeshProUGUI txtPlayer;
    [SerializeField] private TMPro.TextMeshProUGUI txtPaki;



    [Header("Recuadro guardar Partida")]
    [SerializeField] private GameObject guardarPartida;


    [Header("Sistema de Dialogos")] 
    [SerializeField] private string[] frasesPlayer;
    [SerializeField] private string[] frasesPaki;
    [SerializeField] private float velocidadTexto;


    private Coroutine corrutinaPlayer;
    private Coroutine corrutinaPaki;

    [SerializeField]
    private int posicionFrase;


    [Header("EVENTOS")]
    [SerializeField] private GameEvent eventoVolverDelPaki;

    private void Awake()
    {
        posicionFrase = Random.Range(0, frasesPlayer.Length);
        ResetearDatosJugador(); // rellenamos todos los monsters/curaciones los disparos disponibles y la vida del jugador
        IniciarEscenaPaki();
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            velocidadTexto = 0.02f;
        else if(Input.GetMouseButtonUp(0))
            velocidadTexto = 0.1f;
    }

    // Funciones
    public void ResetearDatosJugador()
    {
        inv.curacion = inv.maxCuracion;
        disparos.numeroAtaquesAct = disparos.numeroAtaquesMax;
        vidaGerard.VidaActual = vidaGerard.VidaMaxima;
        vidaPau.VidaActual = vidaPau.VidaMaxima;
    }

    private void IniciarEscenaPaki()
    {
        txtPlayer.text = "";
        txtPaki.text = "";
        if (siTP.quienEntra)
            foto.GetComponent<Image>().sprite = gerard;
        else
            foto.GetComponent<Image>().sprite = pau;
        player.SetActive(true);
        paki.SetActive(false);
        guardarPartida.SetActive(false);
        corrutinaPlayer = StartCoroutine(EscribirLetrasPlayer());
    }


    private void IniciarDialogoPaki()
    {
        player.SetActive(false);
        paki.SetActive(true);
        corrutinaPaki = StartCoroutine(EscribirLetrasPaki());
    }
     private void MostrarMenuGuardarPartida()
    {
        player.SetActive(false);
        paki.SetActive(false);
        guardarPartida.SetActive(true);
    }


    // Corrutinas
    // posicionFrase
    private IEnumerator EscribirLetrasPlayer()
    {
        foreach (char l in frasesPlayer[posicionFrase].ToCharArray())
        {
            txtPlayer.text += l;
            yield return new WaitForSeconds(velocidadTexto);
        }
        yield return new WaitForSeconds(1f);
        IniciarDialogoPaki();
    }

    private IEnumerator EscribirLetrasPaki()
    {
        foreach (char l in frasesPaki[posicionFrase].ToCharArray())
        {
            txtPaki.text += l;
            yield return new WaitForSeconds(velocidadTexto);
        }
        yield return new WaitForSeconds(1f);
        MostrarMenuGuardarPartida();
    }


    public void VolverDelPaki()
    {
        eventoVolverDelPaki.Raise();
    }

}
