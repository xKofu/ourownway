using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MGASetter : MonoBehaviour
{
    [SerializeField] private GameObject Obstaculo1_BloqueoPorHabilidad;
    [SerializeField] private GameObject HabilidadGerard1;
    [SerializeField] private GameObject HabilidadGerard2;
    [SerializeField] private GameObject HabilidadPau;
    [SerializeField] private GameObject PortalTrinidad;
    [SerializeField] private InformacionTranscursoPartida itp;

    void Start()
    {
        if (itp.Obstaculo1MGA)
            Obstaculo1_BloqueoPorHabilidad.SetActive(false);
                
        if (itp.PickedAb3G)
            HabilidadGerard1.SetActive(false);

        if (itp.PickedAb4G)
            HabilidadGerard2.SetActive(false);

        if (itp.PickedAb4P)
            HabilidadPau.SetActive(false);

        if (itp.bossMGADone)
            PortalTrinidad.SetActive(false);


    }
}
