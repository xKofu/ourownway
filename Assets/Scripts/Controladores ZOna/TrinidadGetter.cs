using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrinidadGetter : MonoBehaviour
{
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameEvent Aviso;
    public void SetInfo()
    {
        itp.Obstaculo2Sab = true;
        itp.bossMGADone = true;
        Aviso.Raise();
    }

}
