using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SergioGetter : MonoBehaviour
{
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameEvent Aviso;
    public void SetInfo()
    {
        itp.Obstaculo2CB = true;
        itp.bossCastellbisbalDone = true;
        itp.Atajo1CB = true;
        Aviso.Raise();
    }

}
