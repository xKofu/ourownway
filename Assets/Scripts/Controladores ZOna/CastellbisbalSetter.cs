using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastellbisbalSetter : MonoBehaviour
{
    [SerializeField] private GameObject Obstaculo1_Rocas;
    [SerializeField] private GameObject Obstaculo2_CaminoSabadell;
    [SerializeField] private GameObject Obstaculo3_CaminoMGA;
    [SerializeField] private GameObject HabilidadGerard;
    [SerializeField] private GameObject HabilidadPau;
    [SerializeField] private GameObject PortalSergio;
    [SerializeField] private GameObject AireAtajo;
    [SerializeField] private InformacionTranscursoPartida itp;
    private void Start()
    {
        if (itp.Obstaculo1CB)
            Obstaculo1_Rocas.SetActive(true);

        if (itp.Obstaculo2CB)
            Obstaculo2_CaminoSabadell.SetActive(false);

        if (itp.Obstaculo3CB)
            Obstaculo3_CaminoMGA.SetActive(false);

        if (itp.PickedAb1G)
            HabilidadGerard.SetActive(false);

        if (itp.PickedAb1P)
            HabilidadPau.SetActive(false);

        if (itp.bossCastellbisbalDone)
            PortalSergio.SetActive(false);

        if (itp.Atajo1CB)
            AireAtajo.SetActive(true);
    }
}
