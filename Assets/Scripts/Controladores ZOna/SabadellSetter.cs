using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SabadellSetter : MonoBehaviour
{
    [SerializeField] private GameObject Obstaculo1_CaminoHabilidades;
    [SerializeField] private GameObject Obstaculo2_PuertaFinal;
    [SerializeField] private GameObject HabilidadGerard;
    [SerializeField] private GameObject HabilidadPau1;
    [SerializeField] private GameObject HabilidadPau2;
    [SerializeField] private InformacionTranscursoPartida itp;

    void Start()
    {
        if (itp.Obstaculo1Sab)
            Obstaculo1_CaminoHabilidades.SetActive(false);

        if (itp.Obstaculo2Sab)
            Obstaculo2_PuertaFinal.SetActive(false);

        if (itp.PickedAb2P)
            HabilidadPau1.SetActive(false);

        if (itp.PickedAb3P)
            HabilidadPau2.SetActive(false);

        if (itp.PickedAb2G)
            HabilidadGerard.SetActive(false);

    }
}
