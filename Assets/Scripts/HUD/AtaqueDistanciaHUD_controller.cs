using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AtaqueDistanciaHUD_controller : MonoBehaviour
{
    [Header("Pelotas")]
    [SerializeField] private Image pelota1;
    [SerializeField] private Image pelota2;
    [SerializeField] private Image pelota3;


    [Header("Scriptable AtaqueDistancia")]
    [SerializeField] private ScriptableAtaqueDistancia sc_ataqueDistancia;




    public void QuitarPelota()
    {
        switch (sc_ataqueDistancia.numeroAtaquesAct)
        {
            case 0:
                pelota1.color = Color.gray;
                print("La ultima pelota bro");
                break;
            case 1:
                pelota2.color = Color.gray;
                print("Me quedan 1 pelotas");
                break;
            case 2:
                pelota3.color = Color.gray;
                print("Me quedan 2 pelotas");
                break;
        }
    }

    public void AddPelota()
    {
        switch (sc_ataqueDistancia.numeroAtaquesAct)
        {
            case 1:
                pelota1.color = Color.white;
                break;
            case 2:
                pelota2.color = Color.white;
                break;
            case 3:
                pelota3.color = Color.white;
                break;
        }
    }

}
