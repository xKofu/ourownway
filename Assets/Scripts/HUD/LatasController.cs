using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LatasController : MonoBehaviour
{

    [SerializeField] private ScriptableInventario Latas;
    [SerializeField] private TMPro.TextMeshProUGUI Texto;
    void Start()
    {
        Texto.text = Latas.curacion + "";
    }

    public void Actu()
    {
        GetComponent<Animator>().Play("USAR");
        Texto.text = Latas.curacion + "";
    }
}
