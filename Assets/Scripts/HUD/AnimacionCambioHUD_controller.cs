using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionCambioHUD_controller : MonoBehaviour
{

    [SerializeField]
    public GameObject go_pau;

    [SerializeField]
    public GameObject go_gerard;


    public void CambiarPau()
    {
        go_pau.transform.SetSiblingIndex(0);
    }

    public void CambiarGerard()
    {
        go_gerard.transform.SetSiblingIndex(0);
    }
}
