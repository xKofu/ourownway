using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Pausa_controller : MonoBehaviour
{

    [Header("Menu pausa")]
    [SerializeField] GameEvent ponerMenuPausa;
    [SerializeField] GameEvent quitarMenuPausa;

    [Header("Lo que tengo que parar")]
    [SerializeField] private GameObject personaje;

    private bool iniciar;

    void Start()
    {
        iniciar = false;
    }
    


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            MenuPausa();
    }

    private void MenuPausa()
    {
        if (!iniciar)   // Mostrar menu de pausa
            PonerMenuPausa();
        else            // Quitar menu de pausa
            QuitarMenuPausa();
    }

    public void QuitarMenuPausa()
    {
        quitarMenuPausa.Raise();
        iniciar = false;
        Time.timeScale = 1;
        personaje.GetComponent<CharacterController>().enabled = true;
        personaje.GetComponent<Animator>().enabled = true;
    }

    private void PonerMenuPausa()
    {
        ponerMenuPausa.Raise();
        iniciar = true;
        personaje.GetComponent<CharacterController>().enabled = false;
        personaje.GetComponent<Animator>().enabled = false;
        Time.timeScale = 0;
    }



}
