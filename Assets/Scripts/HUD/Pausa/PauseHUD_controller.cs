using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseHUD_controller : MonoBehaviour
{

    [SerializeField] private GameObject menuPausa;
    [SerializeField] private GameObject menuSeguroSalir;
    [SerializeField] private GameObject ocpionesPausa;

    [Header("Eventos")]
    [SerializeField] private GameEvent continuar;
    [SerializeField] private GameEvent eventoSalir;


    [Header("HUD")]
    [SerializeField] private Transform[] poscionesOpciones;
    [SerializeField] private Transform[] poscionesOpcionesSeguroSalir;
    [SerializeField] private GameObject selector1;
    [SerializeField] private GameObject selector2;



    private int id;
    private int id2;


    private bool menuNormlaActivo;

    private void Start()
    {
        menuNormlaActivo = true;
        menuPausa.SetActive(false);
        id = 0;
        id2 = 0;
        //selector1.transform.position = poscionesOpciones[id].position;
        //selector1.transform.position = poscionesOpcionesSeguroSalir[id2].position;
    }


    private void Update()
    {
        if (menuNormlaActivo)
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A))
                SubirSelector();
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
                BajarSelector();
            else if (Input.GetKeyDown(KeyCode.Return))
                SeleccionarOpcion();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A))
                SubirSelectorSeguroSalir();
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
                BajarSelectorSeguroSalir();
            else if (Input.GetKeyDown(KeyCode.Return))
                SeleccionarOpcionSeguroSalir();
        }
    }




    // SOLO LAS UTILIZAN LOS EVENTOS
    public void PonerMenuPausa()
    {
        id = 0;
        id2 = 0;
        selector1.transform.position = poscionesOpciones[id].position;
        selector1.transform.position = poscionesOpcionesSeguroSalir[id2].position;

        menuPausa.SetActive(true);          // PONER el menu en general
        menuSeguroSalir.SetActive(false);   // QUITAR el menu de seguro salir
        ocpionesPausa.SetActive(true);      // PONER los botones de opciones

        menuNormlaActivo = true;            // comprobacion de en que menu estoy (true = normal / false = seguro salir)
        selector1.SetActive(false);          // PONER selector menu normal
        selector2.SetActive(false);         // QUITAR selector menu seguro salir


    }
    public void QuitarMenuPausa()
    {
        menuPausa.SetActive(false);
    }


    // POR EL SELECTOR O CLICANDO
    public void PonerMenuSeguroSalir()
    {
        menuSeguroSalir.SetActive(true);
        ocpionesPausa.SetActive(false);
        menuNormlaActivo = false;
        selector1.SetActive(false);
        selector2.SetActive(true);
    }
    public void QuitarMenuSeguroSalir()
    {
        menuSeguroSalir.SetActive(false);
        ocpionesPausa.SetActive(true);
        menuNormlaActivo = true;
        selector1.SetActive(true);
        selector2.SetActive(false);
    }



    // LAS DIFERENTES OPCINES
    public void Continuar()
    {
        continuar.Raise();
    }

    public void Opciones()
    {

    }
    public void Salir()
    {
        eventoSalir.Raise();
    }


    // SELECTOR
    private void SubirSelector()
    {
        id--;
        if (id <= 0)
            id = 0;
        selector1.SetActive(true);
        selector1.transform.position = poscionesOpciones[id].position;

    }

    private void BajarSelector()
    {
        id++;
        if (id >= 2)
            id = 2;
        selector1.SetActive(true);
        selector1.transform.position = poscionesOpciones[id].position;
    }


    private void SeleccionarOpcion()
    {
        switch (id)
        {
            case 0:
                Continuar();
                break;
            case 1:
                Opciones();
                break;
            case 2:
                PonerMenuSeguroSalir();
                break;
        }
    }


    private void SubirSelectorSeguroSalir()
    {
        id2--;
        if (id2 <= 0)
            id2 = 0;
        selector2.transform.position = poscionesOpcionesSeguroSalir[id2].position;
    }

    private void BajarSelectorSeguroSalir()
    {
        id2++;
        if (id2 >= 1)
            id2 = 1;
        selector2.transform.position = poscionesOpcionesSeguroSalir[id2].position;
    }

    private void SeleccionarOpcionSeguroSalir()
    {
        switch (id2)
        {
            case 0:
                Salir();
                break;
            case 1:
                QuitarMenuSeguroSalir();
                break;
        }
    }


    public void EntraRatonBotonContinuar()
    {
        id = 0;
        selector1.SetActive(true);
        selector1.transform.position = poscionesOpciones[id].position;
    }

    public void EntraRatonBotonOpciones()
    {
        id = 1;
        selector1.SetActive(true);
        selector1.transform.position = poscionesOpciones[id].position;
    }

    public void EntraRatonBotonSalir()
    {
        id = 2;
        selector1.SetActive(true);
        selector1.transform.position = poscionesOpciones[id].position;
    }
    public void EntraRatonBotonSalirDeVerdad()
    {
        id2 = 0;
        selector2.transform.position = poscionesOpcionesSeguroSalir[id2].position;
    }

    public void EntraRatonBotonCancelar()
    {
        id2 = 1;
        selector2.transform.position = poscionesOpcionesSeguroSalir[id2].position;
    }
}
