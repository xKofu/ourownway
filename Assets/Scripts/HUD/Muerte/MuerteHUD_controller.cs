using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteHUD_controller : MonoBehaviour
{
    [Header("EVENTOS")]
    [SerializeField] private GameEvent continuar;
    [SerializeField] private GameEvent salir;

    [Header("HUD muerte")]
    [SerializeField] private GameObject conjuntoMuerte;
    [SerializeField] private GameObject selector;
    [SerializeField] private Transform[] poscionBotones;
    private int id;


    private void Start()
    {
        conjuntoMuerte.SetActive(false);
        id = 0;
        selector.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A))
            SubirSelector();
        else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
            BajarSelector();
        else if (Input.GetKeyDown(KeyCode.Return))
            SeleccionarOpcion();
    }


    public void ActivarMenuMuerte()
    {
        conjuntoMuerte.SetActive(true);
        selector.SetActive(false);
    }

    public void Continuar()
    {
        continuar.Raise(); // tiro evento de continuar, es decir, que me voy al paki
    }

    public void Salir()
    {
        salir.Raise();
    }


    // SELECTOR
    private void SubirSelector()
    {
        id--;
        if (id <= 0)
            id = 0;
        selector.SetActive(true);
        selector.transform.position = poscionBotones[id].position;

    }

    private void BajarSelector()
    {
        id++;
        if (id >= 1)
            id = 1;
        selector.SetActive(true);
        selector.transform.position = poscionBotones[id].position;
    }

    private void SeleccionarOpcion()
    {
        switch (id)
        {
            case 0:
                Continuar();
                break;
            case 1:
                Salir();
                break;
        }
    }

    public void EntraRatonBotonContinuar()
    {
        id = 0;
        selector.SetActive(true);
        selector.transform.position = poscionBotones[id].position;
    }

    public void EntraRatonBotonSalir()
    {
        id = 1;
        selector.SetActive(true);
        selector.transform.position = poscionBotones[id].position;
    }



}
