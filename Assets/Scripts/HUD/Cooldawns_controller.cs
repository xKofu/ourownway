using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cooldawns_controller : MonoBehaviour
{
    [Header("Foto")]
    [SerializeField]
    private Image foto;

    [Header("Cooldown")]
    [SerializeField]
    private ScriptableCooldown sc_habilidad;

    [Header("Evento 'que lanza' ")]
    [SerializeField]
    private GameEvent ge_habilidad;


    [Header("Cargar al reves")]
    [SerializeField]
    private bool cargaBien = true;

    void Start()
    {
        if (cargaBien)
            foto.fillAmount = 1;
        else
            foto.fillAmount = 0;

        sc_habilidad.tiempoActual = 0;
        sc_habilidad.tiempoActual = 0;
    }

    public void EmpezarCooldown()
    {
        StartCoroutine(CooldownPositivo(sc_habilidad, foto, ge_habilidad));
    }
    public void EmpezarCooldownInverso()
    {
        StartCoroutine(RellenarAlReves(sc_habilidad, foto, ge_habilidad));
    }


    private IEnumerator CooldownNegativo(ScriptableCooldown hb, Image img, GameEvent ge)
    {
        bool flag = true;
        while (flag)
        {
            img.fillAmount -= (0.1f / hb.tiempoTotal);
            yield return new WaitForSeconds(0.1f);
            if (hb.tiempoActual >= hb.tiempoTotal)
                flag = false;
        }
    }

    private IEnumerator CooldownPositivo(ScriptableCooldown hb, Image img, GameEvent ge)
    {
        bool flag = true;

        img.fillAmount = 0;


        while (flag)
        {
            img.fillAmount += (0.1f / hb.tiempoTotal);
            yield return new WaitForSeconds(0.1f);
            if (hb.tiempoActual >= hb.tiempoTotal)
                flag = false;

        }

        hb.tiempoActual = 0;

        if (!cargaBien)
            img.fillAmount = 0;

    }

    private IEnumerator RellenarAlReves(ScriptableCooldown hb, Image img, GameEvent ge)
    {
        bool flag = true;
        img.fillAmount = 1;
        while (flag)
        {
            img.fillAmount -= (0.1f / hb.tiempoTotal);
            yield return new WaitForSeconds(0.1f);
            if (hb.tiempoActual >= hb.tiempoTotal)
                flag = false;

        }

        hb.tiempoActual = 0;

        if (!cargaBien)
            img.fillAmount = 0;

    }
}
