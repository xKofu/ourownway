using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_Proyectil;
    [SerializeField]
    private int m_poolSize = 50;

    private List<GameObject> myPool;
    private List<GameObject> myAvailable;
    private List<GameObject> myGiven;

    // Start is called before the first frame update
    void Start()
    {
        myPool = new List<GameObject>();
        myAvailable = new List<GameObject>();
        myGiven = new List<GameObject>();

        for (int i = 0; i < m_poolSize; i++)
        {
            for(int j = 0; j < m_Proyectil.Length; j++)
            {
                GameObject standard = Instantiate(m_Proyectil[j], gameObject.transform);
                standard.SetActive(false);
                myPool.Add(standard);
                myAvailable.Add(standard);
            }
        }


    }

    public GameObject getElement()
    {
        GameObject element = null;

        if(myAvailable.Count > 0)
        {
            element = myAvailable[0];
            myGiven.Add(element);
            myAvailable.RemoveAt(0);
            element.SetActive(true);
        }else
        {
            element = myGiven[0];
            myGiven.RemoveAt(0);
            myGiven.Add(element);
            element.SetActive(true);
        }

        return element;
    }

    public void ReturnElement(GameObject go)
    {

        if(myPool.Contains(go) && !myAvailable.Contains(go))
        {
            go.SetActive(false);
            myGiven.Remove(go);
            myAvailable.Add(go);
        }
    }

    public void ReturnElement(GameObject go, float tiempo)
    {
        StartCoroutine(DelayedReturn(go, tiempo));
    }

    private IEnumerator DelayedReturn(GameObject go, float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        ReturnElement(go);
    }

}
