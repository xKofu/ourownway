using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    [SerializeField] private string Destino;

    private void Awake()
    {
        GetComponent<Animator>().Play("SceneIn");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
            StartCoroutine(CambiarEscena(Destino));
    }


    public void LlamarCambioEscena(string escena)
    {
        StartCoroutine(CambiarEscena(escena));
    }

    IEnumerator CambiarEscena(string Destino)
    {
        GetComponent<Animator>().Play("SceneOut");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(Destino);
    }

}
