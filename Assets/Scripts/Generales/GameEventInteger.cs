using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GameEvents/GameEventInteger")]
public class GameEventInteger : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GameEventListenerInteger> eventListeners =
        new List<GameEventListenerInteger>();

    public void Raise(int danyo)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(danyo);
    }

    public void RegisterListener(GameEventListenerInteger listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerInteger listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

