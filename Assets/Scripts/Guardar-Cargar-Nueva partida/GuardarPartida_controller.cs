using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GuardarPartida_controller : MonoBehaviour
{

    [SerializeField] private ScriptableInfoTP siTP;
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private DatosGuardarPartida dgp;




    public void Save()
    {
        // Guardamos las habilidades (si las tienen o no)
        dgp.habilidad1Pau = itp.habilidad1Pau;
        dgp.habilidad2Pau = itp.habilidad2Pau;
        dgp.habilidad3Pau = itp.habilidad3Pau;
        dgp.habilidad4Pau = itp.habilidad4Pau;

        dgp.habilidad1Gerard = itp.habilidad1Gerard;
        dgp.habilidad2Gerard = itp.habilidad2Gerard;
        dgp.habilidad3Gerard = itp.habilidad3Gerard;
        dgp.habilidad4Gerard = itp.habilidad4Gerard;


        // Guardamos los datos de cuando entramos al paqui
        dgp.EscenaOrigen = siTP.EscenaOrigen;
        dgp.numTP = siTP.numTP;
        dgp.posSpawn = siTP.posSpawn;
        dgp.quienEntra = siTP.quienEntra;

        // fuardamos los datos de Cada zona
        // CB
        dgp.bossCastellbisbalDone = itp.bossCastellbisbalDone;
        dgp.PickedAb1G = itp.PickedAb1G;
        dgp.PickedAb1P = itp.PickedAb1P;
        //dgp.Obstaculo1CB = itp.Obstaculo1CB;
        //dgp.Obstaculo2CB = itp.Obstaculo2CB;
        //dgp.Obstaculo3CB = itp.Obstaculo3CB;

        // MGA
        dgp.bossMGADone = itp.bossMGADone;
        
        // Sabadell
        dgp.bossSabadellDone = itp.bossSabadellDone;
        dgp.PickedAb2G = itp.PickedAb2G;
        //dgp.PickedAb2P = itp.PickedAb2P;



        string jsonStr = JsonUtility.ToJson(dgp);
        File.WriteAllText("savegame.json", jsonStr);

        print("Se a GUARDADO la partida");
    }

    public void Load()
    {
        string jsonStr = File.ReadAllText("savegame.json");
        JsonUtility.FromJsonOverwrite(jsonStr, dgp);
        
        // Cargamos las habilidades (si las tienen o no)
        itp.habilidad1Pau = dgp.habilidad1Pau;
        itp.habilidad2Pau = dgp.habilidad2Pau;
        itp.habilidad3Pau = dgp.habilidad3Pau;
        itp.habilidad4Pau = dgp.habilidad4Pau;

        itp.habilidad1Gerard = dgp.habilidad1Gerard;
        itp.habilidad2Gerard = dgp.habilidad2Gerard;
        itp.habilidad3Gerard = dgp.habilidad3Gerard;
        itp.habilidad4Gerard = dgp.habilidad4Gerard;


        // Cargamos los datos de cuando entramos al paqui
        siTP.EscenaOrigen = dgp.EscenaOrigen;
        siTP.numTP = dgp.numTP;
        siTP.posSpawn = dgp.posSpawn;
        siTP.quienEntra = dgp.quienEntra;


        //itp.bossCastellbisbal = dgp.bossCastellbisbal;
        //itp.bossCerdanyola = dgp.bossCerdanyola;
        //itp.bossSabadell = dgp.bossSabadell;


        print("Se a CARGADO una partida");
    }
}
