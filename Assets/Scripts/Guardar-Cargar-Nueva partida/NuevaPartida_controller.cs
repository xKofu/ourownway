using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuevaPartida_controller : MonoBehaviour
{

    [Header("Nueva Partida")]
    [SerializeField] private ScriptableAtaqueDistancia sad;
    [SerializeField] private ScriptableVida svGerard;
    [SerializeField] private ScriptableVida svPau;
    [SerializeField] private ScriptableInventario si;
    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private ScriptableInfoTP siTP;
    [SerializeField] private ChangeEscena_controller cec;

    public void NewGame()
    {
        itp.habilidad1Pau = false;
        itp.habilidad2Pau = false;
        itp.habilidad3Pau = false;
        itp.habilidad4Pau = false;

        itp.habilidad1Gerard = false;
        itp.habilidad2Gerard = false;
        itp.habilidad3Gerard = false;
        itp.habilidad4Gerard = false;

        siTP.EscenaOrigen = "Castellbisbal";
        siTP.numTP = 0;
        siTP.posSpawn = new Vector3(0, 0, 0);
        siTP.quienEntra = false;

        itp.bossCastellbisbalDone = false;
        itp.PickedAb1G = false;
        itp.PickedAb1P = false;
        itp.Obstaculo1CB = false;
        itp.Obstaculo2CB = false;
        itp.Obstaculo3CB = false;
        itp.Atajo1CB = false;
        
        itp.bossSabadellDone = false;
        itp.PickedAb2G = false;
        itp.PickedAb2P = false;
        itp.PickedAb3P = false;
        itp.Obstaculo1Sab = false;
        itp.Obstaculo2Sab = false;

        itp.bossMGADone = false;
        itp.PickedAb3G = false;
        itp.PickedAb4G = false;
        itp.PickedAb4P = false;
        itp.Obstaculo1MGA = false;

        ////////////////////////////////////
        sad.numeroAtaquesAct = 3;
        sad.numeroAtaquesMax = 3;

        svGerard.VidaActual = 100;
        svGerard.VidaMaxima = 100;
        svPau.VidaActual = 100;
        svPau.VidaMaxima = 100;

        si.curacion = 5;
        si.maxCuracion = 5;

        cec.SeleccionarDestino(siTP.EscenaOrigen);
    }
}
