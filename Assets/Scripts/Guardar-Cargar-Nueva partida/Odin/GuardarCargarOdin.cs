using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OdinSerializer;
using System.IO;


public class GuardarCargarOdin : MonoBehaviour
{
    [Header("Scriptable de GUARDAR PARTIDA")]
    [SerializeField] private GuardarOdin datos;

    [SerializeField] private ChangeEscena_controller cec;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            Load();
        else if (Input.GetKeyDown(KeyCode.G))
            Save();
    }

    public void Save()
    {
        datos.seHaGuardadoPartida = true;
        byte[] serializedData = SerializationUtility.SerializeValue<GuardarOdin>(datos, DataFormat.JSON);
        File.WriteAllBytes("guardarOdin.json", serializedData);
        print("Se a GUARDADO la partida");
    }

    public void Load()
    {
        //if (!datos.seHaGuardadoPartida) // Si todabia no hay una partida guardada no podemos cargar nada
        //return;

        if (!File.Exists("guardarOdin.json")) // Si no existe es que no se a guardado una partida todavia
            return;

        byte[] postFile = File.ReadAllBytes("guardarOdin.json");
        GuardarOdin datosGuardados = SerializationUtility.DeserializeValue<GuardarOdin>(postFile, DataFormat.JSON);

        // DATOS ORIGEN TP
        datos.siTP.EscenaOrigen = datosGuardados.siTP.EscenaOrigen;
        datos.siTP.numTP = datosGuardados.siTP.numTP;
        datos.siTP.posSpawn = datosGuardados.siTP.posSpawn;
        datos.siTP.quienEntra = datosGuardados.siTP.quienEntra;

        // Informacion Transcuros Partida
        {
            // HABILIDADES PERSONAJES
            datos.itp.habilidad1Pau = datosGuardados.itp.habilidad1Pau;
            datos.itp.habilidad2Pau = datosGuardados.itp.habilidad2Pau;
            datos.itp.habilidad3Pau = datosGuardados.itp.habilidad3Pau;
            datos.itp.habilidad4Pau = datosGuardados.itp.habilidad4Pau;
            datos.itp.habilidad1Gerard = datosGuardados.itp.habilidad1Gerard;
            datos.itp.habilidad2Gerard = datosGuardados.itp.habilidad2Gerard;
            datos.itp.habilidad3Gerard = datosGuardados.itp.habilidad3Gerard;
            datos.itp.habilidad4Gerard = datosGuardados.itp.habilidad4Gerard;
        }
        {
            // BOSSES
            datos.itp.bossCastellbisbalDone = datosGuardados.itp.bossCastellbisbalDone;
            datos.itp.bossMGADone = datosGuardados.itp.bossMGADone;
            datos.itp.bossSabadellDone = datosGuardados.itp.bossSabadellDone;
        }
        {
            // CB
            datos.itp.PickedAb1G = datosGuardados.itp.PickedAb1G;
            datos.itp.PickedAb1P = datosGuardados.itp.PickedAb1P;
            datos.itp.Obstaculo1CB = datosGuardados.itp.Obstaculo1CB;
            datos.itp.Obstaculo2CB = datosGuardados.itp.Obstaculo2CB;
            datos.itp.Obstaculo3CB = datosGuardados.itp.Obstaculo3CB;
            datos.itp.Atajo1CB = datosGuardados.itp.Atajo1CB;
        }
        {
            // Sabadell
            datos.itp.PickedAb2P = datosGuardados.itp.PickedAb2P;
            datos.itp.PickedAb2G = datosGuardados.itp.PickedAb2G;
            datos.itp.PickedAb3P = datosGuardados.itp.PickedAb3P;
            datos.itp.Obstaculo1Sab = datosGuardados.itp.Obstaculo1Sab;
            datos.itp.Obstaculo2Sab = datosGuardados.itp.Obstaculo2Sab;
        }
        {
            // MGA
            datos.itp.PickedAb3G = datosGuardados.itp.PickedAb3G;
            datos.itp.PickedAb4G = datosGuardados.itp.PickedAb4G;
            datos.itp.PickedAb4P = datosGuardados.itp.PickedAb4P;
            datos.itp.Obstaculo1MGA = datosGuardados.itp.Obstaculo1MGA;
        }

        cec.IrPaki();
        print("Se a CARGADO la partida");
    }
}
