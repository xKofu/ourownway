using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HitParticleEnablerBall : MonoBehaviour
{

    [SerializeField] private GameObject Hit;
    [SerializeField] private AudioSource AudioPlayer;
    [SerializeField] private AudioClip BallHitSound;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 16 || collision.gameObject.layer == 17)
        {
            HitParticle();
        }
    }

    private void HitParticle()
    {
        GameObject go = Instantiate(Hit);
        go.GetComponent<VisualEffect>().Play();
        go.transform.position = transform.position;
        AudioPlayer.PlayOneShot(BallHitSound);
    }

}
