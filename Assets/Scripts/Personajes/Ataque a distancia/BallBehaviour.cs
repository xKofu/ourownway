using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    public delegate void Despawn(GameObject go);
    public event Despawn OnDespawn;

    private Vector3 Objetivo;
    private float speed = 7f;

    public void Shoot()
    {
        GetComponent<Rigidbody2D>().AddForce((Objetivo - transform.position).normalized * speed * 75);
        StartCoroutine(DespawnC(5f));
    }
    
    public void SetObjetivo(Vector3 pos)
    {
        Objetivo = pos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(Disable());
        StartCoroutine(DespawnC(2f));
    }

    IEnumerator Disable()
    {
        yield return new WaitForSeconds(0.2f);
        GetComponent<CircleCollider2D>().enabled = false;
    }
    IEnumerator DespawnC(float time)
    {
        yield return new WaitForSeconds(time);
        OnDespawn(this.gameObject);
    }

}
