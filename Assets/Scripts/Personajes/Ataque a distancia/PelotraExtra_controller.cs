using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotraExtra_controller : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Eliminar());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            StopAllCoroutines();
            Destroy(this.gameObject);
        }
    }

    private IEnumerator Eliminar()
    {
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }


}
