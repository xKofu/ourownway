using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PjTP : MonoBehaviour
{
    [SerializeField]
    private ScriptableInfoTP infoTp;

    [SerializeField]
    private GameEvent IrPaki, PonerCamara;

    private void Awake()
    {
        switch (infoTp.numTP)
        {
            case 1:
            case 2:
            case 3:
                this.gameObject.transform.position = infoTp.posSpawn;
                PonerCamara.Raise();
                break;

            default:
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "tp")
        {
            //infoTp.numTP = collision.gameObject.GetComponent<TpController>().GetnumTP();
            //infoTp.EscenaOrigen = SceneManager.GetActiveScene().name;
            //infoTp.posSpawn = new Vector3(collision.gameObject.GetComponent<TpController>().GetReferencia().position.x, collision.gameObject.GetComponent<TpController>().GetReferencia().position.y, 0);

            IrPaki.Raise();
        }
    }
}
