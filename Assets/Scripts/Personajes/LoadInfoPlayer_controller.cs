using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadInfoPlayer_controller : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private GameObject player;

    [Header("Informacion")]
    [SerializeField] private ScriptableInfoTP sitp;
    [SerializeField] private InformacionTranscursoPartida itp;

    [Header("Cosas que se tienen que activar")]
    [SerializeField] private GameObject pausa;


    private bool[] mirarHabilidades = new bool[8];

    public void LoadInfoPlayer()
    {
        // Activar lo que es necesario
        player.GetComponent<CharacterController>().enabled = true;
        pausa.GetComponent<Pausa_controller>().enabled = true;


        // Relacionado con el PLAYER
        {
            // POSICION
            player.transform.position = sitp.posSpawn;

            // QUIEN SOY
            if (sitp.quienEntra)
                player.GetComponent<CharacterController>().LoadGerard();
            else
                player.GetComponent<CharacterController>().LoadPau();

            // HABILIDADES
            mirarHabilidades[0] = itp.habilidad1Pau;
            mirarHabilidades[1] = itp.habilidad2Pau;
            mirarHabilidades[2] = itp.habilidad3Pau;
            mirarHabilidades[3] = itp.habilidad4Pau;

            mirarHabilidades[4] = itp.habilidad1Gerard;
            mirarHabilidades[5] = itp.habilidad2Gerard;
            mirarHabilidades[6] = itp.habilidad3Gerard;
            mirarHabilidades[7] = itp.habilidad4Gerard;
            player.GetComponent<CharacterController>().LodInfoHabilidades(mirarHabilidades);
        }
    }

    public void LoadInfoPlayer_BossSergio()
    {

        // Relacionado con el PLAYER
        {
            // QUIEN SOY
            player.GetComponent<CharacterController>().LoadPau();

            // HABILIDADES
            mirarHabilidades[0] = itp.habilidad1Pau;
            mirarHabilidades[1] = itp.habilidad2Pau;
            mirarHabilidades[2] = itp.habilidad3Pau;
            mirarHabilidades[3] = itp.habilidad4Pau;

            mirarHabilidades[4] = itp.habilidad1Gerard;
            mirarHabilidades[5] = itp.habilidad2Gerard;
            mirarHabilidades[6] = itp.habilidad3Gerard;
            mirarHabilidades[7] = itp.habilidad4Gerard;
            player.GetComponent<CharacterController>().LodInfoHabilidades(mirarHabilidades);
        }
    }

    public void LoadInfoPlayer_BossCerdanyola()
    {

        // Relacionado con el PLAYER
        {
            // QUIEN SOY
            player.GetComponent<CharacterController>().LoadPau();

            // HABILIDADES
            mirarHabilidades[0] = itp.habilidad1Pau;
            mirarHabilidades[1] = itp.habilidad2Pau;
            mirarHabilidades[2] = itp.habilidad3Pau;
            mirarHabilidades[3] = itp.habilidad4Pau;

            mirarHabilidades[4] = itp.habilidad1Gerard;
            mirarHabilidades[5] = itp.habilidad2Gerard;
            mirarHabilidades[6] = itp.habilidad3Gerard;
            mirarHabilidades[7] = itp.habilidad4Gerard;
            player.GetComponent<CharacterController>().LodInfoHabilidades(mirarHabilidades);
        }
    }


    public void LoadInfoPlayer_BossSabadell()
    {

        // Relacionado con el PLAYER
        {
            // QUIEN SOY
            player.GetComponent<CharacterController>().LoadPau();

            // HABILIDADES
            /*
            mirarHabilidades[0] = itp.habilidad1Pau;
            mirarHabilidades[1] = itp.habilidad2Pau;
            mirarHabilidades[2] = itp.habilidad3Pau;
            mirarHabilidades[3] = itp.habilidad4Pau;

            mirarHabilidades[4] = itp.habilidad1Gerard;
            mirarHabilidades[5] = itp.habilidad2Gerard;
            mirarHabilidades[6] = itp.habilidad3Gerard;
            mirarHabilidades[7] = itp.habilidad4Gerard;
            */

            mirarHabilidades[0] = true;
            mirarHabilidades[1] = true;
            mirarHabilidades[2] = true;
            mirarHabilidades[3] = true;

            mirarHabilidades[4] = true;
            mirarHabilidades[5] = true;
            mirarHabilidades[6] = true;
            mirarHabilidades[7] = true;
            player.GetComponent<CharacterController>().LodInfoHabilidades(mirarHabilidades);
        }
    }
}
