using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CharacterController : MonoBehaviour
{
    
    [Header("Estadisticas Personaje")]
    [SerializeField] private ScriptableEstadisticas se_CharStats;
    [SerializeField] private ScriptableVida sv_VidaPau;
    [SerializeField] private ScriptableVida sv_VidaGerard;
    [SerializeField] private float velocity;
    [SerializeField] private float damage;
    [SerializeField] private float jumpForce = 3.5f;
    [SerializeField] private float g_Life;
    [SerializeField] private float g_LifeMax;
    [SerializeField] private float p_Life;
    [SerializeField] private float p_LifeMax;

    [Header("Numeritos y Ayudas")]
    [SerializeField] private bool imGrounded;
    [SerializeField] private bool stoppedJump = true;
    [SerializeField] private bool CanMove;
    [SerializeField] private bool CanJump;
    [SerializeField] private bool CanChangeView;
    [SerializeField] private float jumpTimeCounter;
    [SerializeField] private float jumpMaxTime = 0.3f;
    [SerializeField] private Vector3 mousePos;
    [SerializeField] private Transform posDisparo;
    [SerializeField] private LayerMask WhatIsGround;
    [SerializeField] private Transform centroBox;
    [SerializeField] private Transform PosLata;
    [SerializeField] private Animator ActualAnimator;
    [SerializeField] private GameObject Bubble;
    [SerializeField] private GameObject Paravela;
    [SerializeField] private GameObject ComprobantePlaneo;
    private float anchoBox;

    private enum State { MOVIMIENTO, IDLE, AIRE, DASH, DJUMP, PLANEO, GANCHO_1, GANCHO_2, CURAR, CAMBIO, ON_HIT, ATACAR };
    private State m_state;

    public string Estado;
    
    private enum Direccion { DERECHA, IZQUIERDA };
    private Direccion view_dir;

    private enum Identidad { PAU, GERARD };
    private Identidad m_Identity;



    bool[] activeAbilities = new bool[11];
    bool[] unlockedAbilities = new bool[8];

    //public List<string> Collisions;

    [Header("Skins")]
    [SerializeField] private GameObject SkinPau;
    [SerializeField] private GameObject SkinGerard;

    [Header("Gerard Game Events")]
    [SerializeField] private GameEvent gerard_habilidad1_Planear;
    [SerializeField] private GameEvent gerard_habilidad2_Grito;
    [SerializeField] private GameEvent gerard_habilidad3_Gancho;
    [SerializeField] private GameEvent gerard_habilidad4_CrearPlataforma;

    [Header("Gerard  Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad1_Planear;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad2_Grito;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad3_Gancho;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad4_CrearPlataforma;
    
    [Header("Pau Game Events")]
    [SerializeField] private GameEvent pau_habilidad1_Dash;
    [SerializeField] private GameEvent pau_habilidad2_DoubleJump;
    [SerializeField] private GameEvent pau_habilidad3_Inmunidad;
    [SerializeField] private GameEvent pau_habilidad4_Combate;

    [Header("Pau Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown pau_sc_habilidad1_Dash;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad2_DoubleJump;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad3_Inmunidad;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad4_Combate;
    
    [Header("Acciones Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown sc_disparar;
    [SerializeField] private ScriptableCooldown sc_heal;
    [SerializeField] private ScriptableCooldown sc_change;

    [Header("Scriptable Curaciones")]
    [SerializeField] private ScriptableInventario si_heals;

    [Header("Scriptable Ataque a Distancia")]
    [SerializeField] private ScriptableAtaqueDistancia sad_ataqueDistancia;

    [Header("Datos Habilidades")]
    [SerializeField] private GameObject ScreamArea;
    [SerializeField] private float nonGravityVelocity;
    [SerializeField] private bool activeDJump;
    [SerializeField] private float FallVelocity;

    [Header("Pools")]
    [SerializeField] private Pool platPool;
    [SerializeField] private Pool ballPool;

    [Header("Prefabs")]
    [SerializeField] private GameObject lata;

    [Header("Cambio de personaje")]
    [SerializeField] private bool canChangeEvent;
    private Coroutine corrutinaCuracionPasiva;

    [Header("Events")]
    [SerializeField] private GameEvent ShootEvent;
    [SerializeField] private GameEvent AddShootEvent;
    [SerializeField] private GameEvent DrinkEvent;
    [SerializeField] private GameEvent eventoCambiarVidaHUD;
    [SerializeField] private GameEvent eventoMorirse;
    [SerializeField] private GameEvent changeToGerard, changeToPau;

    [Header("Controladores de Ataque")]
    [SerializeField] private bool toSoon;
    private enum CombatState { NON, HIT1, HIT2, HIT3 };
    [SerializeField] private CombatState BattleState;

    [Header("Utiles Gancho")]
    [SerializeField] private LineRenderer Brazo;
    [SerializeField] private LayerMask grappableMask;
    [SerializeField] private float maxDistanceGrap;
    private RaycastHit2D GrabHit;
    [SerializeField] private Transform PosSalidaBrazo;
    [SerializeField] private GameObject BrazoSkin;
    [SerializeField] private GameObject ManoFinal;

    [Header("Utilies habilidades 3 y 4 Pau")]
    [SerializeField] private SpriteRenderer[] invulnerabilidad;
    [SerializeField] private SpriteRenderer[] enfadado;
    [SerializeField] private GameObject vfxEnfado;


    private bool quienTengoQueSer; // true => gerard // false => pau
    private bool morirGerardInstantaneo;    // Si esto esa a true si el personaje del gerard se muere nos morimos directamente sin el forzar cambio por vida
    private bool morirPauInstantaneo;       // Si esto esa a true si el personaje del pau se muere nos morimos directamente sin el forzar cambio por vida


    public bool JumpCanceled;
        
    [Header("Efectos de Sonido")]
    [SerializeField] private AudioSource SoundPlayer;
    [SerializeField] private AudioClip s_Paravela;
    [SerializeField] private AudioClip s_Burbuja;
    [SerializeField] private AudioClip s_Dash;
    [SerializeField] private AudioClip s_Invulnerabilidad;
    [SerializeField] private AudioClip[] s_Grito = new AudioClip[5];
    [SerializeField] private AudioClip s_Plataforma;
    [SerializeField] private AudioClip s_Buff;
    [SerializeField] private AudioClip s_Gancho;
    [SerializeField] private AudioClip s_Salto;
    [SerializeField] private AudioClip s_Cambio;
    [SerializeField] private AudioClip s_Curar;

    void Start()
    {
        JumpCanceled = false;
        // Revisar esto
        LoadPau();
        view_dir = Direccion.DERECHA;
        m_state = State.IDLE;
        imGrounded = false;
        CanMove = true;
        CanChangeView = true;
        UpdateStats();
        for(int i = 0; i < activeAbilities.Length; i++)
            activeAbilities[i] = true;
        
        for (int i = 0; i < unlockedAbilities.Length; i++)
            unlockedAbilities[i] = false;
        
        // IMPORTANTE, ESTO ESTA EN TRUE PARA PODER USAR LAS HABILIDADES Y HACER TEST, ESTO DEBERIA ESTAR EN FALSE EN SU VERSION FINAL

        sad_ataqueDistancia.numeroAtaquesAct = sad_ataqueDistancia.numeroAtaquesMax;
        si_heals.curacion = si_heals.maxCuracion;
        canChangeEvent = true;
        toSoon = false;
        se_CharStats.da�o = 10;
        se_CharStats.velocidad = 5;
        eventoCambiarVidaHUD.Raise();
        morirGerardInstantaneo = false;
        morirPauInstantaneo = false;
    }

    private void UpdateStats()
    {
        velocity = se_CharStats.velocidad;
        damage = se_CharStats.da�o;
    }
    
    void Update()
    {
        Estado = m_state.ToString();
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;

        if (CanChangeView)
            ViewHandler();

        ActionHandler();

        if (CanMove)
        {
            MovementHandler();
            if (!JumpCanceled)
                if (CanJump)
                    Jump();
        }
        
        StateOnUpdate();
    }

    void FixedUpdate()
    {
        CheckCollisions();
    }

    void CheckCollisions()
    {        
        anchoBox = GetComponent<CapsuleCollider2D>().size[0] * .5f / 2;
        imGrounded = Physics2D.OverlapCircle(centroBox.position, anchoBox, WhatIsGround);
    }
    
    private void Jump()
    {

        

        if (imGrounded)
            jumpTimeCounter = jumpMaxTime;

        if (Input.GetKeyDown(KeyCode.W) && imGrounded)
        {
            //SoundPlayer.PlayOneShot(s_Salto);
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpForce);
            stoppedJump = false;
        }

        if (Input.GetKey(KeyCode.W) && !stoppedJump && jumpTimeCounter > 0)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpForce);
            jumpTimeCounter -= Time.deltaTime;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            stoppedJump = true;
            jumpTimeCounter = 0;
        }

    }

    private void ViewHandler()
    {
        if (transform.position.x > mousePos.x)
        {
            if (view_dir == Direccion.DERECHA)
            {
                Flip();
                view_dir = Direccion.IZQUIERDA;
            }
        }
        if (transform.position.x < mousePos.x)
        {
            if (view_dir == Direccion.IZQUIERDA)
            {
                Flip();
                view_dir = Direccion.DERECHA;
            }
        }

    }

    private void ActionHandler()
    {
        CanJump = true;
        switch (m_state) {
            case State.AIRE:

                if (CanDash())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.DASH);
                        return;
                    }

                if (CanDJump())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        ChangeState(State.DJUMP);
                        return;
                    }

                if (CanTurnInvul())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        TurnInvulnerable();
                        return;
                    }

                if (CanBuff())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        GetBuffed();
                        return;
                    }

                if (CanPlane())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.PLANEO);
                        return;
                    }

                if (CanScream())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        Scream();
                        return;
                    }

                if (CanBuild())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        Build();
                        return;
                    }
                
                break;
            case State.MOVIMIENTO:
            case State.IDLE:
                                
                if (CanDash())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.DASH);
                        return;
                    }

                if (CanTurnInvul())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        TurnInvulnerable();
                        return;
                    }

                if (CanBuff())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        GetBuffed();
                        return;
                    }

                if (CanScream())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        Scream();
                        return;
                    }

                if (CanHook())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        ChangeState(State.GANCHO_1);
                        return;
                    }

                if (CanBuild())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        Build();
                        return;
                    }
                if (CanHeal())
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        ChangeState(State.CURAR);
                        return;
                    }

                if (CanChange())
                    if (Input.GetKeyDown(KeyCode.C))
                    {
                        ChangeState(State.CAMBIO);
                        return;
                    }

                if (CanShoot())
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        StartCoroutine(Shoot());
                        return;
                    }

                if (CanAttack())
                    if (Input.GetMouseButtonDown(0))
                    {
                        ChangeState(State.ATACAR);
                        return;
                    }

                    break;
            case State.PLANEO:
                if (Input.GetKeyDown(KeyCode.Alpha1))
                    ChangeState(State.IDLE);
                break;
            case State.GANCHO_1:
                if (Input.GetKeyDown(KeyCode.Alpha3))
                    ChangeState(State.IDLE);
                CanJump = false;
                break;
            case State.ATACAR:

                if (CanAttack())
                    if (Input.GetMouseButtonDown(0))
                    {
                        ChangeState(State.ATACAR);
                        return;
                    }

                break;
            case State.CURAR:
            case State.CAMBIO:
            case State.GANCHO_2:
            case State.DASH:
            case State.DJUMP:
            case State.ON_HIT:
            
                //print("Estas en un estado en el que no puedes llevar a cabo acciones");
                CanJump = false;
                break;
        }

    }

    private void MovementHandler()
    {

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, GetComponent<Rigidbody2D>().velocity.y);            
        }
        else if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, GetComponent<Rigidbody2D>().velocity.y);            
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }

    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    private void ChangeState(State NextState)
    {

        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();

    }

    private void StateOnUpdate()
    {
        switch (m_state)
        {
            case State.AIRE:
                StateAIRE();                                                                                                   
                break;
            case State.MOVIMIENTO:
                StateMOVIMIENTO();
                break;
            case State.IDLE:
                StateIDLE();
                break;
            case State.DASH:
                StateDASH();
                break;
            case State.DJUMP:
                StateDJUMP();
                break;
            case State.PLANEO:
                StatePLANEO();
                break;
            case State.CURAR:
                StateCURAR();
                break;
            case State.CAMBIO:
                StateCAMBIO();
                break;
            case State.GANCHO_1:
                StateGANCHO1();
                break;
            case State.GANCHO_2:
                StateGANCHO2();
                break;
            case State.ATACAR:
                break;
        }
    }
    
    private void StateOnExit()
    {
        switch (m_state)
        {
            case State.AIRE:
                
                break;
            case State.MOVIMIENTO:
                ActualAnimator.enabled = false;
                break;
            case State.IDLE:
                ActualAnimator.enabled = false;
                break;
            case State.DASH:
                CanMove = true;
                CanChangeView = true;
                break;
            case State.DJUMP:
                GetComponent<Rigidbody2D>().gravityScale = 1;
                Bubble.SetActive(false);
                activeDJump = false;
                CanMove = true;
                break;
            case State.PLANEO:
                GetComponent<Rigidbody2D>().gravityScale = 1;
                CanMove = true;
                Paravela.SetActive(false);
                ComprobantePlaneo.SetActive(false);
                break;
            case State.CURAR:
                CanMove = true;
                break;
            case State.CAMBIO:
                GetComponent<CapsuleCollider2D>().enabled = true;
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                ActualAnimator.enabled = false;
                CanMove = true;
                CanChangeView = true;
                break;
            case State.GANCHO_1:
                Brazo.SetPosition(0, PosSalidaBrazo.position);
                Brazo.SetPosition(1, PosSalidaBrazo.position);
                Brazo.enabled = false;
                GetComponent<DistanceJoint2D>().enabled = false;
                //GetComponent<SpringJoint2D>().enabled = false;
                BrazoSkin.SetActive(true);
                ManoFinal.SetActive(false);
                break;
            case State.GANCHO_2:

                break;
            case State.ON_HIT:
                CanMove = true;
                break;
            case State.ATACAR:
                break;
        }
    }

    private void StateOnEnter(State nextState)
    {
        m_state = nextState;
        ActualAnimator.enabled = true;

        switch (m_state)
        {
            case State.AIRE:

                break;
            case State.MOVIMIENTO:
                ActualAnimator.Play("RUN");
                break;
            case State.IDLE:
                ActualAnimator.Play("IDLE");
                break;
            case State.DASH:
                ActualAnimator.Play("DASH");
                SoundPlayer.PlayOneShot(s_Dash);
                CanMove = false;
                CanChangeView = false;
                pau_habilidad1_Dash.Raise();
                StartCoroutine(CooldownCorrutine(0, pau_sc_habilidad1_Dash));
                StartCoroutine(Dash());
                break;
            case State.DJUMP:
                activeDJump = true;
                SoundPlayer.PlayOneShot(s_Burbuja);
                GetComponent<Rigidbody2D>().gravityScale = 0;
                CanMove = false;
                StartCoroutine(CooldownCorrutine(1, pau_sc_habilidad2_DoubleJump));
                StartCoroutine(DJumpTime());
                pau_habilidad2_DoubleJump.Raise();
                ActualAnimator.Play("BUBBLE");
                break;
            case State.PLANEO:
                SoundPlayer.PlayOneShot(s_Paravela);
                GetComponent<Rigidbody2D>().gravityScale = 0.2f;
                CanMove = false;
                ActualAnimator.Play("PLANEAR");
                ComprobantePlaneo.SetActive(true);
                break;
            case State.CURAR:
                SoundPlayer.PlayOneShot(s_Curar);
                si_heals.curacion--;
                DrinkEvent.Raise();
                CanMove = false;
                ActualAnimator.Play("HEAL");
                StartCoroutine(AccionCurar());
                StartCoroutine(CooldownCorrutine(9, sc_heal));
                break;
            case State.CAMBIO:
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                GetComponent<CapsuleCollider2D>().enabled = false;
                if(corrutinaCuracionPasiva != null)
                {
                    StopCoroutine(corrutinaCuracionPasiva);
                    corrutinaCuracionPasiva = null;
                }
                StartCoroutine(ChangeCharacter());
                CanMove = false;
                CanChangeView = false;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                break;
            case State.GANCHO_1:
                BrazoSkin.SetActive(false);
                ManoFinal.SetActive(true);
                ActionGrab();
                break;
            case State.GANCHO_2:

                break;
            case State.ON_HIT:
                ActualAnimator.Play("HIT");
                StartCoroutine(Hitted());
                CanMove = false;
                break;
            case State.ATACAR:
                StartCoroutine(BattleAction());
                break;
        }
    }

    private bool CanAttack()
    {
        if (toSoon)
            return false;

        return true;
    }

    private bool CanChange()
    {
        if (!activeAbilities[10] || !canChangeEvent || HayAlguienMuerto())
            return false;

        return true;
    }



    // CAMBIO DE PERSONAJE

    public void CantChangeForcePau()
    {
        quienTengoQueSer = false;
        if (m_Identity == Identidad.GERARD)
        {
            ChangeState(State.CAMBIO);
            canChangeEvent = false;
        }
        else
            StartCoroutine(ForzarCambio());

        morirPauInstantaneo = true;
    }



    public void CantChangeForceGerard()
    {
        quienTengoQueSer = true;
        if (m_Identity == Identidad.PAU && imGrounded) {
            ChangeState(State.CAMBIO);
            canChangeEvent = false;
        }
        else
            StartCoroutine(ForzarCambio());

        morirGerardInstantaneo = true;
    }

    private IEnumerator ForzarCambio()
    {
        bool flag = true;
        while (flag)
        {
            yield return new WaitForSeconds(0.1f);
            if (imGrounded)
            {
                if (quienTengoQueSer && m_Identity == Identidad.PAU)
                {
                    ChangeState(State.CAMBIO);
                    canChangeEvent = false;
                    flag = false;
                }
                else if (!quienTengoQueSer && m_Identity == Identidad.GERARD)
                {
                    ChangeState(State.CAMBIO);
                    canChangeEvent = false;
                    flag = false;
                }
            }
        }
    }


    public void CanChangeEvent()
    {
        canChangeEvent = true;
    }

    public void RecibirDanyo(int dany)
    {
        if (m_Identity == Identidad.PAU)
        {
            sv_VidaPau.VidaActual -= dany;               // Me bajan vida
            if (morirPauInstantaneo)
            {
                if (sv_VidaPau.VidaActual <= 0)
                {
                    sv_VidaPau.VidaActual = 0;
                    eventoMorirse.Raise();
                    this.gameObject.GetComponent<CharacterController>().enabled = false;
                    return;
                }
            }
            else
            {
                if (sv_VidaPau.VidaActual < 0)
                {
                    sv_VidaPau.VidaActual = 0;               // para que no me dejen en negativo
                    if (LosDosMuertos())
                    {
                        eventoMorirse.Raise();
                        this.gameObject.GetComponent<CharacterController>().enabled = false;
                        return;
                    }
                }
                if (sv_VidaPau.VidaActual <= 15)             // Si me dejan a 15 o menos de vida fuerzo el cambio
                    StartCoroutine(ForzarCambioPorVida());      // fuerzo cambio
            }
        }
        else
        {
            sv_VidaGerard.VidaActual -= dany;               // Me bajan vida
            if (morirGerardInstantaneo)
            {
                if (sv_VidaGerard.VidaActual <= 0)
                {
                    sv_VidaGerard.VidaActual = 0;
                    eventoMorirse.Raise();
                    this.gameObject.GetComponent<CharacterController>().enabled = false;
                    return;
                }
            }
            else
            {
                if (sv_VidaGerard.VidaActual < 0)
                {
                    sv_VidaGerard.VidaActual = 0;                   // para que no me dejen en negativo
                    if (LosDosMuertos())
                    {
                        eventoMorirse.Raise();
                        this.gameObject.GetComponent<CharacterController>().enabled = false;
                        return;
                    }
                }
                if (sv_VidaGerard.VidaActual <= 15)             // Si me dejan a 15 o menos de vida fuerzo el cambio
                    StartCoroutine(ForzarCambioPorVida());      // fuerzo cambio
            }
        }
        eventoCambiarVidaHUD.Raise();
    }


    private IEnumerator ForzarCambioPorVida()
    {
        bool flag = true;
        while (flag)
        {
            yield return new WaitForSeconds(0.1f);
            if (imGrounded)
            {
                flag = false;
                ChangeState(State.CAMBIO);
            }
        }
    }

    private bool HayAlguienMuerto()
    {
        if (m_Identity == Identidad.PAU)
        {
            if (sv_VidaGerard.VidaActual <= 0)
                return true;
            else
                return false;
        }
        else
        {
            if (sv_VidaPau.VidaActual <= 0)
                return true;
            else
                return false;
        }
    }

    private bool LosDosMuertos()
    {
        if (sv_VidaGerard.VidaActual <= 0 && sv_VidaPau.VidaActual <= 0)
            return true;
        else
            return false;
    }


    // DASH

    private bool CanDash()
    {
        if (!unlockedAbilities[0])
            return false;
        if (!activeAbilities[0])
            return false;
        if (m_Identity == Identidad.GERARD)
            return false;
        
        return true;
    }

    private bool CanDJump()
    {
        if (!unlockedAbilities[1])
            return false;
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[1])
            return false;

        return true;
    }

    private bool CanTurnInvul()
    {
        if (!unlockedAbilities[2])
            return false;
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[2])
            return false;

        return true;
    }

    private bool CanBuff()
    {
        if (!unlockedAbilities[3])
            return false;
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[3])
            return false;

        return true;
    }

    private bool CanPlane()
    {
        if (!unlockedAbilities[4])
            return false;
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[4])
            return false;

        return true;
    }

    private bool CanScream()
    {
        if (!unlockedAbilities[5])
            return false;
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[5])
            return false;

        return true;
    }

    private bool CanHook()
    {
        if (!unlockedAbilities[6])
            return false;
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[6])
            return false;

        return true;
    }

    private bool CanBuild()
    {
        if (!unlockedAbilities[7])
            return false;
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[7])
            return false;

        return true;
    }

    private bool CanHeal()
    {
        if (si_heals.curacion <= 0)
            return false;

        if (!activeAbilities[9])
            return false;

        return true;
    }

    private bool CanShoot()
    {
        if (sad_ataqueDistancia.numeroAtaquesAct <= 0)
            return false;

        if (!activeAbilities[8])
            return false;

        return true;
    }

    private void DoubleJump()
    {
        
    }


    private void TurnInvulnerable()
    {
        pau_habilidad3_Inmunidad.Raise();
        StartCoroutine(CooldownCorrutine(2, pau_sc_habilidad3_Inmunidad));
        StartCoroutine(ActionInv());
    }

    IEnumerator ActionInv()
    {
        SoundPlayer.PlayOneShot(s_Invulnerabilidad);
        foreach (SpriteRenderer s in invulnerabilidad)
            s.color = Color.blue;
            
        gameObject.layer = 27;
        yield return new WaitForSeconds(2f);
        gameObject.layer = 3;
        foreach (SpriteRenderer s in invulnerabilidad)
            s.color = Color.white;

    }

    private void GetBuffed()
    {
        pau_habilidad4_Combate.Raise();
        StartCoroutine(CooldownCorrutine(3, pau_sc_habilidad4_Combate));
        StartCoroutine(ActionBuff());
    }

    IEnumerator ActionBuff()
    {
        SoundPlayer.PlayOneShot(s_Buff);
        vfxEnfado.SetActive(true);
        se_CharStats.da�o += 5;
        se_CharStats.velocidad += 3;
        UpdateStats();
        foreach (SpriteRenderer s in enfadado)
            s.color = Color.red;
        //GetComponent<SombraCombate>().enabled = true;
        yield return new WaitForSeconds(4f);
        vfxEnfado.SetActive(false);
        se_CharStats.da�o -= 5;
        se_CharStats.velocidad -= 3;
        UpdateStats();
        foreach (SpriteRenderer s in enfadado)
            s.color = Color.white;
        //GetComponent<SombraCombate>().enabled = false;
    }

    private void Scream()
    {
        //print("SCREAM");
        gerard_habilidad2_Grito.Raise();
        StartCoroutine(ScreamAction());
        StartCoroutine(CooldownCorrutine(5, gerard_sc_habilidad2_Grito));
    }

    IEnumerator ScreamAction()
    {
        SoundPlayer.PlayOneShot(s_Grito[Random.Range(0,5)]);

        ScreamArea.SetActive(true);
        StartCoroutine(ScreamAreaIncrement());
        yield return new WaitForSeconds(2);
        ScreamArea.SetActive(false);
    }

    IEnumerator ScreamAreaIncrement()
    {
        ScreamArea.transform.localScale = new Vector2(0.1f, 0.1f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.2f, 0.2f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.3f, 0.3f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.4f, 0.4f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.5f, 0.5f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.6f, 0.6f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.7f, 0.7f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.8f, 0.8f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(0.9f, 0.9f);
        yield return new WaitForSeconds(0.1f);
        ScreamArea.transform.localScale = new Vector2(1f, 1f);
        yield return new WaitForSeconds(0.1f);
    }

    private void Build()
    {
        //print("BUILD");
        gerard_habilidad4_CrearPlataforma.Raise();

        GameObject newPlat = platPool.getElement();
        SoundPlayer.PlayOneShot(s_Plataforma);
        if (m_state != State.AIRE)
        {
            if (view_dir == Direccion.DERECHA)
                newPlat.transform.position = new Vector3(transform.position.x + 1, transform.position.y + 0.5f, 0);
            else
                newPlat.transform.position = new Vector3(transform.position.x - 1, transform.position.y + 0.5f, 0);
        }
        else
            newPlat.transform.position = new Vector3(transform.position.x, transform.position.y - 1f, 0);
        
        StartCoroutine(CooldownCorrutine(7, gerard_sc_habilidad4_CrearPlataforma));
        StartCoroutine(DevolverPlat(newPlat));
    }

    IEnumerator DevolverPlat(GameObject go)
    {
        yield return new WaitForSeconds(3f);
        platPool.ReturnElement(go);
    }

    /*
    IEnumerator CooldownCorrutineChangeCharacter()
    {
        CanChange = false;
        while (!CanChange)
        {
            sc_cambio.tiempoActual += 0.1f;
            yield return new WaitForSeconds(0.1f);
            if (sc_cambio.tiempoActual >= sc_cambio.tiempoTotal) { 
                sc_cambio.tiempoActual = 0;
                CanChange = true;
            }
        }
    }
    */

    IEnumerator Shoot()
    {
        print("DISPARO");
        StartCoroutine(CooldownCorrutine(8, sc_disparar));
        sad_ataqueDistancia.numeroAtaquesAct--;
        ShootEvent.Raise();
        GameObject newBall = ballPool.getElement();
        newBall.GetComponent<BallBehaviour>().OnDespawn += DespawnBall;
        newBall.GetComponent<BallBehaviour>().SetObjetivo(mousePos);
        newBall.transform.position = posDisparo.position;
        newBall.GetComponent<BallBehaviour>().Shoot();
        GetComponent<Animator>().Play("MarioShoot");
        yield return new WaitForSeconds(0.75f);
        GetComponent<Animator>().Play("MarioOut");
        yield return new WaitForSeconds(1f);

    }
    
    private void DespawnBall(GameObject go)
    {
        go.GetComponent<BallBehaviour>().OnDespawn -= DespawnBall;
        ballPool.ReturnElement(go);
                
    }

    private void StateAIRE()
    {
        if (imGrounded)
            ChangeState(State.IDLE);

        if (GetComponent<Rigidbody2D>().velocity.y > 0)
            ActualAnimator.Play("JUMP");
        //else if (GetComponent<Rigidbody2D>().velocity.y < 0)
        else
            ActualAnimator.Play("FALL");
    }

    private void StateMOVIMIENTO()
    {
        if (!imGrounded)
            ChangeState(State.AIRE);

        if (GetComponent<Rigidbody2D>().velocity.x == 0 && GetComponent<Rigidbody2D>().velocity.y == 0)
            ChangeState(State.IDLE);
        if (!JumpCanceled)
            Jump();

    }

    private void StateIDLE()
    {
        if (!imGrounded)
            ChangeState(State.AIRE);

        if (GetComponent<Rigidbody2D>().velocity.x != 0)
            ChangeState(State.MOVIMIENTO);
        if (!JumpCanceled)
            Jump();
    }

    private void StateDASH()
    {
        
    }
    
    IEnumerator Dash()
    {
        
        if (view_dir == Direccion.DERECHA)
            GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);


        yield return new WaitForSeconds(0.2f);
        
        ChangeState(State.IDLE);

    }

    private void StateDJUMP()
    {
        
        if (Input.GetKey(KeyCode.D))
            GetComponent<Rigidbody2D>().velocity = new Vector2(nonGravityVelocity, GetComponent<Rigidbody2D>().velocity.y);
        else if (Input.GetKey(KeyCode.A))
            GetComponent<Rigidbody2D>().velocity = new Vector2(-nonGravityVelocity, GetComponent<Rigidbody2D>().velocity.y);
        else if (Input.GetKey(KeyCode.W))
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, nonGravityVelocity);
        else if (Input.GetKey(KeyCode.S))
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -nonGravityVelocity);
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

        if (imGrounded)
            ChangeState(State.IDLE);
        if (!activeDJump)
            ChangeState(State.IDLE);
        
    }

    IEnumerator DJumpTime()
    {        
        yield return new WaitForSeconds(1.2f);
        activeDJump = false;
    }

    private void StatePLANEO()
    {
        StartCoroutine(CooldownCorrutine(4, gerard_sc_habilidad1_Planear));

        if (Input.GetKey(KeyCode.D))
            GetComponent<Rigidbody2D>().velocity = new Vector2((velocity * 3 / 4), FallVelocity);
        else if (Input.GetKey(KeyCode.A))
            GetComponent<Rigidbody2D>().velocity = new Vector2(-(velocity * 3 / 4), FallVelocity);
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, FallVelocity);

        if (imGrounded)
            ChangeState(State.IDLE);
    }

    private void StateCURAR()
    {
        
    }

    IEnumerator AccionCurar()
    {
        yield return new WaitForSeconds(0.5f);

        if (m_Identity == Identidad.GERARD)
        {
            sv_VidaGerard.VidaActual += 50;
            if (sv_VidaGerard.VidaActual > sv_VidaGerard.VidaMaxima)
                sv_VidaGerard.VidaActual = sv_VidaGerard.VidaMaxima;
        }
        else
        {
            sv_VidaPau.VidaActual += 50;
            if (sv_VidaPau.VidaActual > sv_VidaPau.VidaMaxima)
                sv_VidaPau.VidaActual = sv_VidaPau.VidaMaxima;
        }
        GameObject newLata = Instantiate(lata);
        newLata.transform.position = PosLata.position;
        newLata.transform.Rotate(new Vector3(0, 0, 90));
        eventoCambiarVidaHUD.Raise();
        yield return new WaitForSeconds(0.5f);
        ChangeState(State.IDLE);
    }

    private void StateCAMBIO()
    {
        
    }

    IEnumerator ChangeCharacter()
    {
        if (m_Identity == Identidad.PAU)
        {
            SkinGerard.SetActive(true);
            SkinPau.GetComponent<Animator>().Play("SALIDA");
            SkinGerard.GetComponent<Animator>().Play("ENTRADA");
            yield return new WaitForSeconds(1);
            m_Identity = Identidad.GERARD;
            SkinPau.SetActive(false);
            changeToGerard.Raise();
            ActualAnimator = SkinGerard.GetComponent<Animator>();
            if (sv_VidaPau.VidaActual < 10)
                corrutinaCuracionPasiva = StartCoroutine(CurarPlayerPorCambio());
        }
        else
        {
            SkinPau.SetActive(true);
            SkinGerard.GetComponent<Animator>().Play("SALIDA");
            SkinPau.GetComponent<Animator>().Play("ENTRADA");
            yield return new WaitForSeconds(1);
            m_Identity = Identidad.PAU;
            SkinGerard.SetActive(false);
            changeToPau.Raise();
            ActualAnimator = SkinPau.GetComponent<Animator>();
            if (sv_VidaGerard.VidaActual < 10)
                corrutinaCuracionPasiva = StartCoroutine(CurarPlayerPorCambio());
        }
        yield return new WaitForSeconds(0.1f);
        StartCoroutine(CooldownCorrutine(10, sc_change));
        ChangeState(State.IDLE);
    }

    private IEnumerator CurarPlayerPorCambio()
    {
        yield return new WaitForSeconds(10f);
        bool flag = true;
        while (flag)
        {
            if (m_Identity == Identidad.PAU) {
                sv_VidaGerard.VidaActual += 1;
                if (sv_VidaGerard.VidaActual >= 10)
                    flag = false;
            }
            else
            {
                sv_VidaPau.VidaActual += 1;
                if (sv_VidaPau.VidaActual >= 10)
                    flag = false;
            }
            eventoCambiarVidaHUD.Raise();
            yield return new WaitForSeconds(1f);
        }
    }

    private void StateGANCHO1()
    {
        Brazo.SetPosition(0, PosSalidaBrazo.position);
        Brazo.SetPosition(1, GrabHit.point);
        Vector2 dirMano = PosSalidaBrazo.position - ManoFinal.transform.position;
        float angulo = Mathf.Atan2(dirMano.y, dirMano.x) * Mathf.Rad2Deg;
        angulo += 90;
        Quaternion rotation = Quaternion.AngleAxis(angulo, Vector3.forward);
        ManoFinal.transform.rotation = Quaternion.Slerp(ManoFinal.transform.rotation, rotation, 100 * Time.deltaTime);
    }

    private void ActionGrab()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

        GrabHit = Physics2D.Raycast(transform.position, direction, maxDistanceGrap, grappableMask); //RaycastHit2D 
        

        if (GrabHit.collider != null && GrabHit.collider.gameObject.layer==15)
        {
            SoundPlayer.PlayOneShot(s_Gancho);
            ManoFinal.SetActive(true);
            ManoFinal.transform.position = GrabHit.point;
            Brazo.enabled = true;
            Brazo.positionCount = 2;

            GetComponent<Rigidbody2D>().MovePosition(Vector2.MoveTowards(transform.position, GrabHit.transform.position, 2 * Time.deltaTime));

            
            GetComponent<DistanceJoint2D>().enabled = true;
            GetComponent<DistanceJoint2D>().connectedAnchor = new Vector2(GrabHit.point.x, GrabHit.point.y);

            GetComponent<DistanceJoint2D>().distance = 1f;
            
            /*
            GetComponent<SpringJoint2D>().enabled = true;
            GetComponent<SpringJoint2D>().connectedAnchor = new Vector2(GrabHit.point.x, GrabHit.point.y);

            GetComponent<SpringJoint2D>().distance = 1f;
            */
        } else
        {
            ChangeState(State.IDLE);
        }
    }

    private void StateGANCHO2()
    {
        ChangeState(State.IDLE);
    }
    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.GetContact(0).point.y <= transform.position.y)
            Collisions.Add(collision.collider.tag);

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Collisions.Remove(collision.collider.tag);
    }
    */

    IEnumerator CooldownCorrutine(int position, ScriptableCooldown sc)
    {
        sc.tiempoActual = 0;
        activeAbilities[position] = false;
        while (sc.tiempoActual < sc.tiempoTotal)
        {
            yield return new WaitForSeconds(0.1f);
            sc.tiempoActual += 0.1f;
            if (sc.tiempoActual >= sc.tiempoTotal)
                sc.tiempoActual = sc.tiempoTotal;
        }
        activeAbilities[position] = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemigo"))
        {
            if (collision.gameObject.transform.position.x > this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 2);
            else if (collision.gameObject.transform.position.x < this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 2f);
            else
            {
                int r = Random.Range(0, 2);
                if (r == 0)
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 3);
                else
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 3);
            }

            ChangeState(State.ON_HIT);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemigo"))
        {
            if (collision.gameObject.transform.position.x > this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 2);
            else if (collision.gameObject.transform.position.x < this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 2f);
            else
            {
                int r = Random.Range(0, 2);
                if (r == 0)
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 3);
                else
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 3);
            }

            ChangeState(State.ON_HIT);
        }

        if (collision.gameObject.tag.Equals("PelotaExtra"))
            AnyadirPelota();

    }

    private void AnyadirPelota()
    {
        sad_ataqueDistancia.numeroAtaquesAct++;
        if (sad_ataqueDistancia.numeroAtaquesAct >= sad_ataqueDistancia.numeroAtaquesMax)
            sad_ataqueDistancia.numeroAtaquesAct = sad_ataqueDistancia.numeroAtaquesMax;

        AddShootEvent.Raise();
    }
    IEnumerator Hitted()
    {
        gameObject.layer = 27;
        yield return new WaitForSeconds(0.5f);
        gameObject.layer = 3;
        ChangeState(State.IDLE);
    }

    IEnumerator BattleAction()
    {
        
        switch(BattleState)
        {
            case CombatState.NON:
                ActualAnimator.Play("COMBO1");
                BattleState = CombatState.HIT1;
                break;
            case CombatState.HIT1:
                ActualAnimator.Play("COMBO2");
                BattleState = CombatState.HIT2;
                break;
            case CombatState.HIT2:
                ActualAnimator.Play("COMBO3");
                BattleState = CombatState.NON;
                toSoon = true;
                yield return new WaitForSeconds(0.5f);
                ChangeState(State.IDLE);
                break;
        }
        

        CombatState copyState = BattleState;
        toSoon = true;
        yield return new WaitForSeconds(0.5f);
        toSoon = false;
        yield return new WaitForSeconds(0.5f);

        if (BattleState == copyState)
        {
            BattleState = CombatState.NON;
            ChangeState(State.IDLE);
        }
    }

    public bool SoyPau()
    {
        if (m_Identity == Identidad.PAU)
            return true;

        return false;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(centroBox.position, anchoBox);

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(mousePos, 0.1f);
    }

    public void DesbloquearHabilidad(int Index)
    {
        unlockedAbilities[Index] = true;
    }


    // LOAD INFO

    public void LoadGerard()
    {
        m_Identity = Identidad.GERARD;
        ActualAnimator = SkinGerard.GetComponent<Animator>();
        SkinGerard.SetActive(true);
        SkinPau.SetActive(false);
        changeToGerard.Raise();
        if (sv_VidaGerard.VidaActual <= 10) // Basicamente para que no sea una semada
        { 
            sv_VidaGerard.VidaActual = 10;
            eventoCambiarVidaHUD.Raise();
        }
    }

    public void LoadPau()
    {
        m_Identity = Identidad.PAU;
        ActualAnimator = SkinPau.GetComponent<Animator>();
        SkinPau.SetActive(true);
        SkinGerard.SetActive(false);
        if (sv_VidaPau.VidaActual <= 10) // Basicamente para que no sea una semada
        {
            sv_VidaPau.VidaActual = 10;
            eventoCambiarVidaHUD.Raise();
        }
    }

    public void LodInfoHabilidades(bool[] b)
    {
        for (int i = 0; i < b.Length; i++)
            unlockedAbilities[i] = b[i];
    }

}
