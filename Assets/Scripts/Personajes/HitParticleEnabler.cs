using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class HitParticleEnabler : MonoBehaviour
{

    [SerializeField] private VisualEffect Hit;
    [SerializeField] private AudioSource AudioPlayer;
    [SerializeField] private AudioClip[] PunchSounds = new AudioClip[11];

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 16 || collision.gameObject.layer == 17)
        {
            HitParticle();
        }
    }

    private void HitParticle()
    {
        Hit.transform.position = transform.position;
        Hit.Play();
        AudioPlayer.PlayOneShot(PunchSounds[Random.Range(0, 11)]);
    }

}
