using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCastellbisblaSergio_controller : MonoBehaviour
{
    public enum Estado { PENSADO, VOLVER, ATAQUE1, ATAQUE2, ATAQUE3, ESPERANDO, CANSADO };

    [Header("Estado actual del enemigo")]
    [SerializeField] private Estado estadoActual;


    [Header("TRANSFORM del Player")]
    [SerializeField] private GameObject targetPlayer;

    [Header("Scriptable vida")]
    [SerializeField] private ScriptableVida sv;


    [Header("ATAQUE 1")]
    
    [Header("diferentes frases")]
    [SerializeField] private Sprite[] fotos;
    [Header("Puntos salida frases")]
    [SerializeField] private Transform[] puntosSalida;
    [Header("Pool de proyectiles")]
    [SerializeField] Pool pool;
    private bool direccionPlayerAtaque1;


    //[Header("ATAQUE 2")]
    private float tiempo = 2f; // tiempo que tarda en llegar al punto
    private int danyoAtaque2 = 10;



    [Header("ATAQUE 3")]
    [Header("Carrito")]
    [SerializeField] private GameObject carrito;
    [Header("Punto de salida del salto / punto al que tiene que volver si se mueve")]
    [SerializeField] private Transform puntoSalo;
    [Header("Sprite de prueba en estado Esperando y normal")]
    [SerializeField] GameEvent activarTunelDeViento;
    [SerializeField] GameEvent desactivarTunelDeViento;
    private bool direccionPlayerAtaque3;
    private bool inAtaque3;
    private bool mePuedoMoverAtaque3 = false;


    [Header("CANSADO")]

    [Header("Area deteccion para habler con Sergio")]
    [SerializeField] private GameObject AreaHablar;

    [Header("EVENTOS")]

    [Header("Eventos para poder cambiar de personaje")]
    [SerializeField] private GameEvent puedesCambiarDePersonaje;
    [SerializeField] private GameEvent noPuedesCambiarDePersonajeForzarGerard;
    [Header("Evento da�o")]
    [SerializeField] private GameEventInteger gei;
    [Header("Eventos sergio muerto")]
    [SerializeField] private GameEvent sergioMuerto;
    [Header("Evento recivir da�o")]
    [SerializeField] private GameEvent bajarVidaSergio;

    // Variables generales
    private int numAtaques = 0;
    private bool direccionPlayer;
    private Coroutine corrutinaEsperando;


    void Start()
    {
        //noPuedesCambiarDePersonajeForzarGerard.Raise();
        ChangeState(Estado.PENSADO);
        carrito.SetActive(false);
        AreaHablar.SetActive(false);
        inAtaque3 = false;
        //transform.rotation = Quaternion.Euler(0, 0, 0);

        // EVENTO DE FORZAR GERARD
        StartCoroutine(ForzarCambioGerard());
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        sv.VidaActual = sv.VidaMaxima;
    }

    // Update is called once per frame
    void Update()
    {
        StateOnUpdate();
        MirarDireccionPlayer();
    }

    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                ExitPensando();
                break;
            case Estado.VOLVER:
                ExitVolver();
                break;
            case Estado.ATAQUE1:
                ExitAtaque1();
                break;
            case Estado.ATAQUE2:
                ExitAtaque2();
                break;
            case Estado.ATAQUE3:
                ExitAtaque3();
                break;
            case Estado.ESPERANDO:
                ExitEsperando();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.PENSADO:
                EnterPensando();
                break;
            case Estado.VOLVER:
                EnterVolver();
                break;
            case Estado.ATAQUE1:
                EnterAtaque1();
                break;
            case Estado.ATAQUE2:
                EnterAtaque2(targetPlayer.transform);
                break;
            case Estado.ATAQUE3:
                EnterAtaque3();
                break;
            case Estado.ESPERANDO:
                EnterEsperando();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                UpdatePensando();
                break;
            case Estado.VOLVER:
                UpdateVolver();
                break;
            case Estado.ATAQUE1:
                UpdateAtaque1();
                break;
            case Estado.ATAQUE2:
                UpdateAtaque2();
                break;
            case Estado.ATAQUE3:
                UpdateAtaque3(direccionPlayerAtaque3);
                break;
            case Estado.ESPERANDO:
                UpdateEsperando();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }


    // ATAQUE 1

    private void EnterAtaque1()
    {
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        direccionPlayerAtaque1 = direccionPlayer;
        StartCoroutine(Ataque1Corrutina(direccionPlayerAtaque1));
        this.gameObject.GetComponent<Animator>().Play("SCREAM_SERGIO");
    }
    private void ExitAtaque1()
    {
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
    }
    private void UpdateAtaque1()
    {

    }
    private IEnumerator Ataque1Corrutina(bool direccionAtaque)
    {
        direccionAtaque = !direccionAtaque;
        GameObject frase1 = pool.getElement();
        frase1.GetComponent<BoxCollider2D>().enabled = true;
        frase1.transform.position = puntosSalida[Random.RandomRange(0, 2)].position;        // le pongo la posicion de salida
        frase1.GetComponent<SpriteRenderer>().sprite = fotos[Random.RandomRange(0, 5)];     // le pongo el sprite de una frase random
        frase1.GetComponent<FraseBossCastellbisbal_controller>().SetDireccion(direccionAtaque);
        StartCoroutine(DevolverPool(frase1));

        yield return new WaitForSeconds(1.5f);

        GameObject frase2 = pool.getElement();
        frase2.GetComponent<BoxCollider2D>().enabled = true;
        frase2.transform.position = puntosSalida[Random.RandomRange(0, 2)].position;        // le pongo la posicion de salida
        frase2.GetComponent<SpriteRenderer>().sprite = fotos[Random.RandomRange(0, 5)];     // le pongo el sprite de una frase random
        frase2.GetComponent<FraseBossCastellbisbal_controller>().SetDireccion(direccionAtaque);
        StartCoroutine(DevolverPool(frase2));

        yield return new WaitForSeconds(1.5f);

        GameObject frase3 = pool.getElement();
        frase3.GetComponent<BoxCollider2D>().enabled = true;
        frase3.transform.position = puntosSalida[Random.RandomRange(0, 2)].position;        // le pongo la posicion de salida
        frase3.GetComponent<SpriteRenderer>().sprite = fotos[Random.RandomRange(0, 5)];     // le pongo el sprite de una frase random
        frase3.GetComponent<FraseBossCastellbisbal_controller>().SetDireccion(direccionAtaque);
        StartCoroutine(DevolverPool(frase3));

        yield return new WaitForSeconds(1.5f);
        ChangeState(Estado.PENSADO);
    }
    private IEnumerator DevolverPool(GameObject go)
    {
        yield return new WaitForSeconds(10f);
        pool.ReturnElement(go);
    }


    // ATAQUE 2
    private void EnterAtaque2(Transform posicionPlayerDada)
    {
        this.gameObject.GetComponent<Animator>().Play("JUMP_SERGIO");
        danyoAtaque2 = 40;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        float Vx = (posicionPlayerDada.transform.position.x - puntoSalo.transform.position.x) / tiempo;
        float gravity = -9.8f * GetComponent<Rigidbody2D>().gravityScale;
        //float gravity = -9.8f * 1;
        float Vy = (posicionPlayerDada.transform.position.y - puntoSalo.transform.position.y - (gravity * tiempo * tiempo) / 2) / tiempo;    // estas cosas de aqui sin el canon tambin rulan
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Vx, Vy);
        StartCoroutine(Ataque2Corrutina());
    }
    private void ExitAtaque2()
    {
        danyoAtaque2 = 10;
    }
    private void UpdateAtaque2()
    {
       
    }
    private IEnumerator Ataque2Corrutina()
    {
        yield return new WaitForSeconds(4f);
        ChangeState(Estado.VOLVER);
    }
    private IEnumerator EsperandoCorrutina()
    {
        yield return new WaitForSeconds(8f);
        ChangeState(Estado.PENSADO);
    }


    // ATAQUE 3
    private void EnterAtaque3()
    {
        
        activarTunelDeViento.Raise();
        carrito.SetActive(true);
        StartCoroutine(Ataque3Corrutina());
        StartCoroutine(VerCarrito());
        direccionPlayerAtaque3 = direccionPlayer;
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        inAtaque3 = true;
        mePuedoMoverAtaque3 = false;
    }
    private void ExitAtaque3()
    {
        desactivarTunelDeViento.Raise();
        carrito.SetActive(false);
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        inAtaque3 = false;
        mePuedoMoverAtaque3 = false;
    }
    private void UpdateAtaque3(bool dir)
    {
        if (mePuedoMoverAtaque3)
        {
            if (!dir)
                GetComponent<Rigidbody2D>().velocity = new Vector2(8f, 0);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(-8f, 0);
        }
    }
    private IEnumerator Ataque3Corrutina()
    {
        yield return new WaitForSeconds(4.5f);
        ChangeState(Estado.VOLVER);
    }
    private IEnumerator VerCarrito()
    {
        this.gameObject.GetComponent<Animator>().Play("PREPARAR_CARRITO");
        yield return new WaitForSeconds(2f);
        this.gameObject.GetComponent<Animator>().Play("WALK_CARRITO");
        mePuedoMoverAtaque3 = true;
    }


    // VOLVER
    public void EnterVolver()
    {
        this.gameObject.GetComponent<Animator>().Play("WALK_SERGIO");
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
    }
    public void ExitVolver()
    {
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }
    public void UpdateVolver()
    {
        if (transform.position.x == puntoSalo.transform.position.x || (transform.position.x < puntoSalo.transform.position.x + .5 && transform.position.x > puntoSalo.transform.position.x - .5))
            ChangeState(Estado.PENSADO);
        else if (transform.position.x < puntoSalo.transform.position.x)
            GetComponent<Rigidbody2D>().velocity = new Vector2(3f, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
        else if (transform.position.x > puntoSalo.transform.position.x)
            GetComponent<Rigidbody2D>().velocity = new Vector2(-3f, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }


    // ESPERANDO
    private void EnterEsperando()
    {
        this.gameObject.GetComponent<Animator>().Play("REST_SERGIO");
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;

        puedesCambiarDePersonaje.Raise();
        AreaHablar.SetActive(true);
        corrutinaEsperando = StartCoroutine(EsperandoCorrutina());
    }
    private void ExitEsperando()
    {
        this.gameObject.GetComponent<Animator>().Play("IDLE_SERGIO");
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        noPuedesCambiarDePersonajeForzarGerard.Raise();
        AreaHablar.SetActive(false);
    }
    private void UpdateEsperando()
    {

    }


    // CANSADO 
    private void EnterCansado()
    {
        this.gameObject.GetComponent<Animator>().Play("IDLE_SERGIO");
        if (corrutinaEsperando != null)
        {
            StopCoroutine(corrutinaEsperando);
            corrutinaEsperando = null;
        }
        StartCoroutine(EstoyCansado());
    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }
    private IEnumerator EstoyCansado()
    {
        yield return new WaitForSeconds(5f);
        ChangeState(Estado.PENSADO);
    }


    // PENSADO
    private void EnterPensando()
    {
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        this.gameObject.GetComponent<Animator>().Play("PENSANDO_SERGIO");
        StartCoroutine(PensadoAtaque());
    }
    private void ExitPensando()
    {
        //this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        //this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
    }
    private void UpdatePensando()
    {

    }
    private IEnumerator PensadoAtaque()
    {
        yield return new WaitForSeconds(2f);
        
        numAtaques++;
        if (numAtaques > 3)
        {
            ChangeState(Estado.ESPERANDO);
            numAtaques = 0;
        }
        else
        {
            int numeroAtaque = Random.Range(0, 3);
            if (sv.VidaActual == 1)
            {
                numeroAtaque = Random.Range(0, 20);
                if (numeroAtaque <= 3)
                    ChangeState(Estado.ATAQUE1);
                else if (numeroAtaque > 3 && numeroAtaque <= 17)
                    ChangeState(Estado.ATAQUE3);
                else if(numeroAtaque > 17)
                    ChangeState(Estado.ATAQUE2);
            }
            else
            {
                switch (numeroAtaque)
                {
                    case 0:
                        ChangeState(Estado.ATAQUE1);
                        break;
                    case 1:
                        ChangeState(Estado.ATAQUE2);
                        break;
                    case 2:
                        ChangeState(Estado.ATAQUE3);
                        break;
                }
            }
        }
    }

    // Mirar la posicion en la que se encuentra el player
    private void MirarDireccionPlayer()
    {
        if (targetPlayer.transform.position.x > transform.position.x) // es decir, el player esta en la derecha
            direccionPlayer = false; // true = esta a la derecha
        else
            direccionPlayer = true; // si no esta a la derecha esta a la izquierda

        if (!inAtaque3)
        {
            if (direccionPlayer)
                transform.rotation = Quaternion.Euler(0, 180, 0);
            else
                transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    // bajar la vida del boss
    public void BajarVida()
    {
        sv.VidaActual--;
        bajarVidaSergio.Raise();
        if(sv.VidaActual <= 0)
        {
            this.gameObject.GetComponent<Animator>().Play("MUERTE");
            sergioMuerto.Raise();
            puedesCambiarDePersonaje.Raise();
            //StopAllCoroutines();
            return;
        }
        else
            ChangeState(Estado.CANSADO);
    }

    public  void MuerteSergio()
    {
        this.gameObject.SetActive(false);
    }


    private IEnumerator ForzarCambioGerard()
    {
        yield return new WaitForSeconds(0.2f);
        noPuedesCambiarDePersonajeForzarGerard.Raise();
    }
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        if (collision.gameObject.tag.Equals("ground") && estadoActual == Estado.ATAQUE2)
            this.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
        */

        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(danyoAtaque2);
    }


}
