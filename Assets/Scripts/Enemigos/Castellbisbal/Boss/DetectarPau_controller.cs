using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarPau_controller : MonoBehaviour
{
    [SerializeField] private BossCastellbisblaSergio_controller enemigoPadre;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<CharacterController>().SoyPau())
            enemigoPadre.BajarVida();
    }
}
