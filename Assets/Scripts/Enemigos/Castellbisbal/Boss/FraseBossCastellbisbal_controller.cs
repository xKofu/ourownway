using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FraseBossCastellbisbal_controller : MonoBehaviour
{
    private bool direccion;
    [SerializeField] private GameEventInteger gei;

    void Update()
    {
        if(direccion)
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);   
        else
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);
    }

    public void SetDireccion(bool b)
    {
        direccion = b;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            gei.Raise(20);
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}

