using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BossCastellbisblaManager : MonoBehaviour
{

    [Header("Tuneles de viento")]
    [SerializeField] private GameObject tunel1;
    [SerializeField] private GameObject tunel2;

    [Header("Muros Invisibles")]
    [SerializeField] private GameObject inv1;
    [SerializeField] private GameObject inv2;


    [Header("Informacion transcurso partida")]
    [SerializeField] private InformacionTranscursoPartida itp;
    
    [Header("Eventos")]
    [SerializeField] private GameEvent entrarSalaBossSergio;
    [SerializeField] private GameEvent iniciarBossSergio;

    [Header("Particulas")]
    [SerializeField] private GameObject particulas1;
    [SerializeField] private GameObject particulas2;



    private void Start()
    {
        tunel1.SetActive(false);
        tunel2.SetActive(false);
        inv1.SetActive(false);
        inv2.SetActive(false);
        particulas1.SetActive(false);
        particulas2.SetActive(false);

        StartCoroutine(PonerHabilidades());
    }

    public void ActivarTuneles()
    {
        tunel1.SetActive(true);
        tunel2.SetActive(true);
    }


    public void DesctivarTuneles()
    {
        tunel1.SetActive(false);
        tunel2.SetActive(false);
    }


    public void SergioMuerto()
    {

        inv2.SetActive(false);
        itp.bossCastellbisbalDone = true;
        particulas1.SetActive(true);
        particulas2.SetActive(true);
        StartCoroutine(QuitarParticulas());
    }


    public void PonerMueroInvisible()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        iniciarBossSergio.Raise();
        inv1.SetActive(true);
        inv2.SetActive(true);
    }

    public void QuitarMurosInvisible()
    {
        inv1.SetActive(false);
        inv2.SetActive(false);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            PonerMueroInvisible();
    }


    private IEnumerator PonerHabilidades()
    {
        yield return new WaitForSeconds(0.5f);
        entrarSalaBossSergio.Raise();
    }

    private IEnumerator QuitarParticulas()
    {
        yield return new WaitForSeconds(1f);
        particulas1.GetComponent<VisualEffect>().Stop();
        particulas2.GetComponent<VisualEffect>().Stop();
    }
}

