using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCastellbisbalFriki_controller : MonoBehaviour
{


    private GameObject targetPlayer;

    public enum Estado { QUIETO, ATACANDO, APUNTANDO, VUELTA, CANSADO};
    [Header("Estado actual del enemigo")]
    [SerializeField] private Estado estadoActual;
    private Estado estadoPlaceholder;

    private bool direccion; // true --> derecha // false --> izquierda

    [SerializeField] private Pool pool;
    [SerializeField] private Transform posSalida;
    private bool estaDentro;

    private Coroutine apuntarCorrutina;

    [Header("VIDA")]
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;
    [SerializeField] private float vidaEnemigo;

    private bool muerto = false;
    [SerializeField] private GameObject pelotaExtra;

    private void Start()
    {
        muerto = false;
        vidaEnemigo = sv.VidaMaxima;
        GetComponent<Animator>().Play("IDLE");
    }

    void Update()
    {
        if(!muerto)
            StateOnUpdate();
    }

    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                ExitQuieto();
                break;
            case Estado.APUNTANDO:
                ExitApuntando();
                break;
            case Estado.ATACANDO:
                ExitAtacando();
                break;
            case Estado.VUELTA:
                ExitVuelta();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.QUIETO:
                EnterQuieto();
                break;
            case Estado.APUNTANDO:
                EnterApuntando();
                break;
            case Estado.ATACANDO:
                EnterAtacando();
                break;
            case Estado.VUELTA:
                EnterVuelta();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                UpdateQuieto();
                break;
            case Estado.APUNTANDO:
                UpdateApuntando();
                break;
            case Estado.ATACANDO:
                UpdateAtacando();
                break;
            case Estado.VUELTA:
                UpdateVuelta();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }

    //     QUIETO, ATACANDO, APUNTANDO, VUELTA, CANSADO

    // QUIETO

    private void EnterQuieto()
    {
        GetComponent<Animator>().Play("IDLE");
        if (apuntarCorrutina != null)
        {
            StopCoroutine(apuntarCorrutina);
            apuntarCorrutina = null;
        }
    }
    private void ExitQuieto()
    {

    }
    private void UpdateQuieto()
    {

    }

    public void CambiarQuieto()
    {
        estadoPlaceholder = Estado.QUIETO;
        if (estadoActual != Estado.CANSADO)
            ChangeState(Estado.QUIETO);
    }

    // ATACANDO

    private void EnterAtacando()
    {
        GetComponent<Animator>().Play("ATACAR");
        GameObject escuadra = pool.getElement();
        if (direccion)
            escuadra.GetComponent<EscuadraEnemigoCastellbisbalFriki_controller>().ChangeState(EscuadraEnemigoCastellbisbalFriki_controller.Estado.DER);
        else
            escuadra.GetComponent<EscuadraEnemigoCastellbisbalFriki_controller>().ChangeState(EscuadraEnemigoCastellbisbalFriki_controller.Estado.IZC);
        escuadra.GetComponent<EscuadraEnemigoCastellbisbalFriki_controller>().SetRotacion(direccion);
        escuadra.GetComponent<EscuadraEnemigoCastellbisbalFriki_controller>().SetPadre(this.gameObject);
        escuadra.transform.position = posSalida.position;
    }
    private void ExitAtacando()
    {

    }
    private void UpdateAtacando()
    {

    }

    // APUNTANDO

    private void EnterApuntando()
    {
        apuntarCorrutina = StartCoroutine(TiempoApuntandoParaAtacar());
    }
    private void ExitApuntando()
    {
        if (apuntarCorrutina != null)
        {
            StopCoroutine(apuntarCorrutina);
            apuntarCorrutina = null;
        }
    }
    private void UpdateApuntando()
    {
        if (targetPlayer)
        {
            if (targetPlayer.transform.position.x < this.gameObject.transform.position.x) // esta el enemigo a la izquierda
            {
                direccion = false;
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else if (targetPlayer.transform.position.x > this.gameObject.transform.position.x) // esta el enemigo a la derecha
            {
                direccion = true;
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }
    
    public void CambiarApuntando()
    {
        estadoPlaceholder = Estado.APUNTANDO;
        if (estadoActual != Estado.CANSADO)
            ChangeState(Estado.APUNTANDO);
    }

    // VUELTA
    private void EnterVuelta()
    {
        if (estaDentro)
            ChangeState(Estado.APUNTANDO);
        else
            ChangeState(Estado.QUIETO);
    }
    private void ExitVuelta()
    {

    }
    private void UpdateVuelta()
    {

    }

    public void CambiarVuelta()
    {
        estadoPlaceholder = Estado.VUELTA;
        if (estadoActual != Estado.CANSADO)
            ChangeState(Estado.VUELTA);
    }


    // CANSADO
    private void EnterCansado()
    {
        this.gameObject.GetComponent<Animator>().Play("CANSADO");
        StartCoroutine(SacarCansado());
    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }
    public void Cansado()
    {
        ChangeState(Estado.CANSADO);
    }
    private IEnumerator SacarCansado()
    {
        yield return new WaitForSeconds(3f);
        ChangeState(estadoPlaceholder);
    }

    public void setTarget(GameObject go)
    {
        targetPlayer = go;
    }

    public void DevolverPool(GameObject go)
    {
        pool.ReturnElement(go);
    }

    public void SetEstaDentro(bool b)
    {
        estaDentro = b;
    }

    private IEnumerator TiempoApuntandoParaAtacar()
    {
        yield return new WaitForSeconds(2f);
        ChangeState(Estado.ATACANDO);
    }


    // Relacionado con la vida del enemigo
    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o + 5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }
    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }
    private void BajarVidaGrito()
    {
        vidaEnemigo -= (int)vidaEnemigo * 0.2f;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }

    private void Muerte()
    {
        ChangeState(Estado.CANSADO);
        muerto = true;
        StopAllCoroutines();
        this.gameObject.GetComponent<Animator>().Play("MUERTEFRIKI");
    }
    private void EliminarFriki()
    {
        int random = Random.Range(0, 10);
        if (random < 30)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(posSalida.position.x, posSalida.position.y + .5f, 0);

        }
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(3);
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();
        if (collision.gameObject.tag.Equals("Grito"))
        {
            Cansado();
            BajarVidaGrito();
        }
    }


}
