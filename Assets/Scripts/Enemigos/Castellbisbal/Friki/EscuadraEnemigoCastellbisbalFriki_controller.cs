using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscuadraEnemigoCastellbisbalFriki_controller : MonoBehaviour
{
    public enum Estado { DER, IZC };
    [SerializeField] private Estado estadoActual;

    public enum SubEstado { ARRIBA, ABAJO };
    [SerializeField] private SubEstado subEstadoActual;

    private EnemigoCastellbisbalFriki_controller enemigoPadre;
    private bool rotacion;

    [SerializeField] private GameEventInteger gei;



    void Start()
    {
        StartCoroutine(QuitarPoner());
        ChangeSubState(SubEstado.ABAJO);
    }

    // Update is called once per frame
    void Update()
    {
        StateOnUpdate();
        if(rotacion)
            this.gameObject.transform.Rotate(new Vector3(0, 0, 360) * Time.deltaTime * 3);
        else
            this.gameObject.transform.Rotate(new Vector3(0, 0, -360) * Time.deltaTime * 3);
    }



    // ESTADO
    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.DER:
                break;
            case Estado.IZC:
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.DER:
                StartCoroutine(VolverIzquierda());
                break;
            case Estado.IZC:
                StartCoroutine(VolverDerecha());
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.DER:
                Derecha();
                break;
            case Estado.IZC:
                Izquierda();
                break;
        }
    }


    // SUBESTADO
    public void ChangeSubState(SubEstado NextState)
    {
        SubStateOnExit();

        SubStateOnEnter(NextState);

        SubStateOnUpdate();
    }

    private void SubStateOnExit()
    {
        switch (subEstadoActual)
        {
            case SubEstado.ARRIBA:
                break;
            case SubEstado.ABAJO:
                break;
        }
    }

    private void SubStateOnEnter(SubEstado nextState)
    {
        subEstadoActual = nextState;

        switch (subEstadoActual)
        {
            case SubEstado.ARRIBA:
                StartCoroutine(IrArriba());
                break;
            case SubEstado.ABAJO:
                StartCoroutine(IrAbajo());
                break;
        }
    }

    private void SubStateOnUpdate()
    {
        switch (subEstadoActual)
        {
            case SubEstado.ARRIBA:
                Arriba();
                break;
            case SubEstado.ABAJO:
                Abajo();
                break;
        }
    }


    private void Derecha()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(7, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);

    }
    private void Izquierda()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-7, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }

    private void Arriba()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(this.gameObject.GetComponent<Rigidbody2D>().velocity.x, 1.5f);
    }
    private void Abajo()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(this.gameObject.GetComponent<Rigidbody2D>().velocity.x, -1.5f);
    }



    // CORRUTINAS
    private IEnumerator VolverDerecha()
    {
        yield return new WaitForSeconds(0.8f);
        ChangeState(Estado.DER);
    }
    private IEnumerator VolverIzquierda()
    {
        yield return new WaitForSeconds(0.8f);
        ChangeState(Estado.IZC);
    }
    private IEnumerator IrAbajo()
    {
        yield return new WaitForSeconds(0.4f);
        ChangeSubState(SubEstado.ARRIBA);
    }
    private IEnumerator IrArriba()
    {
        yield return new WaitForSeconds(0.8f);
        ChangeSubState(SubEstado.ABAJO);
    }
    private IEnumerator QuitarPoner()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }




    // SETERS GETERS
    public void SetRotacion(bool b)
    {
        rotacion = b;
    }
    public void SetPadre(GameObject go)
    {
        enemigoPadre = go.GetComponent<EnemigoCastellbisbalFriki_controller>();
    }


    // COLISIONES
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(15);//da�o

        if (collision.gameObject.layer == 29)
        {
            // devolver a la pool
            enemigoPadre.DevolverPool(this.gameObject);
            enemigoPadre.CambiarVuelta();
        }
    }

}
