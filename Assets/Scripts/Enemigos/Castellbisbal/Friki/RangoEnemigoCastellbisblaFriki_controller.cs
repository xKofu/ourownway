using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoCastellbisblaFriki_controller : MonoBehaviour
{
    [SerializeField] private EnemigoCastellbisbalFriki_controller enemigoPadre;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarApuntando();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player") { 
            enemigoPadre.setTarget(collision.gameObject);
            enemigoPadre.SetEstaDentro(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            enemigoPadre.CambiarQuieto();
            enemigoPadre.SetEstaDentro(false);
        }
    }
}
