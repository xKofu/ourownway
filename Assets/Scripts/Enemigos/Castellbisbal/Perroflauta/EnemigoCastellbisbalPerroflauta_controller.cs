using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCastellbisbalPerroflauta_controller : MonoBehaviour
{

    [SerializeField] private GameObject targetPlayer;
    [SerializeField] private Transform puntoInicio;

    [Header("Punto para un raycast para que no se caiga por huecos")]
    [SerializeField] private Transform puntoRaycast;
    [Header("Cosas que toca el RayCast")]
    [SerializeField] private LayerMask Mascara;


    public enum Estado { SEGUIR, VOLVER, QUIETO, CANSADO };

    [SerializeField] private Estado estadoActual;
    private Estado estadoPlaceholder;

    private bool direccionVuelta; // true = mirando a la derecha y girar a la izquierda // true = mirando a la izquierda y girar a la derecha
    private bool sePuedeMover = true;
    private bool comprobarCansado = false;

    [Header("VIDA")]
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;
    [SerializeField] private float vidaEnemigo;

    [SerializeField] private GameObject pelotaExtra;

    private bool muerto = false;
    void Start()
    {
        muerto = false;
        vidaEnemigo = sv.VidaMaxima;
        ChangeState(Estado.QUIETO);
    }

    void Update()
    {
        if (!muerto)
        {
            if (sePuedeMover && !comprobarCansado)
                StateOnUpdate();
            RayCast();
        }
    }




    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                break;
            case Estado.VOLVER:
                break;
            case Estado.SEGUIR:
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }


    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.QUIETO:
                GetComponent<Animator>().Play("IDLE_CUCHILLO");
                break;
            case Estado.VOLVER:
                GetComponent<Animator>().Play("WALK_CUCHILLO");
                StartCoroutine(GirarVuelta());
                break;
            case Estado.SEGUIR:
                GetComponent<Animator>().Play("WALK_CUCHILLO");
                StopCoroutine(GirarVuelta());
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }



    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                Quieto();
                break;
            case Estado.VOLVER:
                Volver();
                break;
            case Estado.SEGUIR:
                Seguir();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }


    private void Quieto()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        GetComponent<Animator>().Play("IDLE_CUCHILLO");
    }


    public void Seguir()
    {
        if (targetPlayer) {
            if (transform.position.x > targetPlayer.transform.position.x - 0.5 && transform.position.x < targetPlayer.transform.position.x + 0.5)
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            else if (transform.position.x < targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                direccionVuelta = true;
            }
            else if (transform.position.x > targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
    }

    public void Volver()
    {
        //transform.LookAt(puntoInicio);
        //GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0);
        if (transform.position.x == puntoInicio.transform.position.x || (transform.position.x < puntoInicio.transform.position.x +1 && transform.position.x > puntoInicio.transform.position.x - 1))
            ChangeState(Estado.QUIETO);

        else if (transform.position.x < puntoInicio.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(3f, GetComponent<Rigidbody2D>().velocity.y);
            //transform.rotation = Quaternion.Euler(0, 180, 0);
            direccionVuelta = true;
        }
        else if (transform.position.x > puntoInicio.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-3f, GetComponent<Rigidbody2D>().velocity.y);
            //transform.rotation = Quaternion.Euler(0, 0, 0);
            direccionVuelta = false;
        }
    }

    public void setTarget(GameObject target)
    {
        targetPlayer = target;
    }





    private void RayCast()
    {
        Debug.DrawLine(puntoRaycast.position, new Vector2(puntoRaycast.position.x, puntoRaycast.position.y - 2), Color.red);
        if (Physics2D.Raycast(puntoRaycast.position, Vector2.down, 2, Mascara))
            sePuedeMover = true;
        else
        {
            sePuedeMover = false;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            if (estadoActual == Estado.VOLVER)
                sePuedeMover = true;
        }
    }
    private IEnumerator GirarVuelta()
    {
        yield return new WaitForSeconds(1);
        if (direccionVuelta)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    private IEnumerator DejarQuietoCorrutina()
    {
        comprobarCansado = true;
        sePuedeMover = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        GetComponent<Animator>().Play("CANSADO");
        yield return new WaitForSeconds(3f);
        comprobarCansado = false;
        sePuedeMover = true;
        GetComponent<Animator>().Play("IDLE_CUCHILLO");
    }





    public void CambiarSeguir()
    {
        if(comprobarCansado)
            estadoPlaceholder = Estado.SEGUIR;
        else
            ChangeState(Estado.SEGUIR);

    }

    public void CambiarVolver()
    {
        if (comprobarCansado)
            estadoPlaceholder = Estado.VOLVER;
        else
            ChangeState(Estado.VOLVER);
    }



    // CANSADO
    private void EnterCansado()
    {
        comprobarCansado = true;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        this.gameObject.GetComponent<Animator>().Play("CANSADO");
        StartCoroutine(SacarCansado());
    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }
    public void Cansado()
    {
        ChangeState(Estado.CANSADO);
    }
    private IEnumerator SacarCansado()
    {
        yield return new WaitForSeconds(3f);
        comprobarCansado = false;
        ChangeState(estadoPlaceholder);
    }





    private void Muerte()
    {
        ChangeState(Estado.CANSADO);
        StopAllCoroutines();
        muerto = true;
        this.gameObject.GetComponent<Animator>().Play("MUERTEPERROFLAUTA");
    }

    private void ElmiminarPerroflauta()
    {
        int random = Random.Range(0, 10);
        if (random < 3)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y+1.5f, 0);

        }
        Destroy(this.gameObject);
    }
    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }

    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o+5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }

    private void BajarVidaGrito()
    {
        vidaEnemigo -= (int) vidaEnemigo * 0.2f;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }




    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(5);
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();

        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();
        if (collision.gameObject.tag.Equals("Grito"))
        {
            Cansado();
            BajarVidaGrito();
        }
    }


}
