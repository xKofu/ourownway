using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoCastellbisbalPerroflauta_controller : MonoBehaviour
{
    [SerializeField] private EnemigoCastellbisbalPerroflauta_controller enemigoPadre;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.setTarget(collision.gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarSeguir();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarVolver();
    }
}
