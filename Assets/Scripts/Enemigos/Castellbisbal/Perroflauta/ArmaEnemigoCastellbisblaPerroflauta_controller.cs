using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaEnemigoCastellbisblaPerroflauta_controller : MonoBehaviour
{
    [SerializeField] private EnemigoCastellbisbalPerroflauta_controller padreEnemigo;
    [SerializeField] private GameEventInteger gei;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            padreEnemigo.Cansado();
            gei.Raise(20);
        }
    }
}
