using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoPaloma_controller : MonoBehaviour
{
    [Header("Posicion inicial y final")]
    [SerializeField] private Transform posicion1;
    [SerializeField] private Transform posicion2;

    [Header("pool de caca")]
    [SerializeField] private Pool pool;

    public enum Estado { DER, IZC };
    private Estado estadoActual;

    [Header("VIDA")]
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;
    [SerializeField] private float vidaEnemigo;

    private bool muerto = false;
    [SerializeField] private GameObject pelotaExtra;


    // Start is called before the first frame update
    void Start()
    {
        muerto = false;
        vidaEnemigo = sv.VidaMaxima;
        StartCoroutine(Ataque());
        ChangeState(Estado.DER);
    }

    // Update is called once per frame
    void Update()
    {
        if(!muerto)
            StateOnUpdate();
    }

    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.DER:
                break;
            case Estado.IZC:
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.DER:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case Estado.IZC:
                transform.rotation = Quaternion.Euler(0, 180, 0);
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.DER:
                Derecha();
                break;
            case Estado.IZC:
                Izquierda();
                break;
        }
    }


    private void Derecha()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
        if (this.gameObject.transform.position.x > posicion2.position.x)
            ChangeState(Estado.IZC);
    }

    private void Izquierda()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
        if (this.gameObject.transform.position.x < posicion1.position.x)
            ChangeState(Estado.DER);
    }



    public void DevolverPool(GameObject go)
    {
        pool.ReturnElement(go);
    }

    private IEnumerator Ataque()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(2,5));
            GameObject caca = pool.getElement();
            caca.transform.position = this.gameObject.transform.position;
            caca.GetComponent<CacaPaloma_controller>().SetPadre(this.gameObject);
        }
    }



    private void Morir()
    {
        muerto = true;
        StopAllCoroutines();
        this.GetComponent<Animator>().Play("MUERTEPALOMA");
    }

    private void EliminarPaloma()
    {
        int random = Random.Range(0, 10);
        if (random < 3)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1.5f, 0);

        }
        Destroy(this.gameObject);
    }
    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o + 5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Morir();
    }
    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Morir();
    }
    private void BajarVidaGrito()
    {
        vidaEnemigo -= sv.VidaMaxima;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Morir();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(3);
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();
        if (collision.gameObject.tag.Equals("Grito"))
            BajarVidaGrito();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();
    }
}
