using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CacaPaloma_controller : MonoBehaviour
{
    private EnemigoPaloma_controller enemigoPadre;

    [SerializeField] private GameEventInteger gei;

    void Start()
    {
        StartCoroutine(Devolver());
    }

    public void SetPadre(GameObject go)
    {
        enemigoPadre = go.GetComponent<EnemigoPaloma_controller>();
    }

    private IEnumerator Devolver()
    {
        yield return new WaitForSeconds(3f);
        enemigoPadre.DevolverPool(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(10);
    }
}
