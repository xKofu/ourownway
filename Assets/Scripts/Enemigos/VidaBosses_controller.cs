using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VidaBosses_controller : MonoBehaviour
{
    [Header("VIDA")]
    [SerializeField]private Image imagenVida;
    [SerializeField] private ScriptableVida sv;


    public void ActualizarVida()
    {
        imagenVida.fillAmount = (sv.VidaActual / sv.VidaMaxima);
    }
}
