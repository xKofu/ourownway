using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VidaPlayersHUD_controller : MonoBehaviour
{


    [SerializeField] private Image vidaPau;
    [SerializeField] private Image vidaGerard;

    [SerializeField] private ScriptableVida svPau;
    [SerializeField] private ScriptableVida svGerard;



    public void ModificarVida()
    {
        vidaPau.fillAmount = (svPau.VidaActual / svPau.VidaMaxima);
        vidaGerard.fillAmount = (svGerard.VidaActual / svGerard.VidaMaxima);
    }

}
