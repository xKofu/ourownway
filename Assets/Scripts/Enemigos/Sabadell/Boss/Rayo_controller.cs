using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rayo_controller : MonoBehaviour
{
    private bool direccion;
    private bool sePuedeMover = false;
    [SerializeField] private GameEventInteger gei;

    private void Awake()
    {
        sePuedeMover = false;
    }
    void Update()
    {
        if (sePuedeMover)
        {
            if (direccion)
                this.gameObject.transform.Rotate(new Vector3(0, 0, -180) * Time.deltaTime);
            else
                this.gameObject.transform.Rotate(new Vector3(0, 0, 180) * Time.deltaTime);
        }
    }

    public void Activar()
    {
        sePuedeMover = true;
    }
    public void Desactivar()
    {
        sePuedeMover = false;
    }

    public void SetDireccion(bool b)
    {
        direccion = b;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            gei.Raise(10);
        }
    }

}
