using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Martillo_controller : MonoBehaviour
{
    public delegate void devolverMartillo(GameObject go);
    public event devolverMartillo onDevolverMartillo;
    [SerializeField] private GameEventInteger gei;


    private void Update()
    {
        this.gameObject.transform.Rotate(new Vector3(0, 0, -360) * Time.deltaTime);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("DevolverMartillo"))
            onDevolverMartillo(this.gameObject);


        print("El MARTILLO sale de --> " + collision.gameObject.tag);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            gei.Raise(15);
        }
    }
}
