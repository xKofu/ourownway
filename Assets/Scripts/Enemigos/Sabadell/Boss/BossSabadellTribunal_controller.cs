using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSabadellTribunal_controller : MonoBehaviour
{
    [Header("PLAYER")]
    [SerializeField] private Transform targetPlayer;
    [SerializeField] private ScriptableEstadisticas se;
    [SerializeField] private GameEventInteger hacerDanyoPlayer;


    // ESTADOS
    public enum Estado { PENSADO, ATAQUE1, ATAQUE2, ATAQUE3, ESPERANDO, CANSADO };
    [Header("Estado actual del enemigo")]
    [SerializeField] private Estado estadoActual;


    // VIDA
    [Header("Vida Boss")]
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private GameEvent eventoBajarVida;
    [SerializeField] private GameEvent eventoTrinidadMuerta;

    // ATAQUE 1 Rumbas y Martillos
    [Header("ATAQUE 1")]
    [SerializeField] Pool poolRumbas;
    [SerializeField] Pool poolMartillos;
    [SerializeField] Transform[] posicionSalidaRumbas;
    [SerializeField] Transform[] posicionSalidaMartillos;
    private Transform placeHolderTransform;

    // ATAQUE 2
    [Header("ATAQUE 2")]
    [SerializeField] private GameObject rayo1;
    [SerializeField] private GameObject rayo2;
    [SerializeField] private GameObject zonaSeguraRayo;
    [SerializeField] private GameObject limite;

    // ATAQUE 3
    [Header("ATAQUE 3")]
    [SerializeField] private GameObject columnas;
    private int numeroDeArcoirisSacados;


    private int numeroAtaques;
    private int numeroDeAtaques1IgualesSeguidos;
    private int numeroDeAtaques2IgualesSeguidos;


    [SerializeField] private GameObject Muro;



    private bool animacionEntradaSalida; // true => Me he puesto detras de la mesa // false => Me he puesto delante de la mesa
    private string idle;


    void Start()
    {
        idle = "IDLEFUERA";
        animacionEntradaSalida = false;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        sv.VidaActual = sv.VidaMaxima;
        ChangeState(Estado.PENSADO);
    }

    void Update()
    {

    }

    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                ExitPensando();
                break;
            case Estado.ATAQUE1:
                ExitAtaque1();
                break;
            case Estado.ATAQUE2:
                ExitAtaque2();
                break;
            case Estado.ATAQUE3:
                ExitAtaque3();
                break;
            case Estado.ESPERANDO:
                ExitEsperando();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.PENSADO:
                EnterPensando();
                break;
            case Estado.ATAQUE1:
                EnterAtaque1();
                break;
            case Estado.ATAQUE2:
                EnterAtaque2();
                break;
            case Estado.ATAQUE3:
                EnterAtaque3();
                break;
            case Estado.ESPERANDO:
                EnterEsperando();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                UpdatePensando();
                break;
            case Estado.ATAQUE1:
                UpdateAtaque1();
                break;
            case Estado.ATAQUE2:
                UpdateAtaque2();
                break;
            case Estado.ATAQUE3:
                UpdateAtaque3();
                break;
            case Estado.ESPERANDO:
                UpdateEsperando();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }


    public void EmpezarIdleMesa()
    {
        this.gameObject.GetComponent<Animator>().Play("IDLEMESA");
    }

    // ATAQUE 1

    private void EnterAtaque1()
    {
        idle = "IDLEMESA";
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        StopAllCoroutines();
        if (!animacionEntradaSalida)
        {
            this.gameObject.GetComponent<Animator>().Play("SALTARMESA");
            animacionEntradaSalida = true;
            StartCoroutine(TiempoAnimacionSaltarMesaAtaque1());
        }
        else
        {
            this.gameObject.GetComponent<Animator>().Play("ANIMATAQUE1");
        }
    }

    private void ExitAtaque1()
    {
    }
    private void UpdateAtaque1()
    {

    }

    private IEnumerator LanzarMartillos()
    {
        Shuffle();
        yield return new WaitForSeconds(1f);
        GameObject m0 = poolMartillos.getElement();
        m0.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m0.transform.position = posicionSalidaMartillos[0].position;

        yield return new WaitForSeconds(1f);
        GameObject m1 = poolMartillos.getElement();
        m1.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m1.transform.position = posicionSalidaMartillos[1].position;

        yield return new WaitForSeconds(1f);
        GameObject m2 = poolMartillos.getElement();
        m2.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m2.transform.position = posicionSalidaMartillos[2].position;

        yield return new WaitForSeconds(1f);
        GameObject m3 = poolMartillos.getElement();
        m3.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m3.transform.position = posicionSalidaMartillos[3].position;

        yield return new WaitForSeconds(1f);
        GameObject m4 = poolMartillos.getElement();
        m4.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m4.transform.position = posicionSalidaMartillos[4].position;



        Shuffle();
        yield return new WaitForSeconds(1f);
        GameObject m02 = poolMartillos.getElement();
        m02.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m02.transform.position = posicionSalidaMartillos[0].position;

        yield return new WaitForSeconds(1f);
        GameObject m12 = poolMartillos.getElement();
        m12.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m12.transform.position = posicionSalidaMartillos[1].position;

        yield return new WaitForSeconds(1f);
        GameObject m22 = poolMartillos.getElement();
        m22.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m22.transform.position = posicionSalidaMartillos[2].position;

        yield return new WaitForSeconds(1f);
        GameObject m32 = poolMartillos.getElement();
        m32.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m32.transform.position = posicionSalidaMartillos[3].position;

        yield return new WaitForSeconds(1f);
        GameObject m42 = poolMartillos.getElement();
        m42.GetComponent<Martillo_controller>().onDevolverMartillo += DevolverMartillo;
        m42.transform.position = posicionSalidaMartillos[4].position;


    }


    private IEnumerator DevolverRumbas(GameObject go, GameObject go2)
    {
        yield return new WaitForSeconds(10f);
        go.GetComponent<Rumba_controller>().PararCorrutina();
        go2.GetComponent<Rumba_controller>().PararCorrutina();

        poolRumbas.ReturnElement(go);
        poolRumbas.ReturnElement(go2);
    }

    private void DevolverMartillo(GameObject go)
    {
        go.GetComponent<Martillo_controller>().onDevolverMartillo -= DevolverMartillo;
        poolMartillos.ReturnElement(go);
    }

    private void Shuffle()
    {
        for (int i = 0; i < posicionSalidaMartillos.Length; i++)
        {
            int rand = Random.Range(0, posicionSalidaMartillos.Length);
            placeHolderTransform = posicionSalidaMartillos[rand];
            posicionSalidaMartillos[rand] = posicionSalidaMartillos[i];
            posicionSalidaMartillos[i] = placeHolderTransform;
        }
    }


    public void InicarAtaque1()
    {
        this.gameObject.GetComponent<Animator>().Play(idle);
        // SACAR RUMBAS
        GameObject rumba1 = poolRumbas.getElement();
        rumba1.transform.position = posicionSalidaRumbas[0].position;
        rumba1.GetComponent<Rumba_controller>().EstadoDerecha();

        GameObject rumba2 = poolRumbas.getElement();
        rumba2.transform.position = posicionSalidaRumbas[1].position;
        rumba2.GetComponent<Rumba_controller>().EstadoIzquierda();
        StartCoroutine(DevolverRumbas(rumba1, rumba2));

        // SACAR MARTILLOS
        StartCoroutine(LanzarMartillos());
        StartCoroutine(AcabarAtaque1());
    }

    private IEnumerator TiempoAnimacionSaltarMesaAtaque1()
    {
        yield return new WaitForSeconds(4f);
        this.gameObject.GetComponent<Animator>().Play("ANIMATAQUE1");
    }


    private IEnumerator AcabarAtaque1()
    {
        yield return new WaitForSeconds(15f);
        ChangeState(Estado.PENSADO);
    }


    // ATAQUE 2
    private void EnterAtaque2()
    {
        idle = "IDLEMESA";
        limite.SetActive(false);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        StopAllCoroutines();
        if (!animacionEntradaSalida)
        {
            this.gameObject.GetComponent<Animator>().Play("SALTARMESA");
            animacionEntradaSalida = true;
            StartCoroutine(TiempoAnimacionSaltarMesaAtaque2());
        }
        else
            IniciarAtaque2();
    }
    private void ExitAtaque2()
    {
        // RAYO
        limite.SetActive(true);
        rayo1.GetComponent<Rayo_controller>().Desactivar();
        rayo2.GetComponent<Rayo_controller>().Desactivar();
        rayo1.SetActive(false);
        rayo2.SetActive(false);

        // ZONA SEGURA
        zonaSeguraRayo.SetActive(false);
    }
    private void UpdateAtaque2()
    {

    }

    public void Ataque2Finalizado()
    {
        zonaSeguraRayo.GetComponent<ZonaSeguraRayo_controller>().PararZonaSegura();
        ChangeState(Estado.PENSADO);
    }

    private IEnumerator ActivarRayos()
    {
        yield return new WaitForSeconds(1f);
        rayo1.GetComponent<Rayo_controller>().Activar();
        rayo2.GetComponent<Rayo_controller>().Activar();
    }


    private IEnumerator TiempoAnimacionSaltarMesaAtaque2()
    {
        yield return new WaitForSeconds(4f);
        IniciarAtaque2();
    }

    private void IniciarAtaque2()
    {
        // RAYOS
        rayo1.SetActive(true);
        rayo1.transform.Rotate(0, 0, 0);
        rayo1.GetComponent<BoxCollider2D>().enabled = true;
        rayo1.GetComponent<Rayo_controller>().SetDireccion(false);
        rayo2.SetActive(true);
        rayo2.transform.Rotate(0, 0, 0);
        rayo2.GetComponent<BoxCollider2D>().enabled = true;
        rayo2.GetComponent<Rayo_controller>().SetDireccion(true);

        // ZONA SEGURA
        zonaSeguraRayo.SetActive(true);
        zonaSeguraRayo.transform.position = new Vector3(0f, -2.1f, 0);
        zonaSeguraRayo.GetComponent<ZonaSeguraRayo_controller>().IniciarZonaSegura();

        StartCoroutine(ActivarRayos());
    }



    // ATAQUE 3
    private void EnterAtaque3()
    {
        StopAllCoroutines();
        idle = "IDLEFUERA";
        if (animacionEntradaSalida)
        {
            this.gameObject.GetComponent<Animator>().Play("SALIRMESA");
            animacionEntradaSalida = false;
        }
        else 
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        //columnas
        numeroDeArcoirisSacados = 0;
        StartCoroutine(AnimacionSeleccionarSitio());
    }
    private void ExitAtaque3()
    {

    }
    private void UpdateAtaque3()
    {

    }
    
    private IEnumerator AnimacionSeleccionarSitio()
    {
        yield return new WaitForSeconds(2.1f);
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
        this.gameObject.GetComponent<Animator>().Play("APUNTARARCOIRIS");
        yield return new WaitForSeconds(3.5f);
        SacarArcoiris();
    }
    private void SacarArcoiris()
    {
        this.gameObject.GetComponent<Animator>().Play("SACARARCOIRIS");
        float xPlayer = targetPlayer.position.x;
        if (xPlayer > 4f)
            xPlayer = 5f;
        columnas.transform.position = new Vector3(xPlayer, columnas.transform.position.y,0);
        columnas.GetComponent<Animator>().Play("ATAQUE3");
        StartCoroutine(EsperarArcoiris());
    }

    private IEnumerator EsperarArcoiris()
    {
        yield return new WaitForSeconds(3f);
        columnas.GetComponent<Animator>().Play("IDLEATAQUE3");
        numeroDeArcoirisSacados++;
        if (numeroDeArcoirisSacados == 3)
            ChangeState(Estado.ESPERANDO);
        else
            StartCoroutine(AnimacionSeleccionarSitio());
    }



    // PENSADO
    private void EnterPensando()
    {
        this.gameObject.GetComponent<Animator>().Play(idle);
        StopAllCoroutines();
        StartCoroutine(PensadoAtaque());
    }
    private void ExitPensando()
    {

    }
    private void UpdatePensando()
    {

    }

    private IEnumerator PensadoAtaque()
    {
        yield return new WaitForSeconds(4f);
        numeroAtaques++;
        if (numeroAtaques > 3)
        {
            //ChangeState(Estado.ESPERANDO);
            //numeroAtaques = 0;
            int numeroAtaque = Random.Range(0, 20);

            if (numeroAtaque < 4)
            {
                ChangeState(Estado.ATAQUE1);
            }
            else if (numeroAtaque >= 4 && numeroAtaque <= 17)
            {
                ChangeState(Estado.ATAQUE3);
                numeroAtaques = 0;
            }
            else if (numeroAtaque > 17)
            {
                ChangeState(Estado.ATAQUE2);
            }
        }
        else
        {
            int numeroAtaque = Random.Range(0, 2);
            if(numeroDeAtaques1IgualesSeguidos >= 2)
            {
                ChangeState(Estado.ATAQUE2);
                numeroDeAtaques1IgualesSeguidos = 0;
                numeroDeAtaques2IgualesSeguidos = 0;
            }
            else if(numeroDeAtaques2IgualesSeguidos >= 2)
            {
                ChangeState(Estado.ATAQUE1);
                numeroDeAtaques1IgualesSeguidos = 0;
                numeroDeAtaques2IgualesSeguidos = 0;
            }
            switch (numeroAtaque)
            {
                case 0:
                    ChangeState(Estado.ATAQUE1);
                    numeroDeAtaques1IgualesSeguidos++;
                    numeroDeAtaques2IgualesSeguidos = 0;
                    break;
                case 1:
                    ChangeState(Estado.ATAQUE1);
                    numeroDeAtaques2IgualesSeguidos++;
                    numeroDeAtaques1IgualesSeguidos = 0;
                    break;
            }
        }
    }

    // ESPERANDO
    private void EnterEsperando()
    {
        StopAllCoroutines();
        StartCoroutine(Esperando());
    }
    private void ExitEsperando()
    {

    }
    private void UpdateEsperando()
    {

    }
    private IEnumerator Esperando()
    {
        yield return new WaitForSeconds(5f);
        ChangeState(Estado.PENSADO);
    }


    // CANSADO 
    private void EnterCansado()
    {

    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }



    // FUNCIONES
    private void BajarVidaGolpe()
    {
        sv.VidaActual -= se.da�o;
        eventoBajarVida.Raise();
        ComprobarMuerto();
    }

    private void BajarVidaPelota()
    {
        sv.VidaActual -= se.da�o+5;
        eventoBajarVida.Raise();
        
        ComprobarMuerto();
    }

    private void ComprobarMuerto()
    {
        if (sv.VidaActual <= 0)
        {
            StopAllCoroutines();
            this.gameObject.GetComponent<Animator>().Play("MUERTE");
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            eventoTrinidadMuerta.Raise();
        }
    }

    public void EliminarTribunal()
    {
        Muro.SetActive(false);
        this.gameObject.SetActive(false);
    }






    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();

        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();

        if (collision.gameObject.tag.Equals("Player"))
            hacerDanyoPlayer.Raise(10);
    }


}
