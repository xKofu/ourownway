using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ZonaSeguraRayo_controller : MonoBehaviour
{

    [SerializeField] private GameEvent eventoEntro;
    [SerializeField] private GameEvent eventoSalgo;

    [SerializeField] private GameEvent finalizoRuta;

    public enum Estado { DERECHA, IZQUIERDA, QUIETO};
    [SerializeField] private Estado estadoActual;

    private Estado[] comprobacionMovimientos = new Estado[3];
    private Estado placeHolderEstado;


    private Coroutine corrutunaMovimiento;

    void Start()
    {
        comprobacionMovimientos[0] = Estado.DERECHA;
        comprobacionMovimientos[1] = Estado.IZQUIERDA;
        comprobacionMovimientos[2] = Estado.QUIETO;
    }



    void Update()
    {
        StateOnUpdate();
    }


    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.DERECHA:
                ExitDerecha();
                break;
            case Estado.IZQUIERDA:
                ExitIzquierda();
                break;
            case Estado.QUIETO:
                ExitQuieto();
                break;
        }
    }


    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.DERECHA:
                EnterDerecha();
                break;
            case Estado.IZQUIERDA:
                EnterIzquierda();
                break;
            case Estado.QUIETO:
                EnterQuieto();
                break;
        }
    }



    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.DERECHA:
                UpdateDerecha();
                break;
            case Estado.IZQUIERDA:
                UpdateIzquierda();
                break;
            case Estado.QUIETO:
                UpdateQuieto();
                break;
        }
    }







    // DERECHA
    private void EnterDerecha()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
    }

    private void ExitDerecha()
    {

    }

    private void UpdateDerecha()
    {

    }



    // IZQUIERDA
    private void EnterIzquierda()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2, 0);
    }
    private void ExitIzquierda()
    {

    }
    private void UpdateIzquierda()
    {

    }



    // QUIETO

    private void EnterQuieto()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }
    private void ExitQuieto()
    {

    }
    private void UpdateQuieto()
    {

    }


    // FUNCIONES 
    public void IniciarZonaSegura()
    {
        corrutunaMovimiento = StartCoroutine(IniciarRuta());
    }

    public void PararZonaSegura()
    {
        if (corrutunaMovimiento != null)
        {
            StopCoroutine(corrutunaMovimiento);
            corrutunaMovimiento = null;
        }
    }

    private void Shuffle()
    {
        for (int i = 0; i < comprobacionMovimientos.Length; i++)
        {
            int rand = Random.Range(0, comprobacionMovimientos.Length);
            placeHolderEstado = comprobacionMovimientos[rand];
            comprobacionMovimientos[rand] = comprobacionMovimientos[i];
            comprobacionMovimientos[i] = placeHolderEstado;
        }
    }


    private IEnumerator IniciarRuta()
    {
        Shuffle();
        yield return new WaitForSeconds(1f);
        ChangeState(comprobacionMovimientos[0]);
        yield return new WaitForSeconds(4f);
        ChangeState(comprobacionMovimientos[1]);
        yield return new WaitForSeconds(4f);
        ChangeState(comprobacionMovimientos[2]);
        yield return new WaitForSeconds(4f);
        finalizoRuta.Raise();
    }




    // COLLISIONES 

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            eventoEntro.Raise();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            eventoSalgo.Raise();
    }
}
