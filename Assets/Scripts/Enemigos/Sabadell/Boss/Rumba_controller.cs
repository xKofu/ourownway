using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rumba_controller : MonoBehaviour
{


    public enum Estado { DERECHA, IZQUIERDA};
    [SerializeField] private Estado estadoActual;

    private Coroutine cambioDeDireccion;

    [SerializeField] private GameEventInteger gei;



    void Update()
    {
        StateOnUpdate();
    }


    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.DERECHA:
                ExitDerecha();
                break;
            case Estado.IZQUIERDA:
                ExitIzquierda();
                break;
        }
    }
    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.DERECHA:
                EnterDerecha();
                break;
            case Estado.IZQUIERDA:
                EnterIzquierda();
                break;
        }
    }
    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.DERECHA:
                UpdateDerecha();
                break;
            case Estado.IZQUIERDA:
                UpdateIzquierda();
                break;
        }
    }





    // DERECHA
    private void EnterDerecha()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(16, 0);
        transform.rotation = Quaternion.Euler(0, 0, 0);
        cambioDeDireccion = StartCoroutine(CambiarDireccion());
    }

    private void ExitDerecha()
    {

    }

    private void UpdateDerecha()
    {

    }

    public void EstadoDerecha()
    {
        ChangeState(Estado.DERECHA);
    }



    // IZQUIERDA
    private void EnterIzquierda()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-16, 0);
        transform.rotation = Quaternion.Euler(0, 180, 0);
        cambioDeDireccion = StartCoroutine(CambiarDireccion());
    }
    private void ExitIzquierda()
    {

    }
    private void UpdateIzquierda()
    {

    }

    public void EstadoIzquierda()
    {
        ChangeState(Estado.IZQUIERDA);
    }


    private IEnumerator CambiarDireccion()
    {
        yield return new WaitForSeconds(2f);
        if (estadoActual == Estado.DERECHA)
            ChangeState(Estado.IZQUIERDA);
        else
            ChangeState(Estado.DERECHA);
    }




    public void PararCorrutina()
    {
        if(cambioDeDireccion != null)
        {
            StopCoroutine(cambioDeDireccion);
            cambioDeDireccion = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(10);
    }
}
