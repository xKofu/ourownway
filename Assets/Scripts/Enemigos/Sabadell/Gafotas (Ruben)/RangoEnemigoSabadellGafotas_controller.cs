using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoSabadellGafotas_controller : MonoBehaviour
{

    [SerializeField] private EnemigoSabadellGafotas_controller enemigoPadre;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.EntrarPlayer();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.tag == "Player")
            enemigoPadre.setTarget(collision.gameObject);
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.SalirPlayer();
    }
}
