using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilEnemigoSabadellGafotas_controller : MonoBehaviour
{

    private GameObject enemigoPadre;
    private GameObject targetPlayer;

    [SerializeField] private GameEventInteger gei;
    [SerializeField] private float tiempo = 2f;

    private bool direccionRotacion; // true esta a la derecha // false esta a la izquuierda

    private void Start()
    {
        this.gameObject.GetComponent<CircleCollider2D>().enabled = true;
    }

    private void Update()
    {
        if(direccionRotacion)
            this.gameObject.transform.Rotate(new Vector3(0, 0, 360) * Time.deltaTime);
        else
            this.gameObject.transform.Rotate(new Vector3(0, 0, -360) * Time.deltaTime);
    }

    public void setCanon(GameObject g)
    {
        enemigoPadre = g;
        float Vx = (targetPlayer.transform.position.x - enemigoPadre.transform.position.x) / tiempo;
        float gravity = -9.8f * GetComponent<Rigidbody2D>().gravityScale;
        float Vy = (targetPlayer.transform.position.y - enemigoPadre.transform.position.y - (gravity * tiempo * tiempo) / 2) / tiempo;    // estas cosas de aqui sin el canon tambin rulan
        GetComponent<Rigidbody2D>().velocity = new Vector2(Vx, Vy);
    }

    public void setTarget(GameObject g)
    {
        targetPlayer = g;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            gei.Raise(30);
            print("Toco con el player");
            if (enemigoPadre != null) // puede ser que el enemigo este "muerto" y no puede ser devolvido este proyectil
                enemigoPadre.GetComponent<EnemigoSabadellGafotas_controller>().devolverALaPool(gameObject);
        }

        if (collision.gameObject.layer == 8 || collision.gameObject.tag == "ground")
        {
            StartCoroutine(QuitarCollider());
            if (enemigoPadre)
                StartCoroutine(Devolver(this.gameObject));
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "LimiteMapa")
        {
            if (enemigoPadre != null) // puede ser que el enemigo este "muerto" y no puede ser devolvido este proyectil
                enemigoPadre.GetComponent<EnemigoSabadellGafotas_controller>().devolverALaPool(gameObject);
        }
    }

    public void SetDireccionRotacion(bool r)
    {
        direccionRotacion = r;
    }
    private IEnumerator Devolver(GameObject go)
    {
        yield return new WaitForSeconds(2f);
        if (enemigoPadre != null) // puede ser que el enemigo este "muerto" y no puede ser devolvido este proyectil
            enemigoPadre.GetComponent<EnemigoSabadellGafotas_controller>().devolverALaPool(go);
    }
    private IEnumerator QuitarCollider()
    {
        yield return new WaitForSeconds(0.1f);
        this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
    }
}
