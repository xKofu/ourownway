using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoSabadellGafotas_controller : MonoBehaviour
{
    [SerializeField] private Pool pool;

    [SerializeField] private bool puedeAtacar = false;
    [SerializeField]  private GameObject targetPlayer;
    [SerializeField] private Transform PosLanzamiento;
    [SerializeField] private GameEventInteger gei;

    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;

    [SerializeField] private float vidaEnemigo;
    [SerializeField] private GameObject pelotaExtra;

    private bool puedeAtacarPlaceholder;
    private bool muerto = false;
    private bool cansado = false;
    private Coroutine corrutinaCansado;


    private void Start()
    {
        vidaEnemigo = sv.VidaMaxima;
        StartCoroutine(atacar());
        cansado = false;
    }
    void Update()
    {
        MirarPlayer();

    }

    private IEnumerator atacar()
    {
        while (true)
        {
            if (puedeAtacar)
            {
                GetComponent<Animator>().Play("KANE_LANZARBOLA", GetComponent<Animator>().GetLayerIndex("Base Layer"), 0);
                yield return new WaitForSeconds(0.2f);
                GameObject proyectil = pool.getElement(); // pillo el elemento de la pool
                if (targetPlayer.transform.position.x < this.gameObject.transform.position.x)
                    proyectil.GetComponent<ProyectilEnemigoSabadellGafotas_controller>().SetDireccionRotacion(false);
                else
                    proyectil.GetComponent<ProyectilEnemigoSabadellGafotas_controller>().SetDireccionRotacion(true);
                proyectil.transform.position = PosLanzamiento.position; // pongo como posicion del proyectil la mia, tambie podrai ser otra con un transorm que le pasemos por referencia dependiendo de la animacion del ataque
                proyectil.GetComponent<ProyectilEnemigoSabadellGafotas_controller>().setTarget(targetPlayer);
                proyectil.GetComponent<ProyectilEnemigoSabadellGafotas_controller>().setCanon(this.gameObject);

            }
            yield return new WaitForSeconds(2f);
        }
    }
    public void devolverALaPool(GameObject a)
    {
        pool.ReturnElement(a);
    }

    public void setTarget(GameObject target)
    {
        targetPlayer = target;
    }


    private void MirarPlayer()
    {
        if (!muerto)
        {
            if (targetPlayer) // para que mire al player
            {
                if (targetPlayer.transform.position.x > gameObject.transform.position.x)
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                else
                    transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }


    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }
    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o + 5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }
    private void BajarVidaGrito()
    {
        vidaEnemigo -= (int) vidaEnemigo * 0.2f;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }


    private void Muerto()
    {
        muerto = true;
        StopAllCoroutines();
        this.gameObject.GetComponent<Animator>().Play("MUERTEKANE");
    }

    private void EliminarKane()
    {
        int random = Random.Range(0, 10);
        if (random < 30)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1.5f, 0);

        }
        Destroy(this.gameObject);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(5);
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();
        if (collision.gameObject.tag.Equals("Grito"))
        {
            Cansado();
            BajarVidaGrito();
        }
    }

    private void Cansado()
    {
        print("Llamar a Cansado Kane");
        if (corrutinaCansado != null)
        {
            StopCoroutine(corrutinaCansado);
            corrutinaCansado = null;
        }
        corrutinaCansado = StartCoroutine(CorrutinaCansado());
    }

    private IEnumerator CorrutinaCansado()
    {
        this.gameObject.GetComponent<Animator>().Play("CANSADO");
        puedeAtacar = false;
        cansado = true;
        yield return new WaitForSeconds(5f);
        puedeAtacar = puedeAtacarPlaceholder;
        cansado = false;
    }

    public void SalirPlayer()
    {
        puedeAtacarPlaceholder = false;
        if(!cansado)
            puedeAtacar = false;
    }

    public void EntrarPlayer()
    {
        puedeAtacarPlaceholder = true;
        if (!cansado)
            puedeAtacar = true;
    }

}
