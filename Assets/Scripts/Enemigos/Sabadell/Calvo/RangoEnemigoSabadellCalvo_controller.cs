using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoSabadellCalvo_controller : MonoBehaviour
{
    [SerializeField] private EnemigoSabadellCalvo_controller enemigoPadre;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarSeguir(); //enemigoPadre.ChangeState(EnemigoSabadellCalvo_controller.Estado.SEGUIR);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.setTarget(collision.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarVolver(); //enemigoPadre.ChangeState(EnemigoSabadellCalvo_controller.Estado.VOLVER);
    }
}
