using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoSabadellCalvo_controller : MonoBehaviour
{
    [Header("GO del jugador (esta para comprobaciones)")]
    [SerializeField] private GameObject targetPlayer;
    [Header("Punto de vuelta")]
    [SerializeField] private Transform puntoInicio;
    [Header("RayCast para no caerse")]
    [SerializeField] private Transform puntoRaycast;
    [SerializeField] private LayerMask Mascara;
    

    public enum Estado {SEGUIR, VOLVER, QUIETO, DEAMBULAR, CANSADO};

    [SerializeField]  private Estado estadoActual;

    private bool direccionVuelta; // true = mirando a la derecha y girar a la izquierda // false = mirando a la izquierda y girar a la derecha
    private bool direccionAtaque; // true = mirando a la derecha // fals = mirando a la izquierda

    private float vida = 10;

    private bool sePuedeMover = true;
    private bool comprobarCansado = false;

    [SerializeField] private float tiempoCargaAtaque = 0;
    [SerializeField] private float tiempoParaDeambular = 0;

    [SerializeField] private int direccionDeambular;

    [Header("VIDA")]
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;

    [SerializeField] private float vidaEnemigo;

    private bool muerto = false;

    [SerializeField] private GameObject pelotaExtra;

    private Estado estadoPlaceholder;


    private Coroutine corrutinaGirarVuelta, corrutinaPararAtaque, corrutinaDeambular;

    

    void Start()
    {
        vidaEnemigo = sv.VidaMaxima;
        estadoActual = Estado.QUIETO;   
    }

    void Update()
    {
        if (!muerto)
        {
            if (sePuedeMover && !comprobarCansado)
                StateOnUpdate();
            RayCast();
        }
    }


    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                ExitQuieto();
                break;
            case Estado.VOLVER:
                ExitVolver();
                break;
            case Estado.SEGUIR:
                ExitSeguir();
                break;
            case Estado.DEAMBULAR:
                ExitDeambular();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }


    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.QUIETO:
                EnterQuieto();
                break;
            case Estado.VOLVER:
                EnterVolver();
                break;
            case Estado.SEGUIR:
                EnterSeguir();
                break;
            case Estado.DEAMBULAR:
                EnterDeambular();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;

        }
    }



    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                UpdateQuieto();
                break;
            case Estado.VOLVER:
                UpdateVolver();
                break;
            case Estado.SEGUIR:
                UpdateSeguir();
                break;
            case Estado.DEAMBULAR:
                UpdateDeambular();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }

    // QUIETO
    private void EnterQuieto()
    {
        GetComponent<Animator>().Play("IDLE");
    }
    private void ExitQuieto()
    {

    }
    private void UpdateQuieto()
    {
        GetComponent<Animator>().Play("IDLE");
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        tiempoParaDeambular += Time.deltaTime;
        if (tiempoParaDeambular > 5)
            ChangeState(Estado.DEAMBULAR);
    }



    // VOLVER
    private void EnterVolver()
    {
        GetComponent<Animator>().Play("WALK");
        corrutinaGirarVuelta = StartCoroutine(GirarVuelta());
        if(corrutinaPararAtaque != null)
        {
            StopCoroutine(corrutinaPararAtaque);
            corrutinaPararAtaque = null;
        }
        if(corrutinaDeambular != null)
        {
            StopCoroutine(corrutinaDeambular);
            corrutinaDeambular = null;
        }
        tiempoParaDeambular = 0;
    }
    private void ExitVolver()
    {
        if (corrutinaGirarVuelta != null)
        {
            StopCoroutine(corrutinaGirarVuelta);
            corrutinaGirarVuelta = null;
        }
    }
    private void UpdateVolver()
    {
        if (transform.position.x == puntoInicio.transform.position.x || (transform.position.x < puntoInicio.transform.position.x + 0.5 && transform.position.x > puntoInicio.transform.position.x - 0.5))
            ChangeState(Estado.QUIETO);
        else if (transform.position.x < puntoInicio.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(3f, GetComponent<Rigidbody2D>().velocity.y);
            direccionVuelta = true;
        }
        else if (transform.position.x > puntoInicio.transform.position.x)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-3f, GetComponent<Rigidbody2D>().velocity.y);
            direccionVuelta = false;
        }
    }

    // Basicamente se vuelve a su punto inicial pero despues de 1 segundo de ir volviendo se gira mirando en direccion a donde esta el punto, de esta manera se deja claro que ya no te esta siguiendo
    private IEnumerator GirarVuelta()
    {
        yield return new WaitForSeconds(1);
        if (direccionVuelta)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    // SEGUIR
    private void EnterSeguir()
    {
        GetComponent<Animator>().Play("WALK");
        tiempoParaDeambular = 0;
    }
    private void ExitSeguir()
    {
        tiempoCargaAtaque = 0;
    }
    private void UpdateSeguir()
    {
        if (targetPlayer)
        {
            if (transform.position.x > targetPlayer.transform.position.x - 2 && transform.position.x < targetPlayer.transform.position.x + 2)
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            else if (transform.position.x < targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(4f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                direccionAtaque = true;
            }
            else if (transform.position.x > targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-4f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 0, 0);
                direccionAtaque = false;
            }

            tiempoCargaAtaque += Time.deltaTime;
            if (tiempoCargaAtaque > 2f)
            {
                Atacar();
                sePuedeMover = false;
            }
        }
    }

    // "habilidad/manera que tiene de hacer da�o" del enemigo (es un dash hacia tu direccion)
    private void Atacar()
    {
        gameObject.layer = 17; // le pongo el layer de estar atacando
        if (direccionAtaque)
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(10, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            GetComponent<Animator>().Play("ATACAR");
        }
        else
        {
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, this.gameObject.GetComponent<Rigidbody2D>().velocity.y);
            GetComponent<Animator>().Play("ATACAR");
        }

        corrutinaPararAtaque = StartCoroutine(PararAtaque());
    }

    private IEnumerator PararAtaque()
    {
        yield return new WaitForSeconds(1f);
        gameObject.layer = 16;
        StartCoroutine(DejarQuietoCorrutina());
    }

    // Despues de que ataque dejamos quieto al enemigo para que al jugador le de tiempo a reaccionar despues del ataque
    private IEnumerator DejarQuietoCorrutina()
    {
        sePuedeMover = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        GetComponent<Animator>().Play("IDLE");
        comprobarCansado = true;
        tiempoCargaAtaque = 0;
        yield return new WaitForSeconds(3f);
        comprobarCansado = false;
        sePuedeMover = true;
    }


    // DEAMBULAR
    private void EnterDeambular()
    {
        corrutinaDeambular = StartCoroutine(DeambularCorrutina());
    }
    private void ExitDeambular()
    {

    }
    private void UpdateDeambular()
    {
        switch (direccionDeambular)
        {
            case 0:
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
                GetComponent<Animator>().Play("IDLE");
                break;
            case 1:
                GetComponent<Animator>().Play("WALK");
                GetComponent<Rigidbody2D>().velocity = new Vector2(2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                break;
            case 2:
                GetComponent<Animator>().Play("WALK");
                GetComponent<Rigidbody2D>().velocity = new Vector2(-2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
        }
    }

    private IEnumerator DeambularCorrutina()
    {
        direccionDeambular = Random.Range(0, 3);
        yield return new WaitForSeconds(3f);
        tiempoParaDeambular = 0;
        ChangeState(Estado.QUIETO);
    }



    // RAYCAST para no caerte
    private void RayCast()
    {
        Debug.DrawLine(puntoRaycast.position, new Vector2(puntoRaycast.position.x, puntoRaycast.position.y - 2), Color.red);
        if (Physics2D.Raycast(puntoRaycast.position, Vector2.down, 2, Mascara))
            sePuedeMover = true;
        else
        {
            sePuedeMover = false;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            if (estadoActual == Estado.VOLVER)
                sePuedeMover = true;

            if (estadoActual == Estado.DEAMBULAR || estadoActual == Estado.QUIETO)
            {
                sePuedeMover = true;
                ChangeState(Estado.VOLVER);
            }
        }
    }

    public void setTarget(GameObject target)
    {
        targetPlayer = target;
    }


    // CANSADO
    private void ExitCansado()
    {

    }
    private void EnterCansado()
    {
        this.gameObject.GetComponent<Animator>().Play("CANSADO");
        StartCoroutine(CorrutinaCansado());
    }

    private void UpdateCansado()
    {

    }
    private void Cansado()
    {
        ChangeState(Estado.CANSADO);
    }

    private IEnumerator CorrutinaCansado()
    {
        yield return new WaitForSeconds(3f);
        ChangeState(estadoPlaceholder);
    }


    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o + 5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }
    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }
    private void BajarVidaGrito()
    {
        vidaEnemigo -= (int)vidaEnemigo * 0.2f;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerto();
    }

    private void Muerto()
    {
        ChangeState(Estado.CANSADO);
        muerto = true;
        StopAllCoroutines();
        this.gameObject.GetComponent<Animator>().Play("MUERTECALVO");
    }

    private void EliminarCalvo()
    {
        int random = Random.Range(0, 10);
        if (random < 3)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1.5f, 0);

        }
        Destroy(this.gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (this.gameObject.layer == 16)
            {
                gei.Raise(5); //da�o normal
            }
            else if (this.gameObject.layer == 17)
            {
                gei.Raise(20); // da�o de la "habilidad", es decir el enemigo no va ha hacer el mismo da�o cuando lo tocas que cuando se esfuerza en hacerte da�o (tiene sentido, si no lo ves asi haztelo mirar)
            }
        }
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();

        if (collision.gameObject.tag.Equals("Grito"))
        {
            Cansado();
            BajarVidaGrito();
        }
    }









    public void CambiarSeguir()
    {
        estadoPlaceholder = Estado.SEGUIR;
        if (estadoActual != Estado.CANSADO)
            ChangeState(Estado.SEGUIR);
    }

    public void CambiarVolver()
    {
        estadoPlaceholder = Estado.VOLVER;
        if (estadoActual != Estado.CANSADO)
            ChangeState(Estado.VOLVER);
    }


}