using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBolsoEnemigoCerdanyola_controller : MonoBehaviour
{

    [SerializeField] private GameEventInteger gei;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(20);
    }
}
