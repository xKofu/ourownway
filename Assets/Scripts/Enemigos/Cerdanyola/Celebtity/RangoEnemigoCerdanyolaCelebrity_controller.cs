using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigoCerdanyolaCelebrity_controller : MonoBehaviour
{

    [SerializeField] private Enemigo_Cerdanyola_Celebrity_controller enemigoPadre;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarSeguir();    //ChangeState(Enemigo_Cerdanyola_Celebrity_controller.Estado.SEGUIR);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.setTarget(collision.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarVolver();    //ChangeState(Enemigo_Cerdanyola_Celebrity_controller.Estado.VOLVER);
    }

}
