using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZonaAtaqueEnemigoCerdanyolaCelebrity_controller : MonoBehaviour
{
    [SerializeField] private Enemigo_Cerdanyola_Celebrity_controller enemigoPadre;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarAtacar();   //ChangeState(Enemigo_Cerdanyola_Celebrity_controller.Estado.ATACANDO);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            enemigoPadre.CambiarSeguir();   //ChangeState(Enemigo_Cerdanyola_Celebrity_controller.Estado.SEGUIR);
    }

}
