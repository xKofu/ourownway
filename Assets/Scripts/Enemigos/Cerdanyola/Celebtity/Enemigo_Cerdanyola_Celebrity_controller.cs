using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo_Cerdanyola_Celebrity_controller : MonoBehaviour
{

    [Header("GO del jugador (esta para comprobaciones)")]
    [SerializeField] private GameObject targetPlayer;
    [Header("Punto de vuelta")]
    [SerializeField] private Transform puntoInicio;
    [Header("RayCast para no caerse")]
    [SerializeField] private Transform puntoRaycast;
    [SerializeField] private LayerMask Mascara;

    public enum Estado { SEGUIR, VOLVER, QUIETO, DEAMBULAR, ATACANDO, CANSADO };

    [SerializeField] private Estado estadoActual;
    private Estado estadoReserva;

    private bool sePuedeMover = true;
    private bool comprobarCansado = false;
    private bool estoyAtacando = false;
    private bool direccionVuelta; // true = mirando a la derecha y girar a la izquierda // false = mirando a la izquierda y girar a la derecha
    private bool sePuedeMoverVolver = false;

    [SerializeField] private float tiempoParaDeambular = 0;
    [SerializeField] private int direccionDeambular;

    private Coroutine corrutinaDeambular;
    private Coroutine corrutinaGirarAlVolver;
    private Coroutine corrutinaEsperarVolver;

    [Header("VIDA")]
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private ScriptableEstadisticas se;
    [SerializeField] private float vidaEnemigo;

    [Header("Pelota extra")]
    [SerializeField] private GameObject pelotaExtra;

    private bool muerto = false;

    private Coroutine corrutinaAtaque;

    void Start()
    {
        muerto = false;
        vidaEnemigo = sv.VidaMaxima;
        estadoActual = Estado.QUIETO;
    }


    void Update()
    {
        if (!muerto)
        {
            if (sePuedeMover && !comprobarCansado)
                StateOnUpdate();
            RayCast();
        }
    }


    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                ExitQuieto();
                break;
            case Estado.VOLVER:
                ExitVolver();
                break;
            case Estado.SEGUIR:
                ExitSeguir();
                break;
            case Estado.DEAMBULAR:
                ExitDeambular();
                break;
            case Estado.ATACANDO:
                ExitAtacar();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }



    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.QUIETO:
                EnterQuieto();
                break;
            case Estado.VOLVER:
                EnterVolver();
                break;
            case Estado.SEGUIR:
                EnterSeguir();
                break;
            case Estado.DEAMBULAR:
                EnterDeambular();
                break;
            case Estado.ATACANDO:
                EnterAtacar();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }

 
    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.QUIETO:
                UpdateQuieto();
                break;
            case Estado.VOLVER:
                UpdateVolver();
                break;
            case Estado.SEGUIR:
                UpdateSeguir();
                break;
            case Estado.DEAMBULAR:
                UpdateDeambular();
                break;
            case Estado.ATACANDO:
                UpdateAtacar();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }




    // QUIETO
    private void EnterQuieto()
    {
        this.gameObject.GetComponent<Animator>().Play("IDLE");
    }
    private void ExitQuieto()
    {

    }
    private void UpdateQuieto()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        tiempoParaDeambular += Time.deltaTime;
        if (tiempoParaDeambular >5.1)
            ChangeState(Estado.DEAMBULAR);
    }

    // VOLVER
    private void EnterVolver()
    {
        this.gameObject.GetComponent<Animator>().Play("WALK");
        corrutinaGirarAlVolver = StartCoroutine(GirarVuelta());
        corrutinaEsperarVolver = StartCoroutine(EsperarVolver());
        sePuedeMoverVolver = false;
    }
    private void ExitVolver()
    {
        if (corrutinaGirarAlVolver != null)
        {
            StopCoroutine(corrutinaGirarAlVolver);
            corrutinaGirarAlVolver = null;
        }
        if(corrutinaEsperarVolver != null)
        {
            StopCoroutine(corrutinaEsperarVolver);
            corrutinaEsperarVolver = null;
        }
        sePuedeMoverVolver = false;
    }
    private void UpdateVolver()
    {
        if (sePuedeMoverVolver)
        {
            if (transform.position.x == puntoInicio.transform.position.x || (transform.position.x < puntoInicio.transform.position.x + .5 && transform.position.x > puntoInicio.transform.position.x - .5))
            {
                ChangeState(Estado.QUIETO);
            }
            else if (transform.position.x < puntoInicio.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(3f, GetComponent<Rigidbody2D>().velocity.y);
                direccionVuelta = true;
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (transform.position.x > puntoInicio.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-3f, GetComponent<Rigidbody2D>().velocity.y);
                direccionVuelta = false;
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }
    public void CambiarVolver()
    {
        estadoReserva = Estado.VOLVER;
        if (!comprobarCansado && !estoyAtacando)
            ChangeState(Estado.VOLVER);
    }
    private IEnumerator GirarVuelta()
    {
        yield return new WaitForSeconds(1);
        if (direccionVuelta)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        else
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    private IEnumerator EsperarVolver()
    {
        sePuedeMoverVolver = false;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        yield return new WaitForSeconds(2.5f);
        sePuedeMoverVolver = true;
    }



    // SEGUIR
    private void EnterSeguir()
    {

    }
    private void ExitSeguir()
    {

    }
    private void UpdateSeguir()
    {
        if (targetPlayer)
        {
            if (transform.position.x > targetPlayer.transform.position.x - 1 && transform.position.x < targetPlayer.transform.position.x + 1)
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            else if (transform.position.x < targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(4f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (transform.position.x > targetPlayer.transform.position.x)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-4f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }
    public void CambiarSeguir()
    {
        estadoReserva = Estado.SEGUIR;
        if (!comprobarCansado && !estoyAtacando)
            ChangeState(Estado.SEGUIR);
    }


    // DEAMBULAR
    private void EnterDeambular()
    {
        corrutinaDeambular = StartCoroutine(DeambularCorrutina());
    }
    private void ExitDeambular()
    {
        tiempoParaDeambular = 0;
        if (corrutinaDeambular != null)
        {
            StopCoroutine(corrutinaDeambular);
            corrutinaDeambular = null;
        }
    }
    private void UpdateDeambular()
    {
        switch (direccionDeambular)
        {
            case 0:
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
                break;
            case 1:
                GetComponent<Rigidbody2D>().velocity = new Vector2(2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case 2:
                GetComponent<Rigidbody2D>().velocity = new Vector2(-2f, GetComponent<Rigidbody2D>().velocity.y);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                break;
        }
    }

    private IEnumerator DeambularCorrutina()
    {
        direccionDeambular = Random.Range(0, 3);
        yield return new WaitForSeconds(3f);
        tiempoParaDeambular = 0;
        ChangeState(Estado.QUIETO);
    }


    // ATACAR
    private void EnterAtacar()
    {
        estadoReserva = Estado.SEGUIR;
        estoyAtacando = true;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0);
        corrutinaAtaque = StartCoroutine(AtacarCorrutina());
    }
    private void ExitAtacar()
    {
        this.gameObject.GetComponent<Animator>().Play("IDLE"); // o la que toke
    }
    private void UpdateAtacar()
    {

    }

    private IEnumerator AtacarCorrutina()
    {
        
        this.gameObject.GetComponent<Animator>().Play("ATTACK");
        yield return new WaitForSeconds(1.2f);
        this.gameObject.GetComponent<Animator>().Play("ATTACK");
        yield return new WaitForSeconds(1.2f);
        this.gameObject.GetComponent<Animator>().Play("ATTACK");
        yield return new WaitForSeconds(1.2f);
        estoyAtacando = false;
        ChangeState(Estado.CANSADO);
    }

    public void CambiarAtacar()
    {
        estadoReserva = Estado.ATACANDO;
        if (!comprobarCansado && !estoyAtacando)
            ChangeState(Estado.ATACANDO);
    }



    // CANSADO

    private void EnterCansado()
    {
        GetComponent<Animator>().Play("CANSADO");
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
        comprobarCansado = true;
        if(corrutinaAtaque != null)
        {
            StopCoroutine(corrutinaAtaque);
            corrutinaAtaque = null;
        }
        StartCoroutine(TiempoCansado());
    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }

    private IEnumerator TiempoCansado()
    {
        yield return new WaitForSeconds(3f);
        comprobarCansado = false;
        ChangeState(estadoReserva);
    }



    // POSICION PLAYER
    public void setTarget(GameObject target)
    {
        targetPlayer = target;
    }

    // RAYCAST
    private void RayCast()
    {
        Debug.DrawLine(puntoRaycast.position, new Vector2(puntoRaycast.position.x, puntoRaycast.position.y - 2), Color.red);
        if (Physics2D.Raycast(puntoRaycast.position, Vector2.down, 2, Mascara))
            sePuedeMover = true;
        else
        {
            sePuedeMover = false;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y);
            if (estadoActual == Estado.VOLVER)
                sePuedeMover = true;

            if (estadoActual == Estado.DEAMBULAR || estadoActual == Estado.QUIETO)
            {
                sePuedeMover = true;
                ChangeState(Estado.VOLVER);
            }
        }
    }

    private void BajarVidaPelota()
    {
        vidaEnemigo -= se.da�o + 5;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }
    private void BajarVidaGolpe()
    {
        vidaEnemigo -= se.da�o;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }
    private void BajarVidaGrito()
    {
        vidaEnemigo -= (int)vidaEnemigo * 0.2f;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vidaEnemigo <= 0)
            Muerte();
    }

    private void Muerte()
    {
        ChangeState(Estado.CANSADO);
        muerto = true;
        StopAllCoroutines();
        this.gameObject.GetComponent<Animator>().Play("MUERTECELEBRITY");
    }

    private void EliminarCelebrity()
    {
        int random = Random.Range(0, 10);
        if (random < 3)
        {
            GameObject go = Instantiate(pelotaExtra);
            go.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1.5f, 0);

        }
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 25)
            BajarVidaGolpe();
        if (collision.gameObject.tag.Equals("Grito"))
        {
            ChangeState(Estado.CANSADO);
            BajarVidaGrito();
        }

        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(5);
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(5);
        if (collision.gameObject.tag.Equals("Pelota"))
            BajarVidaPelota();
        */
    }


}
