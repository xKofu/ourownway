using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCerdantilaTrinidad_controller : MonoBehaviour
{
    [Header("CABEZAS")]
    [SerializeField] private GameObject Cabeza1;
    [SerializeField] private GameObject Cabeza2;
    [SerializeField] private GameObject Cabeza3;


    [Header("Pool de los diferentes proyectiles")]
    [SerializeField] private Pool poolMonedas;
    [SerializeField] private Pool poolTragaperras;


    [Header("Scriptable vida")]
    [SerializeField] private ScriptableVida sv;

    public enum Estado { PENSADO, ATAQUE1, ATAQUE2, ATAQUE3, ESPERANDO, CANSADO };
    [Header("Estado actual del enemigo")]
    [SerializeField] private Estado estadoActual;

    [Header("ATAQUE 1")]
    // ATAQUE 1
    [SerializeField] private Transform[] posicionesSalidaMoneda;
    [SerializeField] private Transform playerPosition;


    [Header("ATAQUE 2")]
    // ATAQUE 2
    [SerializeField] private Transform[] posicionesCaidaTragaperras;
    [SerializeField] private Transform[] posicionesSalidaTragaperra;
    [SerializeField] private int numeroDeDevueltas = 0;
    private int numero1;
    private int numero2;
    private int numero3;



    // ATAQUE 3
    private Coroutine corrutinaEsperaAtaque3;


    private int numeroAtaques = 0;


    [Header("Evento da�o")]
    [SerializeField] private GameEventInteger gei;

    [Header("Evento")]
    [SerializeField] private GameEvent bajarVidaBossCerdanyola;
    [SerializeField] private GameEvent trinidadMuerta;


    [Header("Debug animaciomes")]
    [SerializeField] private string idle;
    [SerializeField] private string animacionAtaque1;
    [SerializeField] private string animacionAtaque2;


    private void Start()
    {
        sv.VidaActual = sv.VidaMaxima;
        idle = "IDLE";
        animacionAtaque1 = "ATAQUE1";
        animacionAtaque2 = "ATAQUE2";

        ChangeState(Estado.PENSADO);
        
    }

    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                ExitPensando();
                break;
            case Estado.ATAQUE1:
                ExitAtaque1();
                break;
            case Estado.ATAQUE2:
                ExitAtaque2();
                break;
            case Estado.ATAQUE3:
                ExitAtaque3();
                break;
            case Estado.ESPERANDO:
                ExitEsperando();
                break;
            case Estado.CANSADO:
                ExitCansado();
                break;
        }
    }

    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.PENSADO:
                EnterPensando();
                break;
            case Estado.ATAQUE1:
                EnterAtaque1();
                break;
            case Estado.ATAQUE2:
                EnterAtaque2();
                break;
            case Estado.ATAQUE3:
                EnterAtaque3();
                break;
            case Estado.ESPERANDO:
                EnterEsperando();
                break;
            case Estado.CANSADO:
                EnterCansado();
                break;
        }
    }

    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.PENSADO:
                UpdatePensando();
                break;
            case Estado.ATAQUE1:
                UpdateAtaque1();
                break;
            case Estado.ATAQUE2:
                UpdateAtaque2();
                break;
            case Estado.ATAQUE3:
                UpdateAtaque3();
                break;
            case Estado.ESPERANDO:
                UpdateEsperando();
                break;
            case Estado.CANSADO:
                UpdateCansado();
                break;
        }
    }




    // ATAQUE 1

    private void EnterAtaque1()
    {
        this.gameObject.GetComponent<Animator>().Play(animacionAtaque1);
        StartCoroutine(EsperarAnimacionAtaque1());
    }
    private void ExitAtaque1()
    {
        this.gameObject.GetComponent<Animator>().Play(idle);
    }
    private void UpdateAtaque1()
    {

    }
    private void SacarMoneda()
    {
        switch (sv.VidaActual) {
            case 3:
                Ataque1_3Cabezas();
                break;
            case 2:
                Ataque1_2Cabezas();
                break;
            case 1:
                Ataque1_1Cabezas();
                break;
        }
    }

    private void Ataque1_3Cabezas()
    {
        GameObject moneda11 = poolMonedas.getElement();
        moneda11.GetComponent<CircleCollider2D>().enabled = true;
        moneda11.transform.position = posicionesSalidaMoneda[0].position;
        moneda11.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda11.GetComponent<Moneda_controller>().Abajo();

        GameObject moneda21 = poolMonedas.getElement();
        moneda21.GetComponent<CircleCollider2D>().enabled = true;
        moneda21.transform.position = posicionesSalidaMoneda[1].position;
        moneda21.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda21.GetComponent<Moneda_controller>().Abajo();

        GameObject moneda31 = poolMonedas.getElement();
        moneda31.GetComponent<CircleCollider2D>().enabled = true;
        moneda31.transform.position = posicionesSalidaMoneda[2].position;
        moneda31.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda31.GetComponent<Moneda_controller>().Recto();

        StartCoroutine(DevolverMoneda(moneda11));
        StartCoroutine(DevolverMoneda(moneda21));
        StartCoroutine(DevolverMoneda(moneda31));
        StartCoroutine(FinAtaque1());

    }
    private void Ataque1_2Cabezas()
    {
        GameObject moneda22 = poolMonedas.getElement();
        moneda22.GetComponent<CircleCollider2D>().enabled = true;
        moneda22.transform.position = posicionesSalidaMoneda[1].position;
        moneda22.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda22.GetComponent<Moneda_controller>().Abajo();

        GameObject moneda32 = poolMonedas.getElement();
        moneda32.GetComponent<CircleCollider2D>().enabled = true;
        moneda32.transform.position = posicionesSalidaMoneda[2].position;
        moneda32.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda32.GetComponent<Moneda_controller>().Arriba();

        StartCoroutine(DevolverMoneda(moneda22));
        StartCoroutine(DevolverMoneda(moneda32));
        StartCoroutine(FinAtaque1());

    }
    private void Ataque1_1Cabezas()
    {
        GameObject moneda33 = poolMonedas.getElement();
        moneda33.GetComponent<CircleCollider2D>().enabled = true;
        moneda33.transform.position = posicionesSalidaMoneda[2].position;
        moneda33.GetComponent<Moneda_controller>().SetPlayerPosition(playerPosition.position);
        moneda33.GetComponent<Moneda_controller>().Arriba();

        StartCoroutine(DevolverMoneda(moneda33));
        StartCoroutine(FinAtaque1());
    }

    private IEnumerator DevolverMoneda(GameObject go)
    {
        yield return new WaitForSeconds(5f);
        poolMonedas.ReturnElement(go);
    }

    private IEnumerator FinAtaque1()
    {
        yield return new WaitForSeconds(5f);
        ChangeState(Estado.PENSADO);
    }

    private IEnumerator EsperarAnimacionAtaque1()
    {
        yield return new WaitForSeconds(1f);
        SacarMoneda();
    }


    // ATAQUE 2
    private void EnterAtaque2()
    {
        this.gameObject.GetComponent<Animator>().Play(animacionAtaque2);
        StartCoroutine(SacarTragaperras());
        numeroDeDevueltas = 0;
    }
    private void ExitAtaque2()
    {
    }
    private void UpdateAtaque2()
    {

    }
    /*

        Cabeza 1: 	Pilla maquina 	--> 1f
		            Carga		--> 2f
		            Lanza		--> 2.5f (2.3 Animator)
		            Quitar		--> 2.31 Animator

        Cabeza 2:	Pilla maquina 	--> 1.5f (1.3 Animator)
		            Carga 		--> 2.5f (2.3 Animator)
		            Lanza 		--> 3f
		            Quitar		--> 3.1 Animator

        Cabeza 3:	Pilla maquina	--> 2f
		            Carga		--> 3f
		            Lanza		--> 3.5f (3.3 Animator)
		            Quitar		--> 3.51 Animator

    */

    private IEnumerator SacarTragaperras()
    {
        switch (sv.VidaActual) {
            case 3:
                yield return new WaitForSeconds(2.5f);
                numero1 = Random.Range(0, posicionesCaidaTragaperras.Length);
                GameObject tragaperras1 = poolTragaperras.getElement();
                tragaperras1.transform.position = posicionesSalidaTragaperra[0].position;
                tragaperras1.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras1.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras1.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero1]);

                yield return new WaitForSeconds(0.5f);

                numero2 = Random.Range(0, posicionesCaidaTragaperras.Length);
                bool flag = true;
                if (numero2 == numero1)
                {
                    while (flag)
                    {
                        numero2 = Random.Range(0, posicionesCaidaTragaperras.Length);
                        if (numero2 != numero1)
                        {
                            flag = false;
                        }
                    }
                }
                GameObject tragaperras2 = poolTragaperras.getElement();
                tragaperras2.transform.position = posicionesSalidaTragaperra[1].position;
                tragaperras2.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras2.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras2.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero2]);

                yield return new WaitForSeconds(0.5f);
                numero3 = Random.Range(0, posicionesCaidaTragaperras.Length);
                flag = true;
                if (numero3 == numero1 || numero3 == numero2)
                {
                    while (flag)
                    {
                        numero3 = Random.Range(0, posicionesCaidaTragaperras.Length);
                        if (numero3 != numero1 && numero3 != numero2)
                        {
                            flag = false;
                        }
                    }
                }
                GameObject tragaperras3 = poolTragaperras.getElement();
                tragaperras3.transform.position = posicionesSalidaTragaperra[2].position;
                tragaperras3.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras3.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras3.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero3]);
                this.gameObject.GetComponent<Animator>().Play(idle);
                break;
            case 2:
                yield return new WaitForSeconds(2.5f);
                // lanazamiento 1
                yield return new WaitForSeconds(0.5f);
                numero2 = Random.Range(0, posicionesCaidaTragaperras.Length);
                GameObject tragaperras22 = poolTragaperras.getElement();
                tragaperras22.transform.position = posicionesSalidaTragaperra[1].position;
                tragaperras22.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras22.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras22.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero2]);

                yield return new WaitForSeconds(0.5f);
                numero3 = Random.Range(0, posicionesCaidaTragaperras.Length);
                flag = true;
                if (numero3 == numero2)
                {
                    while (flag)
                    {
                        numero3 = Random.Range(0, posicionesCaidaTragaperras.Length);
                        if ( numero3 != numero2)
                        {
                            flag = false;
                        }
                    }
                }
                GameObject tragaperras32 = poolTragaperras.getElement();
                tragaperras32.transform.position = posicionesSalidaTragaperra[2].position;
                tragaperras32.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras32.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras32.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero3]);
                this.gameObject.GetComponent<Animator>().Play(idle);

                break;
            case 1:
                yield return new WaitForSeconds(2.5f);
                // lanazamiento 1
                yield return new WaitForSeconds(0.5f);
                numero3 = Random.Range(0, posicionesCaidaTragaperras.Length);
                GameObject tragaperras33 = poolTragaperras.getElement();
                tragaperras33.transform.position = posicionesSalidaTragaperra[2].position;
                tragaperras33.GetComponent<Tragaperras_controller>().SetPadre(this.gameObject);
                tragaperras33.GetComponent<Tragaperras_controller>().CambiarEstadoSubir();
                tragaperras33.GetComponent<Tragaperras_controller>().SetPosicionBajar(posicionesCaidaTragaperras[numero3]);
                this.gameObject.GetComponent<Animator>().Play(idle);
                break;
        }
    }

   


    // ATAQUE 3
    private void EnterAtaque3()
    {
        switch (sv.VidaActual)
        {
            case 3:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA1");
                break;
            case 2:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA2");
                break;
            case 1:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA3");
                break;
        }
        corrutinaEsperaAtaque3 = StartCoroutine(Ataque3Cansado());
    }
    private void ExitAtaque3()
    {
    }
    private void UpdateAtaque3()
    {
        
    }

    private IEnumerator Ataque3Cansado()
    {
        yield return new WaitForSeconds(3f); // lo que dura la animacion
        // Ahora me espero el 5s para que el player haga da�o
        yield return new WaitForSeconds(5f);
        switch (sv.VidaActual)
        {
            case 3:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA1VUELTA");
                break;
            case 2:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA2VUELTA");
                break;
            case 1:
                this.gameObject.GetComponent<Animator>().Play("ATAQUE3CABEZA3VUELTA");
                break;
        }
        yield return new WaitForSeconds(3f); // lo que dura la animacion de vuelta
        ChangeState(Estado.PENSADO);
    }

    public void HanMatadoCabeza()
    {
        if (corrutinaEsperaAtaque3 != null)
        {
            StopCoroutine(corrutinaEsperaAtaque3);
            corrutinaEsperaAtaque3 = null;
        }
        BajarVida();
    }


    // PENSADO
    private void EnterPensando()
    {
        this.gameObject.GetComponent<Animator>().Play(idle);
        StartCoroutine(PensadoAtaque());
    }
    private void ExitPensando()
    {

    }
    private void UpdatePensando()
    {

    }

    private IEnumerator PensadoAtaque()
    {
        yield return new WaitForSeconds(4f);
        numeroAtaques++;
        if (numeroAtaques > 3)
        {
            ChangeState(Estado.ESPERANDO);
            numeroAtaques = 0;
        }
        else if (numeroAtaques == 3)
        {
            ChangeState(Estado.ATAQUE3);
        }
        else
        {
            int numeroAtaque = Random.Range(0, 2);
            switch (numeroAtaque)
            {
                case 0:
                    ChangeState(Estado.ATAQUE1);
                    break;
                case 1:
                    ChangeState(Estado.ATAQUE2);
                    break;
            }
        }
    }

    // ESPERANDO
    private void EnterEsperando()
    {
        this.gameObject.GetComponent<Animator>().Play(idle);
        StartCoroutine(Esperando());
    }
    private void ExitEsperando()
    {

    }
    private void UpdateEsperando()
    {

    }

    private IEnumerator Esperando()
    {
        yield return new WaitForSeconds(5f);
        ChangeState(Estado.PENSADO);
    }




    // CANSADO 
    private void EnterCansado()
    {
        StartCoroutine(Cansado());
    }
    private void ExitCansado()
    {

    }
    private void UpdateCansado()
    {

    }

    private IEnumerator Cansado()
    {
        yield return new WaitForSeconds(2.05f);
        this.gameObject.GetComponent<Animator>().Play(idle);
        yield return new WaitForSeconds(3f);
        ChangeState(Estado.PENSADO);
    }




    // POOL'S

    public void DevolverPoolMonedas(GameObject go)
    {
        poolMonedas.ReturnElement(go);
    }

    public void DevolverPoolTragaperras(GameObject go)
    {
        poolTragaperras.ReturnElement(go);
        numeroDeDevueltas++;
        print("ME DEVUELVEN UNA TRAGAPERRAS " + numeroDeDevueltas);
        if (numeroDeDevueltas == sv.VidaActual)
            ChangeState(Estado.PENSADO);
    }


    // VIDA
    public void BajarVida()
    {
        sv.VidaActual--;
        bajarVidaBossCerdanyola.Raise();
        numeroAtaques = 0;
        switch (sv.VidaActual)
        {
            case 2:
                Cabeza1.SetActive(false);
                this.gameObject.GetComponent<Animator>().Play("CABEZAELIMINADA");
                idle = "IDLE_2";
                animacionAtaque1 = "ATAQUE1_2";
                animacionAtaque2 = "ATAQUE2_2";
                StopAllCoroutines();
                ChangeState(Estado.CANSADO);
                break;
            case 1:
                Cabeza1.SetActive(false);
                Cabeza2.SetActive(false);
                this.gameObject.GetComponent<Animator>().Play("CABEZAELIMINADA2");
                idle = "IDLE_3";
                animacionAtaque1 = "ATAQUE1_1";
                animacionAtaque2 = "ATAQUE2_1";
                StopAllCoroutines();
                ChangeState(Estado.CANSADO);
                break;
            case 0:
                Cabeza1.SetActive(false);
                Cabeza2.SetActive(false);
                // MORIRSE
                this.gameObject.GetComponent<Animator>().Play("MUERTE");
                StopAllCoroutines();
                trinidadMuerta.Raise();
                break;
        }
    }

    public void TrinidadMuerta()
    {
        this.gameObject.GetComponent<BossCerdantilaTrinidad_controller>().enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(10);
    }



}