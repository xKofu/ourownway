using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tragaperras_controller : MonoBehaviour
{

    public enum Estado { SUBIR, BAJAR, SUELO};
    [SerializeField] private Estado estadoActual;

    [Header("BoxColider que hace de pies")]
    [SerializeField] private GameObject pies;

    private BossCerdantilaTrinidad_controller enemigoPadre;

    private Transform posicionBajar;

    [Header("Evento da�o")]
    [SerializeField] private GameEventInteger gei;

    void Start()
    {
        //ChangeState(Estado.SUBIR);
    }
    private void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StateOnUpdate();
    }


    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }


    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.SUBIR:
                ExitSubir();
                break;
            case Estado.BAJAR:
                ExitBajar();
                break;
            case Estado.SUELO:
                ExitSuelo();
                break;
        }
    }



    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.SUBIR:
                EnterSubir();
                break;
            case Estado.BAJAR:
                EnterBajar();
                break;
            case Estado.SUELO:
                EnterSuelo();
                break;
        }
    }


    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.SUBIR:
                UpdateSubir();
                break;
            case Estado.BAJAR:
                UpdateBajar();
                break;
            case Estado.SUELO:
                UpdateSuelo();
                break;
        }
    }



    // SUBIR

    private void EnterSubir()
    {
        pies.SetActive(false);
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        StartCoroutine(AcabarSubir());
    }
    private void ExitSubir()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }
    private void UpdateSubir()
    {
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 10);
        
    }

    private IEnumerator AcabarSubir()
    {
        yield return new WaitForSeconds(3f);
        ChangeState(Estado.BAJAR);
    }

    public void CambiarEstadoSubir()
    {
        ChangeState(Estado.SUBIR);
    }


    // BAJAR
    private void EnterBajar()
    {
        this.gameObject.transform.position = posicionBajar.position;
        pies.SetActive(true);
        this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 2.5f;
    }
    private void ExitBajar()
    {

    }
    private void UpdateBajar()
    {

    }



    // SUELO
    private void EnterSuelo()
    {
        StartCoroutine(DevolverTragaperra());
    }
    private void ExitSuelo()
    {

    }

    private void UpdateSuelo()
    {

    }

    public void CambiarEstadoHaSuelo()
    {
        if (estadoActual != Estado.SUELO)
            ChangeState(Estado.SUELO);
    }

    private IEnumerator DevolverTragaperra()
    {
        yield return new WaitForSeconds(3f);
        pies.SetActive(false);
        yield return new WaitForSeconds(3f);
        enemigoPadre.DevolverPoolTragaperras(this.gameObject);
    }






    // FUNCINES GENERALES
    public void SetPadre(GameObject boss)
    {
        enemigoPadre = boss.GetComponent<BossCerdantilaTrinidad_controller>();
    }

    public void SetPosicionBajar(Transform pos)
    {
        posicionBajar = pos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("ground"))
            ChangeState(Estado.SUELO);
        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(30);
    }

}
