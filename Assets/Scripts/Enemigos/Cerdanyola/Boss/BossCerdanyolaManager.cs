using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BossCerdanyolaManager : MonoBehaviour
{

    [Header("Eventos")]
    [SerializeField] private GameEvent entrarSalaBossTrinidad;
    [SerializeField] private GameEvent iniciarBossTrinidad;

    [Header("Muros Invisibles")]
    [SerializeField] private GameObject inv1;
    [SerializeField] private GameObject inv2;

    [Header("Particulas")]
    [SerializeField] private GameObject particulas1;
    [SerializeField] private GameObject particulas2;

    [Header("Informacion transcurso partida")]
    [SerializeField] private InformacionTranscursoPartida itp;

    void Start()
    {
        StartCoroutine(PonerHabilidades());
        inv1.SetActive(false);
        inv2.SetActive(false);
        particulas1.SetActive(false);
        particulas2.SetActive(false);
    }

    void Update()
    {
        
    }

    public void PonerMueroInvisible()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        iniciarBossTrinidad.Raise();
        inv1.SetActive(true);
        inv2.SetActive(true);
    }

    public void QuitarMurosInvisible()
    {
        inv1.SetActive(false);
        inv2.SetActive(false);
    }


    public void TrinidadMuerto()
    {
        inv2.SetActive(false);
        itp.bossMGADone = true;
        particulas1.SetActive(true);
        particulas2.SetActive(true);
        StartCoroutine(QuitarParticulas());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            PonerMueroInvisible();
    }


    private IEnumerator PonerHabilidades()
    {
        yield return new WaitForSeconds(0.5f);
        entrarSalaBossTrinidad.Raise();
    }

    private IEnumerator QuitarParticulas()
    {
        yield return new WaitForSeconds(1f);
        particulas1.GetComponent<VisualEffect>().Stop();
        particulas2.GetComponent<VisualEffect>().Stop();
    }
}
