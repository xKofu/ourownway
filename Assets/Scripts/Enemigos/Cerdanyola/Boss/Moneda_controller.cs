using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda_controller : MonoBehaviour
{
    public enum Estado { RECTO, ARRIBA, ABAJO };
    [SerializeField] private Estado estadoActual;


    private Vector2 direccionPlayer;

    [Header("Evento da�o")]
    [SerializeField] private GameEventInteger gei;

    void Update()
    {
        StateOnUpdate();
    }
    public void ChangeState(Estado NextState)
    {
        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();
    }

    private void StateOnExit()
    {
        switch (estadoActual)
        {
            case Estado.RECTO:
                ExitRecto();
                break;
            case Estado.ARRIBA:
                ExitArriba();
                break;
            case Estado.ABAJO:
                ExitAbajo();
                break;
        }
    }



    private void StateOnEnter(Estado nextState)
    {
        estadoActual = nextState;

        switch (estadoActual)
        {
            case Estado.RECTO:
                EnterRecto();
                break;
            case Estado.ARRIBA:
                EnterArriba();
                break;
            case Estado.ABAJO:
                EnterAbajo();
                break;
        }
    }



    private void StateOnUpdate()
    {
        switch (estadoActual)
        {
            case Estado.RECTO:
                UpdateRecto();
                break;
            case Estado.ARRIBA:
                UpdateArriba();
                break;
            case Estado.ABAJO:
                UpdateAbajo();
                break;
        }
    }

    private void DarVelocidad()
    {
        this.GetComponent<Rigidbody2D>().velocity = direccionPlayer.normalized * Random.Range(8f, 10f);
    }


    // RECTO
    private void EnterRecto()
    {
        DarVelocidad();
    }
    private void ExitRecto()
    {
    }
    private void UpdateRecto()
    {
    }

    public void Recto()
    {
        ChangeState(Estado.RECTO);
    }


    // ARRIBA
    private void EnterArriba()
    {
        direccionPlayer -= Vector2.up * Random.Range(1f, 3f);
        DarVelocidad();
    }
    private void ExitArriba()
    {
    }
    private void UpdateArriba()
    {
    }

    public void Arriba()
    {
        ChangeState(Estado.ARRIBA);
    }



    // ABAJO
    private void EnterAbajo()
    {
        //Vector2 direccion = (Vector2)transform.position - posicionPlayer;
        direccionPlayer -= Vector2.up* Random.Range(-1f, -3f);
        DarVelocidad();
    }
    private void ExitAbajo()
    {
    }
    private void UpdateAbajo()
    {
    }

    public void Abajo()
    {
        ChangeState(Estado.ABAJO);
    }



    public void SetPlayerPosition(Vector2 pos)
    {
        direccionPlayer = pos-(Vector2)transform.position;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            gei.Raise(25);
            this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
        }
    }

}
