using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabezaTrinidad_controller : MonoBehaviour
{
    [SerializeField] private BossCerdantilaTrinidad_controller enemigoPadre;
    [SerializeField] private ScriptableVida sv;
    [SerializeField] private GameEventInteger gei;
    [SerializeField] private float vida;


    private void Start()
    {
        vida = sv.VidaMaxima;
    }

    private void BajarVida()
    {
        vida--;
        this.gameObject.GetComponent<DanyoEnemigo_controller>().Hurt();
        if (vida <= 0)
            enemigoPadre.HanMatadoCabeza();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("GolpePlayer") || collision.gameObject.tag.Equals("Pelota"))
            BajarVida();

        if (collision.gameObject.tag.Equals("Player"))
            gei.Raise(20);
    }


}
