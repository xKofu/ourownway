using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Susoension_controller : MonoBehaviour
{
    [SerializeField]
    private bool isGrounded = true;
    [SerializeField]
    private bool suspension = false;
    [SerializeField]
    private bool puedoUtilizarLaHabilidad = true;


    private float m_velocidad = 5;


    void Update()
    {

        if (!suspension)
        {
            if (Input.GetKey(KeyCode.D))
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else if (Input.GetKey(KeyCode.A))
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);

            if (Input.GetKey(KeyCode.W) && isGrounded)
            {
                isGrounded = false;
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
            }

            if (Input.GetKey(KeyCode.Alpha2) && !isGrounded && puedoUtilizarLaHabilidad)
            {
                suspension = true;
                puedoUtilizarLaHabilidad = false;
                StartCoroutine(TiempoSuspension());
            }
        }
        else 
        {
            GetComponent<Rigidbody2D>().gravityScale = 0;
            if (Input.GetKey(KeyCode.D))
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else if (Input.GetKey(KeyCode.A))
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else if (Input.GetKey(KeyCode.W))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, m_velocidad);
            else if (Input.GetKey(KeyCode.S))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -m_velocidad);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = true;
    }




    private IEnumerator TiempoSuspension()
    {
        yield return new WaitForSeconds(2f);
        suspension = false;
        GetComponent<Rigidbody2D>().gravityScale = 1;
        StartCoroutine(CooldownSuspension());
    }

    private IEnumerator CooldownSuspension()
    {
        yield return new WaitForSeconds(5f);
        puedoUtilizarLaHabilidad = true;
    }

}

