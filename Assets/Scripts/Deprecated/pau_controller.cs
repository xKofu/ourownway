using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pau_controller : MonoBehaviour
{
    private bool dash, suspension, inmunidad, combate = false;

    [Header("Game Events")]
    [SerializeField] private GameEvent habilidad1_Dash;
    [SerializeField] private GameEvent habilidad2_DobleSalto;
    [SerializeField] private GameEvent habilidad3_Inmunodad;
    [SerializeField] private GameEvent habilidad4_Combate;


    [Header("Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown sc_habilidad1_Dash;
    [SerializeField] private ScriptableCooldown sc_habilidad2_Suspension;
    [SerializeField] private ScriptableCooldown sc_habilidad3_Inmunidad;
    [SerializeField] private ScriptableCooldown sc_habilidad4_Combate;



    [Header("Dash")]
    [SerializeField]
    private float d_velocidad = 10;
    private float d_tiempo = 0;

    



    private void Awake()
    {
        dash = false;
        suspension = false;
        inmunidad = false;
        combate = false;
    }
    private void Start()
    {
        
    }
    private void Update()
    {
        if (!dash)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                dash = true;
                print("PAU --> pulso 1");
                habilidad1_Dash.Raise();
                StartCoroutine(Habilidad1());
                /*

                    d_tiempo += Time.deltaTime;


                    if (d_tiempo > 0.25)
                    {
                        dash = true;
                        d_tiempo = 0;
                    }

                    if (dash)
                        StartCoroutine(Control_Dash());


                    if (this.gameObject.GetComponent<MoveController>().Derecha()) {

                        GetComponent<Rigidbody2D>().velocity = new Vector2(15, 0);

                        //this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * 5000, 0); // FUNCIONA MEH
                    }
                    else {

                        GetComponent<Rigidbody2D>().velocity = new Vector2(-15, 0);

                        //this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * -5000, 0); // FUNCIONA MEH
                    }
                    
                
                */
                //StartCoroutine(DashCorrutina(this.gameObject.GetComponent<MoveController>().Derecha()));
            }
        }


        if (!suspension)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                suspension = true;
                print("PAU --> pulso 2");
                habilidad2_DobleSalto.Raise();
                StartCoroutine(Habilidad2());
            }
        }

        if (!inmunidad)
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                inmunidad = true;
                print("PAU --> pulso 3");
                habilidad3_Inmunodad.Raise();
                StartCoroutine(Habilidad3());
            }

        }

        if (!combate)
        {
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                combate = true;
                print("PAU --> pulso 4");
                habilidad4_Combate.Raise();
                StartCoroutine(Habilidad4());
            }
        }
    }


    private IEnumerator Habilidad1()
    {
        sc_habilidad1_Dash.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad1_Dash.tiempoActual >= sc_habilidad1_Dash.tiempoTotal)
        {
            sc_habilidad1_Dash.tiempoTotal = 0;
            dash = false;
        }
    }
    private IEnumerator Habilidad2()
    {
        sc_habilidad2_Suspension.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad2_Suspension.tiempoActual >= sc_habilidad2_Suspension.tiempoTotal)
        {
            sc_habilidad2_Suspension.tiempoTotal = 0;
            suspension = false;
        }
    }

    private IEnumerator Habilidad3()
    {
        sc_habilidad3_Inmunidad.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad3_Inmunidad.tiempoActual >= sc_habilidad3_Inmunidad.tiempoTotal)
        {
            sc_habilidad3_Inmunidad.tiempoTotal = 0;
            inmunidad = false;
        }
    }

    private IEnumerator Habilidad4()
    {
        sc_habilidad4_Combate.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad4_Combate.tiempoActual >= sc_habilidad4_Combate.tiempoTotal)
        {
            sc_habilidad4_Combate.tiempoTotal = 0;
            combate = false;
        }
    }









    /*
    private void Dash()
    {

    }
    private IEnumerator Control_Dash()
    {
        //yield return new WaitForSeconds(2f);
        //dash = false;
        //print("YA PUEDES UTILIZAR DASH");
    }


    private IEnumerator DashCorrutina(bool direccion)
    {
        /*
        dash = true;
        
        if(direccion)
            GetComponent<Rigidbody2D>().velocity = new Vector2(15, 0);

        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(-15, 0);

        yield return new WaitForSeconds(0.5f);



        StartCoroutine(Control_Dash());
        
    }



    private void Suspension()
    {

    }
    private void Control_Suspension()
    {
        //if (this.gameObject.GetComponent<MoveController>().GetIsGrounded())
            //suspension = false;
    }

    private void Inmunidad()
    {

    }
    private IEnumerator Control_Inmunidad()
    {
        
        inmunidad = true;
        yield return new WaitForSeconds(10f);
        inmunidad = false;
        
    }

    private void Combate()
    {

    }
    private IEnumerator Control_Combate()
    {
        
        combate = true;
        yield return new WaitForSeconds(8f);
        combate = false;
        
    }



    public bool GetDash() { return dash; }
*/
}
