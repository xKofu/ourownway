using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planear_controller : MonoBehaviour
{

    [Header("true = estoy tocando el suelo")]
    [SerializeField]
    private bool isGrounded = false;

    [Header("true = estoy planeando")]
    [SerializeField]
    private bool planear = false;

    [Header("true = puedo utilizar la habilidad")]
    [SerializeField]
    private bool puedoUtilizarLaHabilidad = true;

    [Header("true = puedo pulsar para desactivar planear")]
    [SerializeField]
    private bool controladorPulsador = false;

    [Header("true = He pulsado para utilizar la habilidad")]
    [SerializeField]
    private bool hePulsado = false;




    private bool InCorrutine = false;
   

    private float m_velocidad = 5;



    private void Start()
    {
        isGrounded = false;
        planear = false;
        puedoUtilizarLaHabilidad = true;
        controladorPulsador = false;
        InCorrutine = false;
    }
    void Update()
    {
        if(!planear)
        {
            if (Input.GetKey(KeyCode.D))
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else if (Input.GetKey(KeyCode.A))
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            if (Input.GetKey(KeyCode.W) && isGrounded)
            {
                isGrounded = false;
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) && !isGrounded && puedoUtilizarLaHabilidad)
            {
                StartCoroutine(ControladorVolverPulsar());
                planear = true;
                this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0.2f;
                print("a planear " + planear +  " " + puedoUtilizarLaHabilidad + " " + GetComponent<Rigidbody2D>().gravityScale);
                hePulsado = true;
            }

        }
        else
        {
            if (Input.GetKey(KeyCode.D))
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            else if (Input.GetKey(KeyCode.A))
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && planear)
        {
            if (controladorPulsador)
            {
                print("Vuelvo a pulsar");
                GetComponent<Rigidbody2D>().gravityScale = 3f;
                planear = false;
                StartCoroutine(CooldownPlanear());
            }
        }
           
        
     
    }





    private IEnumerator CooldownPlanear()
    {
        puedoUtilizarLaHabilidad = false;
        InCorrutine = true;
        yield return new WaitForSeconds(10f);
        puedoUtilizarLaHabilidad = true;
        hePulsado = false;
        InCorrutine = false;
    }

    private IEnumerator ControladorVolverPulsar()
    {
        print("Entro a la corrutina de ControladorVolverPulsar");
        controladorPulsador = false;
        yield return new WaitForSeconds(1.5f);
        controladorPulsador = true;
        print("Salgo a la corrutina de ControladorVolverPulsar");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            controladorPulsador = false;
            isGrounded = true;
            planear = false;
            GetComponent<Rigidbody2D>().gravityScale = 1f;
            if (!InCorrutine && hePulsado)
                StartCoroutine(CooldownPlanear());
        }
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = false;
    }

}

