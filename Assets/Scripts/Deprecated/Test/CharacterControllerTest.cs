using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerTest : MonoBehaviour
{
    [Header("Numeritos personaje")]
    [SerializeField]
    private float m_velocidad = 5f;
    [SerializeField]
    private float m_FuerzaSalto = 100f;
    [SerializeField]
    private bool imGrounded;
    [SerializeField]
    private bool CanMove;

    private float jumpMultiplier = 0.5f;

    private enum State { MOVIMIENTO, IDLE, AIRE, DASH, PLANEO, GANCHO_1, GANCHO_2, CURAR, CAMBIO };
    private State m_state;

    public string Estado;
    private enum Direccion { DERECHA, IZQUIERDA };
    private Direccion view_dir;

    private enum Identidad { PAU, GERARD };
    private Identidad m_Identity;

    [Header("Evento cambio de Personaje")]
    [SerializeField]
    private GameEvent changeToGerard, changeToPau;

    private bool CanChange = true;



    bool[] activeAbilities = new bool[8];



    public List<string> Collisions;

    [Header("Skins")]
    [SerializeField] private GameObject SkinPau;
    [SerializeField] private GameObject SkinGerard;

    [Header("Gerard Game Events")]
    [SerializeField] private GameEvent gerard_habilidad1_Planear;
    [SerializeField] private GameEvent gerard_habilidad2_Grito;
    [SerializeField] private GameEvent gerard_habilidad3_Gancho;
    [SerializeField] private GameEvent gerard_habilidad4_CrearPlataforma;

    [Header("Gerard  Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad1_Planear;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad2_Grito;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad3_Gancho;
    [SerializeField] private ScriptableCooldown gerard_sc_habilidad4_CrearPlataforma;


    [Header("Pau Game Events")]
    [SerializeField] private GameEvent pau_habilidad1_Dash;
    [SerializeField] private GameEvent pau_habilidad2_DoubleJump;
    [SerializeField] private GameEvent pau_habilidad3_Inmunidad;
    [SerializeField] private GameEvent pau_habilidad4_Combate;

    [Header("Pau Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown pau_sc_habilidad1_Dash;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad2_DoubleJump;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad3_Inmunidad;
    [SerializeField] private ScriptableCooldown pau_sc_habilidad4_Combate;

    [Header("Cambio de personaje Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown sc_cambio;

    void Start()
    {
        m_Identity = Identidad.PAU;
        SkinPau.SetActive(true);
        SkinGerard.SetActive(false);
        view_dir = Direccion.DERECHA;
        m_state = State.IDLE;
        imGrounded = false;
        CanMove = true;
        m_velocidad = 5f;
        m_FuerzaSalto = 1500f;

        for(int i = 0; i < activeAbilities.Length; i++)
            activeAbilities[i] = true;
    }

    void Update()
    {

        Estado = m_state.ToString();

        ActionHandler();

        if (CanMove)
            MovementHandler();

        StateOnUpdate();
    }

    void FixedUpdate()
    {
        CheckCollisions();
    }

    void CheckCollisions()
    {
        if (Collisions.Contains("ground"))
            imGrounded = true;
        else
            imGrounded = false;
    }

    private void ActionHandler()
    {

        switch(m_state) {
            case State.AIRE:

                if (CanDash())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.DASH);
                        return;
                    }

                if (CanDJump())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        DoubleJump();
                        return;
                    }

                if (CanTurnInvul())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        TurnInvulnerable();
                        return;
                    }

                if (CanBuff())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        GetBuffed();
                        return;
                    }

                if (CanPlane())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.PLANEO);
                        return;
                    }

                if (CanScream())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        Scream();
                        return;
                    }

                if (CanBuild())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        Build();
                        return;
                    }

                if (Input.GetKeyDown(KeyCode.C))
                    ChangeChar();

                break;
            case State.MOVIMIENTO:
            case State.IDLE:
                                
                if (CanDash())
                    if (Input.GetKeyDown(KeyCode.Alpha1))
                    {
                        ChangeState(State.DASH);
                        return;
                    }

                if (CanTurnInvul())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        TurnInvulnerable();
                        return;
                    }

                if (CanBuff())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        GetBuffed();
                        return;
                    }

                if (CanScream())
                    if (Input.GetKeyDown(KeyCode.Alpha2))
                    {
                        Scream();
                        return;
                    }

                if (CanHook())
                    if (Input.GetKeyDown(KeyCode.Alpha3))
                    {
                        ChangeState(State.GANCHO_1);
                        return;
                    }

                if (CanBuild())
                    if (Input.GetKeyDown(KeyCode.Alpha4))
                    {
                        Build();
                        return;
                    }

                if (Input.GetKeyDown(KeyCode.F))
                {
                    Heal();
                    return;
                }

                if (Input.GetKeyDown(KeyCode.C))
                {
                    ChangeChar();
                    return;
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Shoot();
                    return;
                }


                    break;
            case State.PLANEO:
                if (Input.GetKeyDown(KeyCode.Alpha1))
                    ChangeState(State.IDLE);
                break;
            case State.GANCHO_1:
                if (Input.GetKeyDown(KeyCode.Alpha3))
                    ChangeState(State.IDLE);
                break;
            case State.CURAR:
            case State.CAMBIO:
            case State.GANCHO_2:
            case State.DASH:
                //print("Estas en un estado en el que no puedes llevar a cabo acciones");
                break;
        }

    }

    private void MovementHandler()
    {

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            if (view_dir == Direccion.IZQUIERDA)
            {
                Flip();
                view_dir = Direccion.DERECHA;
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
            if (view_dir == Direccion.DERECHA)
            {
                Flip();
                view_dir = Direccion.IZQUIERDA;
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }

    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    private void ChangeState(State NextState)
    {

        StateOnExit();

        StateOnEnter(NextState);

        StateOnUpdate();

    }

    private void StateOnUpdate()
    {
        switch (m_state)
        {
            case State.AIRE:
                StateAIRE();                                                                                                   
                break;
            case State.MOVIMIENTO:
                StateMOVIMIENTO();
                break;
            case State.IDLE:
                StateIDLE();
                break;
            case State.DASH:
                StateDASH();
                break;
            case State.PLANEO:
                StatePLANEO();
                break;
            case State.CURAR:
                StateCURAR();
                break;
            case State.CAMBIO:
                StateCAMBIO();
                break;
            case State.GANCHO_1:
                StateGANCHO1();
                break;
            case State.GANCHO_2:
                StateGANCHO2();
                break;
        }
    }
    
    private void StateOnExit()
    {
        switch (m_state)
        {
            case State.AIRE:
                
                break;
            case State.MOVIMIENTO:

                break;
            case State.IDLE:

                break;
            case State.DASH:

                break;
            case State.PLANEO:

                break;
            case State.CURAR:

                break;
            case State.CAMBIO:

                break;
            case State.GANCHO_1:

                break;
            case State.GANCHO_2:

                break;
        }
    }

    private void StateOnEnter(State nextState)
    {
        m_state = nextState;

        switch (m_state)
        {
            case State.AIRE:

                break;
            case State.MOVIMIENTO:

                break;
            case State.IDLE:

                break;
            case State.DASH:

                break;
            case State.PLANEO:

                break;
            case State.CURAR:

                break;
            case State.CAMBIO:

                break;
            case State.GANCHO_1:

                break;
            case State.GANCHO_2:

                break;
        }
    }

    private bool CanDash()
    {
        if (!activeAbilities[0])
            return false;
        if (m_Identity == Identidad.GERARD)
            return false;
        
        return true;
    }

    private bool CanDJump()
    {
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[1])
            return false;

        return true;
    }

    private bool CanTurnInvul()
    {
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[2])
            return false;

        return true;
    }

    private bool CanBuff()
    {
        if (m_Identity == Identidad.GERARD)
            return false;
        if (!activeAbilities[3])
            return false;

        return true;
    }

    private bool CanPlane()
    {
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[4])
            return false;

        return true;
    }

    private bool CanScream()
    {
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[5])
            return false;

        return true;
    }

    private bool CanHook()
    {
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[6])
            return false;

        return true;
    }

    private bool CanBuild()
    {
        if (m_Identity == Identidad.PAU)
            return false;
        if (!activeAbilities[7])
            return false;

        return true;
    }


    private void DoubleJump()
    {
        print("DJUMP");
        pau_habilidad2_DoubleJump.Raise();
        StartCoroutine(CooldownCorrutine(1, pau_sc_habilidad2_DoubleJump));
    }


    private void TurnInvulnerable()
    {
        print("INV");
        pau_habilidad3_Inmunidad.Raise();
        StartCoroutine(CooldownCorrutine(2, pau_sc_habilidad3_Inmunidad));
    }


    private void GetBuffed()
    {
        print("BUFF");
        pau_habilidad4_Combate.Raise();
        StartCoroutine(CooldownCorrutine(3, pau_sc_habilidad4_Combate));
    }


    private void Scream()
    {
        print("SCREAM");
        gerard_habilidad2_Grito.Raise();
        StartCoroutine(CooldownCorrutine(5, gerard_sc_habilidad2_Grito));
    }

    private void Build()
    {
        print("BUILD");
        gerard_habilidad4_CrearPlataforma.Raise();
        StartCoroutine(CooldownCorrutine(7, gerard_sc_habilidad4_CrearPlataforma));
    }

    private void Heal()
    {
        print("HEAL");
    }

    private void ChangeChar()
    {
        if (CanChange)
            ControlCambioPersonaje();
    }

    private void ControlCambioPersonaje()
    {
        if (m_Identity == Identidad.PAU)
        {
            m_Identity = Identidad.GERARD;
            print("m_Ident: " + m_Identity);
            SkinPau.SetActive(false);
            print("SkinPau: " + SkinPau.active);
            SkinGerard.SetActive(true);
            print("SkinGerard: " + SkinGerard.active);
            changeToGerard.Raise();
        }
        else
        {
            m_Identity = Identidad.PAU;
            print("m_Ident: " + m_Identity);
            SkinPau.SetActive(true);
            print("SkinPau: " + SkinPau.active);
            SkinGerard.SetActive(false);
            print("SkinGerard: " + SkinGerard.active);
            changeToPau.Raise();
        }

        transform.position = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
        StartCoroutine(CooldownCorrutineChangeCharacter());   
    }

    IEnumerator CooldownCorrutineChangeCharacter()
    {
        CanChange = false;
        while (!CanChange)
        {
            sc_cambio.tiempoActual += 0.1f;
            yield return new WaitForSeconds(0.1f);
            if (sc_cambio.tiempoActual >= sc_cambio.tiempoTotal) { 
                sc_cambio.tiempoActual = 0;
                CanChange = true;
            }
        }
    }

    private void Shoot()
    {

    }

    private void Jump()
    {
        
        if (Input.GetKey(KeyCode.W))
        {
            if (jumpMultiplier < 1)
            {
                jumpMultiplier += Time.deltaTime;
            }
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, (m_FuerzaSalto * jumpMultiplier)) * Time.deltaTime, ForceMode2D.Impulse);
            jumpMultiplier = 0.5f;
        }

    }

    private void StateAIRE()
    {
        if (imGrounded)
            ChangeState(State.IDLE);
    }

    private void StateMOVIMIENTO()
    {
        if (!imGrounded)
            ChangeState(State.AIRE);

        if (GetComponent<Rigidbody2D>().velocity.x == 0 && GetComponent<Rigidbody2D>().velocity.y == 0)
            ChangeState(State.IDLE);

        Jump();

    }

    private void StateIDLE()
    {
        if (!imGrounded)
            ChangeState(State.AIRE);

        if (GetComponent<Rigidbody2D>().velocity.x != 0)
            ChangeState(State.MOVIMIENTO);

        Jump();
    }

    private void StateDASH()
    {
        pau_habilidad1_Dash.Raise();
        StartCoroutine(CooldownCorrutine(0, pau_sc_habilidad1_Dash));
        StartCoroutine(Dash());
    }




    IEnumerator Dash()
    {
        CanMove = false;
        if (view_dir == Direccion.DERECHA)
            GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);


        yield return new WaitForSeconds(0.2f);
        CanMove = true;
        ChangeState(State.IDLE);

    }
    
    private void StatePLANEO()
    {
        print("PLANEAR");
        ChangeState(State.IDLE);
    }

    private void StateCURAR()
    {
        print("CURAR");
        ChangeState(State.IDLE);
    }

    private void StateCAMBIO()
    {
        print("CAMBIO");
        ChangeState(State.IDLE);
    }

    private void StateGANCHO1()
    {
        print("GANCHO");
        ChangeState(State.IDLE);
    }

    private void StateGANCHO2()
    {
        ChangeState(State.IDLE);
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.GetContact(0).point.y <= transform.position.y)
            Collisions.Add(collision.collider.tag);

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Collisions.Remove(collision.collider.tag);
    }


    IEnumerator CooldownCorrutine(int position, ScriptableCooldown sc)
    {
        sc.tiempoActual = 0;
        activeAbilities[position] = false;
        while (sc.tiempoActual < sc.tiempoTotal)
        {
            yield return new WaitForSeconds(0.1f);
            sc.tiempoActual += 0.1f;
            if (sc.tiempoActual >= sc.tiempoTotal)
                sc.tiempoActual = sc.tiempoTotal;
        }
        activeAbilities[position] = true;
    }

}
