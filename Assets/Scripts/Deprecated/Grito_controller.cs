using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grito_controller : MonoBehaviour
{

    [Header("true = estoy tocando el suelo")]
    [SerializeField]
    private bool isGrounded = false;

    [Header("true = puedo utilizar la habilidad")]
    [SerializeField]
    private bool puedoUtilizarLaHabilidad = true;

    [Header("true = puedo utilizar la habilidad")]
    [SerializeField]
    private GameObject collider;

    private void Start()
    {
        collider.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(5, GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-5, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        if (Input.GetKey(KeyCode.W) && isGrounded)
        {
            isGrounded = false;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4) && puedoUtilizarLaHabilidad)
        {
            puedoUtilizarLaHabilidad = false;
            StartCoroutine(TiempoUsoHabilidad2());
        }
    }


    private IEnumerator TiempoUsoHabilidad2()
    {
        collider.SetActive(true);
        yield return new WaitForSeconds(2f);
        collider.SetActive(false);
        StartCoroutine(UtilziarHabilidad());
    }

    private IEnumerator UtilziarHabilidad()
    {
        yield return new WaitForSeconds(10f);
        puedoUtilizarLaHabilidad = true;
    }

}
