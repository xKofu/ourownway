using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrearPlataformas_controller : MonoBehaviour
{


    [Header("true = estoy tocando el suelo")]
    [SerializeField]
    private bool isGrounded = false;

    [Header("true = puedo utilizar la habilidad")]
    [SerializeField]
    private bool puedoUtilizarLaHabilidad = true;

    [Header("true = Mirando derecha")]
    [SerializeField]
    private bool direccion = true;



    [Header("La plataforma que ponemos")]
    [SerializeField]
    private Pool m_pool;

    private GameObject plataforma;

    private float m_velocidad = 5;


    private bool mePuedoMover = true;


    void Update()
    {
        if (mePuedoMover)
        {
            if (Input.GetKey(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                direccion = true;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                direccion = false;
            }
            else
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            if (Input.GetKey(KeyCode.W) && isGrounded)
            {
                isGrounded = false;
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
            }

            if (Input.GetKeyDown(KeyCode.Alpha4) && puedoUtilizarLaHabilidad)
            {
                plataforma = m_pool.getElement();

                if (direccion)
                    plataforma.transform.position = new Vector3(this.gameObject.transform.position.x + 5, this.gameObject.transform.position.y + 2, 0);
                else
                    plataforma.transform.position = new Vector3(this.gameObject.transform.position.x - 5, this.gameObject.transform.position.y + 2, 0);

                StartCoroutine(UtilziarHabilidad());
                StartCoroutine(DevolverPool(plataforma));

            }
        }
    }

    private IEnumerator UtilziarHabilidad()
    {
        puedoUtilizarLaHabilidad = false;
        yield return new WaitForSeconds(10f);
        puedoUtilizarLaHabilidad = true;
    }

    private IEnumerator DevolverPool(GameObject go)
    {
        yield return new WaitForSeconds(3f);
        m_pool.ReturnElement(go);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = true;

        if (collision.collider.tag.Equals("Enemigo"))
        {
            print("CHOCO CON UN ENEMIGO O ALGO QUE ME TENGA QUE REPELER");
            if (collision.gameObject.transform.position.x > this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 2);
            else if (collision.gameObject.transform.position.x < this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 2f);
            else
            {
                int r = Random.RandomRange(0, 1);
                if(r == 0)
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 3);
                else
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 3);
            }

            StartCoroutine(DejarDeMover());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemigo"))
        {
            print("CHOCO CON UN ENEMIGO O ALGO QUE ME TENGA QUE REPELER");
            if (collision.gameObject.transform.position.x > this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 2);
            else if (collision.gameObject.transform.position.x < this.gameObject.transform.position.x)
                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 2f);
            else
            {
                int r = Random.RandomRange(0, 1);
                if (r == 0)
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 3);
                else
                    this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 3);
            }

            StartCoroutine(DejarDeMover());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = false;
    }

    private IEnumerator DejarDeMover()
    {
        mePuedoMover = false;
        yield return new WaitForSeconds(0.5f);
        mePuedoMover = true;
    }


}
