using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SombraCombate : MonoBehaviour
{
    [SerializeField]
    private Pool sombraPool;

    private float tiempoSombreCD = 0.1f;

    void Update()
    {
        if (tiempoSombreCD > 0)
            tiempoSombreCD -= Time.deltaTime;
        else
        {
            CrearSombra();
            tiempoSombreCD = 0.1f;
        }
    }



    private void CrearSombra()
    {
        GameObject m_sombra = sombraPool.getElement(); //Instantiate(sombra, this.gameObject.transform.position, Quaternion.identity);
        m_sombra.transform.position = this.gameObject.transform.position;
        m_sombra.transform.localScale = this.gameObject.transform.localScale;
        m_sombra.GetComponent<SpriteRenderer>().sprite = this.gameObject.GetComponent<SpriteRenderer>().sprite;
        m_sombra.GetComponent<SpriteRenderer>().color = Color.red;
        StartCoroutine(DevolverPool(m_sombra));
    }


    private IEnumerator DevolverPool(GameObject go)
    {
        yield return new WaitForSeconds(0.31f); // basicamente porque la animacion dura 0.30
        sombraPool.ReturnElement(go);
    }
}
