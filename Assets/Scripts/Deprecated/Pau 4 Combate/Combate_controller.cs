using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combate_controller : MonoBehaviour
{

    [Header("Estadisticas")]
    [SerializeField]
    ScriptableEstadisticas se_estadisticasPersonaje;


    [Header("true = estoy tocando el suelo")]
    [SerializeField]
    private bool isGrounded = false;

    [Header("true = puedo utilizar la habilidad")]
    [SerializeField]
    private bool puedoUtilizarLaHabilidad = true;

    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(se_estadisticasPersonaje.velocidad, GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(-se_estadisticasPersonaje.velocidad, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        if (Input.GetKey(KeyCode.W) && isGrounded)
        {
            isGrounded = false;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4) && puedoUtilizarLaHabilidad)
        {
            puedoUtilizarLaHabilidad = false;
            StartCoroutine(TiempoUsoHabilidad4());
        }
    }

    private IEnumerator TiempoUsoHabilidad4()
    {
        PonerHabilidad4Combate();
        yield return new WaitForSeconds(4f);
        QuitarHabilidad4Combate();
        StartCoroutine(UtilziarHabilidad());
    }

    private IEnumerator UtilziarHabilidad()
    {
        yield return new WaitForSeconds(10f);
        puedoUtilizarLaHabilidad = true;
    }


    private void PonerHabilidad4Combate()
    {
        se_estadisticasPersonaje.da�o += 5;
        se_estadisticasPersonaje.velocidad += 3;
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
        this.gameObject.GetComponent<SombraCombate>().enabled = true;
    }

    private void QuitarHabilidad4Combate()
    {
        se_estadisticasPersonaje.da�o -= 5;
        se_estadisticasPersonaje.velocidad -= 3;
        this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        this.gameObject.GetComponent<SombraCombate>().enabled = false;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
            isGrounded = false;
    }
}
