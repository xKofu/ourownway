using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambio_controller : MonoBehaviour
{

    [SerializeField]
    private GameObject pau, gerard, general;

    //[SerializeField]
    //GameEvent cambioAPau, cambioAGerard;

    [SerializeField]
    private bool empezar_contador = false;


    private void Awake()
    {
        Desactivar(gerard);
        //general.GetComponent<gerard_controller>().enabled = false;
        Solo_Activar(pau);
        //general.GetComponent<pau_controller>().enabled = true;
    }

    void Start()
    {

    }

    void Update()
    {
        /*
        if (!empezar_contador) { 
            if (Input.GetKeyDown(KeyCode.C)) // C de Cambio o Change
            {
                if (pau.active)
                {
                    Desactivar(pau);
                    Activar(gerard, pau);
                }
                else
                {
                    Desactivar(gerard);
                    Activar(pau, gerard);
                }
                StartCoroutine(ControlCambioPersonaje());
            }
        }
        */

        if (Input.GetKeyDown(KeyCode.C))
            CambiarPJ();


    }

    public void CambiarPJ()
    {
        if (!empezar_contador)
        {
            if (pau.active)
            {
                Desactivar(pau);
                //general.GetComponent<pau_controller>().enabled = false;
                Activar(gerard, pau);
                //general.GetComponent<gerard_controller>().enabled = true;
                //cambioAGerard.Raise();
            }
            else
            {
                Desactivar(gerard);
                //general.GetComponent<gerard_controller>().enabled = false;
                Activar(pau, gerard);
                //general.GetComponent<pau_controller>().enabled = true;
                //cambioAPau.Raise();
            }
            empezar_contador = true;
            //StartCoroutine(ControlCambioPersonaje());   
        }
    }

    public void CambiarPJWithEvent()
    {
        if (pau.active)
        {
            Desactivar(pau);
            Activar(gerard, pau);
        }
        else
        {
            Desactivar(gerard);
            Activar(pau, gerard);
        }
    }

    private void Activar(GameObject entra, GameObject sale)
    {
        entra.SetActive(true);
        CambioPosicion(entra, sale);


    }
    private void Desactivar(GameObject go)
    {
        go.SetActive(false);
    }
    private void Solo_Activar(GameObject go)
    {
        go.SetActive(true);
    }
    private void CambioPosicion(GameObject entra, GameObject sale)
    {
        Vector3 posicion = sale.transform.position;
        posicion.z = 0;
        posicion.y = posicion.y+0.5f;

        general.transform.position = posicion;
    }

    /*
    private IEnumerator ControlCambioPersonaje()
    {
        empezar_contador = true;
        yield return new WaitForSeconds(5f);
        empezar_contador = false;
    }
    */

    public void CambioPersonaje()
    {
        empezar_contador = false;
    }
}
