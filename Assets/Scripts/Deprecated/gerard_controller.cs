using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gerard_controller : MonoBehaviour
{
    [Header("Game Events")]
    [SerializeField] private GameEvent habilidad1_Planear;
    [SerializeField] private GameEvent habilidad2_Grito;
    [SerializeField] private GameEvent habilidad3_Gancho;
    [SerializeField] private GameEvent habilidad4_CrearPlataforma;


    [Header("Scriptables Cooldows")]
    [SerializeField] private ScriptableCooldown sc_habilidad1_Planear;
    [SerializeField] private ScriptableCooldown sc_habilidad2_Grito;
    [SerializeField] private ScriptableCooldown sc_habilidad3_Gancho;
    [SerializeField] private ScriptableCooldown sc_habilidad4_CrearPlataforma;


    private bool planear, grito, gancho, plataforma = false;




    [Header("Gancho")]
    [SerializeField]
    LineRenderer line;

    [SerializeField] 
    LayerMask grapplableMask;

    [SerializeField] 
    float maxDistance = 100f; 

    bool isGrappling = false;


    [HideInInspector] 
    public bool retracting = false;

    RaycastHit2D hit;

    Vector2 target;

    //GameObject hitGameObject;


    private void Start()
    {
    }

    private void Update()
    {
        if (!planear)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                planear = true;
                print("GERARD --> pulso 1");
                habilidad1_Planear.Raise();
                StartCoroutine(Habilidad1());

            }
        }
        if (!grito)
        {
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                grito = true;
                print("GERARD -->  pulso 2");
                habilidad2_Grito.Raise();
                StartCoroutine(Habilidad2());

            }
        }
        if (!gancho)
        {
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                gancho = true;
                print("GERARD -->  pulso 3");
                habilidad3_Gancho.Raise();
                StartCoroutine(Habilidad3());

            }
        }
        if (!plataforma)
        {
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                plataforma = true;
                print("GERARD -->  pulso 4");
                habilidad4_CrearPlataforma.Raise();
                StartCoroutine(Habilidad4());
            }
        }
    

        /*
        if (Input.GetMouseButtonDown(1) && !isGrappling)
            StartGrapple();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            gameObject.GetComponent<DistanceJoint2D>().enabled = false;
            isGrappling = false;
            //hitGameObject = null;
        }

        if (isGrappling)
        {
            line.SetPosition(0, transform.position);
            line.SetPosition(1, hit.point);
        }

        line.enabled = isGrappling;

       // if (hitGameObject)
        //{
            //if(hitGameObject.transform.position.y - gameObject.transform.position.y < -1.5f || hitGameObject.transform.position.y - gameObject.transform.position.y > 1.5f)
            {
                gameObject.GetComponent<DistanceJoint2D>().enabled = false;
                isGrappling = false;
                //hitGameObject = null;
            }
        //}
        */
    }

    /*
    private void StartGrapple()
    {

        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        hit = Physics2D.Raycast(transform.position, direction, maxDistance, grapplableMask); //RaycastHit2D 

        if (hit.collider != null)
            Debug.DrawLine(transform.position, new Vector2(hit.point.x, hit.point.y), Color.blue, 10.0f);

        if (hit.collider != null && hit.collider.gameObject.layer == 6)
        {
            //hitGameObject = hit.collider.gameObject;

            isGrappling = true;
            line.enabled = true;
            line.positionCount = 2;
            gameObject.GetComponent<Rigidbody2D>().MovePosition(Vector2.MoveTowards(transform.position, hit.transform.position, 0.2f * Time.deltaTime));
            gameObject.GetComponent<DistanceJoint2D>().enabled = true;
            gameObject.GetComponent<DistanceJoint2D>().connectedAnchor = new Vector2(hit.point.x, hit.point.y);
            gameObject.GetComponent<DistanceJoint2D>().distance = 1.0f;
            target = hit.point;
        }
    }
    */


    private IEnumerator Habilidad1()
    {
        sc_habilidad1_Planear.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad1_Planear.tiempoActual >= sc_habilidad1_Planear.tiempoTotal)
        {
            sc_habilidad1_Planear.tiempoTotal = 0;
            planear = false;
        }
    }
    private IEnumerator Habilidad2()
    {
        sc_habilidad2_Grito.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad2_Grito.tiempoActual >= sc_habilidad2_Grito.tiempoTotal)
        {
            sc_habilidad2_Grito.tiempoTotal = 0;
            grito = false;
        }
    }

    private IEnumerator Habilidad3()
    {
        sc_habilidad3_Gancho.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad3_Gancho.tiempoActual >= sc_habilidad3_Gancho.tiempoTotal)
        {
            sc_habilidad3_Gancho.tiempoTotal = 0;
            gancho = false;
        }
    }

    private IEnumerator Habilidad4()
    {
        sc_habilidad4_CrearPlataforma.tiempoActual += 0.1f;
        yield return new WaitForSeconds(0.1f);
        if (sc_habilidad4_CrearPlataforma.tiempoActual >= sc_habilidad4_CrearPlataforma.tiempoTotal)
        {
            sc_habilidad4_CrearPlataforma.tiempoTotal = 0;
            plataforma = false;
        }
    }


}
