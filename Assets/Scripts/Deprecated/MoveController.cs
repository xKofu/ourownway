using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField]
    private float m_velocidad = 5f;
    [SerializeField]
    private float m_velocidadAgua = 100f;
    [SerializeField]
    private float m_FuerzaSalto = 5f;
    [SerializeField]
    private bool m_nadar;
    [SerializeField]
    private bool isGrounded;
    [SerializeField]
    private bool MovimientoActivo;

    private float tiempoMin = 0.5f;
    private float tiempoMax = 1.0f;

    bool toSoon = false;

    private CombatState state = CombatState.Nothing;

    private enum State { MOVEMENT, IDLE, NADAR };

    private enum Direccion { DERECHA, IZQUIERDA }
    private Direccion view_dir;
    private enum CombatState { C, V, Nothing, CC, CV };

    [SerializeField]
    private State m_state;





    private bool suspension = false;


    void Start()
    {
        view_dir = Direccion.DERECHA;
        m_state = State.IDLE;
        isGrounded = true;
        m_velocidad = 5f;
        m_velocidadAgua = 6f;
        m_FuerzaSalto = 5f;
        m_nadar = false;
        MovimientoActivo = true;
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(comboNewInput("C"));
        }
        else if (Input.GetKeyDown(KeyCode.V))
        {
            StartCoroutine(comboNewInput("V"));
        }


        if (MovimientoActivo)
        {

            if (Input.GetKey(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                if (view_dir == Direccion.IZQUIERDA)
                {
                    Flip();
                    view_dir = Direccion.DERECHA;
                }
            }
            else if (Input.GetKey(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidad, GetComponent<Rigidbody2D>().velocity.y);
                if (view_dir == Direccion.DERECHA)
                {
                    Flip();
                    view_dir = Direccion.IZQUIERDA;
                }
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            }

        }

        MaquinaEjecutar();

    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    private void MaquinaIniciar(State NewState)
    {

        switch (NewState)
        {

            case State.IDLE:
                m_state = NewState;
                break;
            case State.MOVEMENT:
                m_state = NewState;
                MovimientoActivo = true;
                break;
            case State.NADAR:
                m_state = NewState;
                if (m_nadar)
                    print("Estoy nadando");
                break;
        }

    }

    private void MaquinaAcabar()
    {

        switch (m_state)
        {

            case State.IDLE:
                //print("acabar idle");
                break;
            case State.MOVEMENT:
                //print("acabar movimiento"); 
                break;
            case State.NADAR:
                //print("acabar nadar");
                break;
        }

    }

    private void MaquinaEjecutar()
    {

        switch (m_state)
        {

            case State.IDLE:
                IdleState();
                break;
            case State.MOVEMENT:
                JumpState();
                break;
            case State.NADAR:
                SwimState();
                break;
        }

    }

    private void ChangeState(State NextState)
    {

        MaquinaAcabar();

        MaquinaIniciar(NextState);

        MaquinaEjecutar();

    }

    private void IdleState()
    {

        if (m_nadar)
        {
            ChangeState(State.NADAR);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || (isGrounded && Input.GetKeyDown(KeyCode.W)))
        {
            ChangeState(State.MOVEMENT);
        }

    }

    [SerializeField]
    private float tiempoSalto;

    private void JumpState()
    {

        if (Input.GetKey(KeyCode.W))
        {
            if (isGrounded) {
                tiempoSalto += Time.deltaTime;

                if (tiempoSalto < 0.15)
                {
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 50f) * Time.deltaTime, ForceMode2D.Impulse);
                }
                else
                {
                    isGrounded = false;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            isGrounded = false;
            tiempoSalto = 0;
        }

        


            /*
            if (isGrounded && Input.GetKeyDown(KeyCode.W))
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, m_FuerzaSalto), ForceMode2D.Impulse);
                isGrounded = false;
            }
            */
        if (!m_nadar && !isGrounded && GetComponent<Rigidbody2D>().velocity.y > 0)
        {
        }
        else if (!m_nadar && !isGrounded && GetComponent<Rigidbody2D>().velocity.y < 0)
        {


        }
        else if (!m_nadar && GetComponent<Rigidbody2D>().velocity.x != 0)
        {
        }


        if (m_nadar)
        {
            ChangeState(State.NADAR);
        }
        else if (GetComponent<Rigidbody2D>().velocity.x == 0 && GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            ChangeState(State.IDLE);
        }

    }

    private void SwimState()
    {

        if (m_nadar)
        {

            if (Input.GetKeyDown(KeyCode.D))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                //GetComponent<SpriteRenderer>().flipX = false;
            }

            else if (Input.GetKeyDown(KeyCode.A))
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(-m_velocidadAgua, GetComponent<Rigidbody2D>().velocity.y);
                //GetComponent<SpriteRenderer>().flipX = true;
            }

            if (Input.GetKeyDown(KeyCode.W))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, m_velocidadAgua);
            else if (Input.GetKeyDown(KeyCode.S))
                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -m_velocidadAgua);
        }
        else
        {
            m_state = State.IDLE;
        }


    }

    IEnumerator comboNewInput(string input)
    {

        if (!toSoon)
        {
            switch (state)
            {
                case CombatState.Nothing:
                    if (input == "C")
                    {
                        state = CombatState.C;
                        print("Hace c");
                    }
                    else if (input == "V")
                    {

                        //otro ataque
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.C:
                    if (input == "C")
                    {
                        print("Hace c_c");
                        state = CombatState.CC;

                    }
                    else if (input == "V")
                    {
                        print("hace c_v");
                        state = CombatState.CV;
                    }
                    break;

                case CombatState.CC:
                    if (input == "C")
                    {
                        // tercer ataque
                        print("Hace c_c_c");
                        state = CombatState.Nothing;// si este fuera el ataque final me pndria como estado inicial, si no seguiria

                    }
                    else if (input == "V")
                    {
                        print("hace c_c_v");
                        state = CombatState.Nothing;
                    }
                    break;
                case CombatState.CV:
                    if (input == "C")
                    {
                        // tercer/2 ataque
                        print("Hace c_v_c");
                        state = CombatState.Nothing;

                    }
                    else if (input == "V")
                    {
                        print("hace v");
                        state = CombatState.Nothing;
                    }
                    break;
            }
        }




        CombatState copyState = state;
        toSoon = true;
        yield return new WaitForSeconds(tiempoMin);
        toSoon = false;
        yield return new WaitForSeconds(tiempoMax - tiempoMin);


        if (state == copyState) //si no a variado lo reiniciamos
        {
            isGrounded = true;
            state = CombatState.Nothing;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "ground")
        {
            isGrounded = true;
            //this.GetComponent<Rigidbody2D>().velocity = new Vector3();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {

    }

    // ESTO ES PA QUEDARME LO MIO

}