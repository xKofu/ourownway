using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueDistancia_controller : MonoBehaviour
{

    [SerializeField]
    private GameEvent ataqueDistancia;

    [SerializeField]
    private GameEvent anadirAtaqueDistancia;


    [SerializeField]
    private ScriptableCooldown sc_ataqueDistancia;

    [SerializeField]
    private ScriptableAtaqueDistancia sad_ataqueDistancia;

    [SerializeField]
    private bool ataque = true;



    void Start()
    {
        sad_ataqueDistancia.numeroAtaquesAct = 3;
    }


    void Update()
    {
        if (ataque)
        {
            if (sad_ataqueDistancia.numeroAtaquesAct > 0)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    print("ATAQUE A DISTANCIA");
                    sad_ataqueDistancia.numeroAtaquesAct--;
                    ataqueDistancia.Raise();
                    StartCoroutine(AtaqueDistancia());
                }
            }
        }
    }

    private IEnumerator AtaqueDistancia()
    {
        sc_ataqueDistancia.tiempoActual = 0;
        ataque = false;
        while (sc_ataqueDistancia.tiempoActual < sc_ataqueDistancia.tiempoTotal) {
            yield return new WaitForSeconds(0.1f);
            sc_ataqueDistancia.tiempoActual += 0.1f;
            if (sc_ataqueDistancia.tiempoActual >= sc_ataqueDistancia.tiempoTotal)
                sc_ataqueDistancia.tiempoActual = sc_ataqueDistancia.tiempoTotal;
        }
        ataque = true;
    }


    public void AddAtaque()
    {
        if ((sad_ataqueDistancia.numeroAtaquesAct + 1) < 3)
        {
            sad_ataqueDistancia.numeroAtaquesAct++;
            anadirAtaqueDistancia.Raise();
        }
    }
}
