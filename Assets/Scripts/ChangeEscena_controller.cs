using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeEscena_controller : MonoBehaviour
{

    [SerializeField] private ScriptableInfoTP info;
    [SerializeField] private GameEvent PonerCamara;
    [SerializeField] private PosicionesGeneralesTp pg;
    

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name != "Paki")
            PonerCamara.Raise();
    }
    public void IrPaki()
    {
        SceneManager.LoadScene("Paki");
    }
    public void Volver()
    {
        SceneManager.LoadScene(info.EscenaOrigen);
    }

    public void PantallaInicio()
    {
        SceneManager.LoadScene("PantallaInicio");
    }

    public void SeleccionarDestino(string s)
    {
        SceneManager.LoadScene(s);
    }

    public void CambiaZonaPortal(int Index)
    {
        switch(Index) {
            // Portales CB
            case 1:
                info.posSpawn = pg.CB_to_Sab;
                SeleccionarDestino("Sabadell");
                break;
            case 2:
                info.posSpawn = pg.CB_to_MGA;
                SeleccionarDestino("MGA");
                break;
            // Portales Sabadell
            case 3:
                info.posSpawn = pg.Sab_to_CB;
                SeleccionarDestino("Castellbisbal");
                break;
            case 4:
                info.posSpawn = pg.Sab_to_MGA;
                SeleccionarDestino("MGA");
                break;
            // Portales MGA
            case 5:
                info.posSpawn = pg.MGA_to_Sab;
                SeleccionarDestino("Sabadell");
                break;
            case 6:
                info.posSpawn = pg.MGA_to_CB;
                SeleccionarDestino("Castellbisbal");
                break;
            // Portales de vuelta de Boss
            case 7:
                info.posSpawn = pg.CB_from_Boss;
                SeleccionarDestino("Castellbisbal");
                break;
            case 8:
                info.posSpawn = pg.MGA_from_Boss;
                SeleccionarDestino("MGA");
                break;

        }
    }

}
