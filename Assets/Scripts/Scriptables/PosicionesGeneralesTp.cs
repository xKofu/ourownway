using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PosicionesGenerales")]
public class PosicionesGeneralesTp : ScriptableObject
{
    [Header("Origen: Castellbisbal")]
    public Vector3 CB_to_Sab;
    public Vector3 CB_to_MGA;

    [Header("Origen: Sabadell")]
    public Vector3 Sab_to_CB;
    public Vector3 Sab_to_MGA;

    [Header("Origen: MGA")]
    public Vector3 MGA_to_CB;
    public Vector3 MGA_to_Sab;

    [Header("Origen: Boss")]
    public Vector3 CB_from_Boss;
    public Vector3 MGA_from_Boss;

}
