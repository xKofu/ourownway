using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableAtaqueDistancia : ScriptableObject
{
    public int numeroAtaquesMax = 3;
    public int numeroAtaquesAct = 3;
}
