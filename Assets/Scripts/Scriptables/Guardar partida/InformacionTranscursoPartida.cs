using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InformacionTranscursoPartida : ScriptableObject
{
    [Header("Habilidades Pau")]
    public bool habilidad1Pau;
    public bool habilidad2Pau;
    public bool habilidad3Pau;
    public bool habilidad4Pau;

    [Header("Habilidades Gerard")]
    public bool habilidad1Gerard;
    public bool habilidad2Gerard;
    public bool habilidad3Gerard;
    public bool habilidad4Gerard;

    [Header("Datos Castellbisbal")]
    public bool bossCastellbisbalDone;
    public bool PickedAb1G;
    public bool PickedAb1P;
    public bool Obstaculo1CB;
    public bool Obstaculo2CB;
    public bool Obstaculo3CB;
    public bool Atajo1CB;

    [Header("Datos Sabadell")]
    public bool bossSabadellDone;
    public bool PickedAb2G;
    public bool PickedAb2P;
    public bool PickedAb3P;
    public bool Obstaculo1Sab;
    public bool Obstaculo2Sab;

    [Header("Datos MGA")]
    public bool bossMGADone;
    public bool PickedAb3G;
    public bool PickedAb4G;
    public bool PickedAb4P;
    public bool Obstaculo1MGA;

}
