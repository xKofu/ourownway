using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Guardar/Odin/GuardarOdin")]
public class GuardarOdin : ScriptableObject
{
     public ScriptableInfoTP siTP;
     public InformacionTranscursoPartida itp;
    public bool seHaGuardadoPartida = false;
}
