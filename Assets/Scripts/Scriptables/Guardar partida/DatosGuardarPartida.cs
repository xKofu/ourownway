using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Guardar/DatosGuardarPartida")]
public class DatosGuardarPartida : ScriptableObject
{
    [Header("Habilidades Pau")]
    public bool habilidad1Pau;
    public bool habilidad2Pau;
    public bool habilidad3Pau;
    public bool habilidad4Pau;

    [Header("Habilidades Gerard")]
    public bool habilidad1Gerard;
    public bool habilidad2Gerard;
    public bool habilidad3Gerard;
    public bool habilidad4Gerard;

    [Header("Datos Castellbisbal")]
    public bool bossCastellbisbalDone;
    public bool PickedAb1G;
    public bool PickedAb1P;
    public bool Obstaculo1;
    public bool Obstaculo2;
    public bool Obstaculo3;

    [Header("Datos Sabadell")]
    public bool bossSabadellDone;
    public bool PickedAb2G;
    public bool PickedAb2P;

    [Header("Datos MGA")]
    public bool bossMGADone;

    // A DONDE LO ENVIAMOS AL PLAYER DESPUES DE QUE SALGA DEL PAKI
    public string EscenaOrigen;
    public int numTP;
    public Vector3 posSpawn;
    public bool quienEntra; // true: entra el gerard // false: entra el pau


    // ZONAS��


    // 


}
