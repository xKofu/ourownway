using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableCooldown : ScriptableObject
{
    public int tiempoTotal;
    public float tiempoActual;
}
