using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]

public class ScriptableInventario : ScriptableObject
{
    public int curacion = 5;
    public int maxCuracion = 5;
}
