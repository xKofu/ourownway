using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]

public class ScriptableInfoTP : ScriptableObject
{
    public string EscenaOrigen;
    public int numTP;
    public Vector3 posSpawn;
    public bool quienEntra; // true: entra el gerard // false: entra el pau
}
