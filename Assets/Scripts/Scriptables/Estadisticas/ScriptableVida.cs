using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableVida : ScriptableObject
{
    public float VidaActual;
    public float VidaMaxima;
}
