using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara_Controller : MonoBehaviour
{
    [SerializeField]
    private GameObject Personaje;

    


    private Vector3 m_Offset;
    private Vector3 m_FollowPosition;
    private Vector3 m_InitialPosition;

    private float m_FollowSpeed = 0.3f;
    private float m_FollowTime = 0.0f;



    private void Awake()
    {
        //SetCameraPosition();
    }
    void Start()
    {
        //SetCameraPosition();
    }

    //lerp
    void Update()
    {
        if (Personaje)
        {
            if (Personaje.transform.position != m_FollowPosition)
            {
                m_InitialPosition = transform.position;
                m_InitialPosition.z = -10;
                m_FollowPosition = Personaje.transform.position;
                m_FollowTime = 0.0f;
            }

            if (m_FollowTime < m_FollowSpeed)
            {
                m_FollowTime += Time.deltaTime;
                transform.position = Vector3.Lerp(m_InitialPosition, m_FollowPosition - m_Offset, (m_FollowTime / m_FollowSpeed));
                transform.position = new Vector3(transform.position.x, transform.position.y,-10);
                Debug.DrawLine(transform.position, m_FollowPosition - m_Offset, Color.red);
            }
        }
    }


    public void SetCameraPosition()
    {
        print("LLamo a la funcion de la camara");
        if (Personaje)
        {
            m_FollowPosition = Personaje.transform.position;
            m_InitialPosition = transform.position;
            m_Offset = Personaje.transform.position - transform.position;
            m_FollowTime = m_FollowSpeed;
        }
    }
}
