using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibracionCamara_controller : MonoBehaviour
{
    private float tiempoVibracion = 0.15f;
    private float fuerzaVibracion = 0.1f;
    


    public void LlamarVibrar()
    {
        StartCoroutine(Vibrar());

    } 
    private IEnumerator Vibrar()
    {
        Vector3 posicionOriginal = transform.position;


        float tiempoPasado = 0;
        
        while(tiempoPasado < tiempoVibracion)
        {
            float x = Random.Range(this.gameObject.transform.position.x - 1f, this.gameObject.transform.position.x + 1f) * fuerzaVibracion;
            float y = Random.Range(this.gameObject.transform.position.y - 1f, this.gameObject.transform.position.y + 1f) * fuerzaVibracion;

            this.gameObject.transform.position = new Vector3(x, y, this.gameObject.transform.position.z); // seguramente que lo pondria a 0 y luego no se veria nada
            tiempoPasado += Time.deltaTime;
            yield return null; // ????? esto es legl?
        }
        this.transform.localPosition = posicionOriginal;
    }
}
