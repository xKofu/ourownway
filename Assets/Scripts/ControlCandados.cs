using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCandados : MonoBehaviour
{

    [SerializeField] private InformacionTranscursoPartida itp;
    [SerializeField] private GameObject Candado0;
    [SerializeField] private GameObject Candado1;
    [SerializeField] private GameObject Candado2;
    [SerializeField] private GameObject Candado3;
    [SerializeField] private GameObject Candado4;
    [SerializeField] private GameObject Candado5;
    [SerializeField] private GameObject Candado6;
    [SerializeField] private GameObject Candado7;

    void Start()
    {
        if (itp.habilidad1Pau)
            Candado0.SetActive(false);

        if (itp.habilidad2Pau)
            Candado1.SetActive(false);

        if (itp.habilidad3Pau)
            Candado2.SetActive(false);

        if (itp.habilidad4Pau)
            Candado3.SetActive(false);

        if (itp.habilidad1Gerard)
            Candado4.SetActive(false);

        if (itp.habilidad2Gerard)
            Candado5.SetActive(false);

        if (itp.habilidad3Gerard)
            Candado6.SetActive(false);

        if (itp.habilidad4Gerard)
            Candado7.SetActive(false);
    }

    public void QuitarCandado(int Index)
    {
        switch(Index)
        {
            case 0:
                StartCoroutine(AccionCandado(Candado0));
                break;
            case 1:
                StartCoroutine(AccionCandado(Candado1));
                break;
            case 2:
                StartCoroutine(AccionCandado(Candado2));
                break;
            case 3:
                StartCoroutine(AccionCandado(Candado3));
                break;
            case 4:
                StartCoroutine(AccionCandado(Candado4));
                break;
            case 5:
                StartCoroutine(AccionCandado(Candado5));
                break;
            case 6:
                StartCoroutine(AccionCandado(Candado6));
                break;
            case 7:
                StartCoroutine(AccionCandado(Candado7));
                break;
        }
    }

    IEnumerator AccionCandado(GameObject A)
    {
        A.GetComponent<Animator>().Play("CaidaCandado");
        yield return new WaitForSeconds(1);
        A.SetActive(false);
    }

}
