using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarHUDBosses : MonoBehaviour
{
    [SerializeField] private GameObject conjuntoVida;

    private void Start()
    {
        conjuntoVida.SetActive(false);
    }

    public void PonerHUDBoss()
    {
        conjuntoVida.SetActive(true);
    }

    public void QuitarHUDBoss()
    {
        conjuntoVida.SetActive(false);
    }
}
