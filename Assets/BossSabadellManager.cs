using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BossSabadellManager : MonoBehaviour
{
    [Header("Muros Invisibles")]
    [SerializeField] private GameObject inv1;
    [SerializeField] private GameObject inv2;


    [Header("Informacion transcurso partida")]
    [SerializeField] private InformacionTranscursoPartida itp;

    [Header("Eventos")]
    [SerializeField] private GameEvent entrarSalaBossTribunal;
    [SerializeField] private GameEvent iniciarBossTribunal;

    [Header("Particulas")]
    [SerializeField] private GameObject particulas1;
    [SerializeField] private GameObject particulas2;




    void Start()
    {
        inv1.SetActive(false);
        inv2.SetActive(false);
        particulas1.SetActive(false);
        particulas2.SetActive(false);
        StartCoroutine(PonerHabilidades());
    }


    public void TrinidadMuerto()
    {

        inv2.SetActive(false);
        itp.bossSabadellDone = true;
        // y lo que me diga el gerar que no lo se //MAPA
        particulas1.SetActive(true);
        particulas2.SetActive(true);
        StartCoroutine(QuitarParticulas());
    }

    public void PonerMueroInvisible()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        iniciarBossTribunal.Raise();
        inv1.SetActive(true);
        inv2.SetActive(true);
    }

    public void QuitarMurosInvisible()
    {
        inv1.SetActive(false);
        inv2.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
            PonerMueroInvisible();
    }

    private IEnumerator PonerHabilidades()
    {
        yield return new WaitForSeconds(0.5f);
        entrarSalaBossTribunal.Raise();
    }

    private IEnumerator QuitarParticulas()
    {
        yield return new WaitForSeconds(1f);
        particulas1.GetComponent<VisualEffect>().Stop();
        particulas2.GetComponent<VisualEffect>().Stop();
    }

}
