using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanyoEnemigo_controller : MonoBehaviour
{

    [Header("CUERPO")]
    [SerializeField] private SpriteRenderer[] cuerpo;


    public void Hurt()
    {
        StartCoroutine(CorrutinaDanyo());
    }

    private IEnumerator CorrutinaDanyo()
    {
        foreach (SpriteRenderer s in cuerpo)
            s.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        foreach (SpriteRenderer s in cuerpo)
            s.color = Color.white;
    }

    private void Blanco()
    {
        foreach (SpriteRenderer s in cuerpo)
            s.color = Color.white;
    }
}
